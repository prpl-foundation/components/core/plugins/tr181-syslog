/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_path.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <netmodel/common_api.h>

#include "tr181-syslog.h"
#include "tr181-syslog_utils.h"
#include "tr181-syslog_main.h"
#include "tr181-syslog_config.h"
#include "tr181-syslog_config_tls.h"
#include "tr181-syslog_config_source.h"

typedef struct {
    char* path;
    amxc_var_t var;
    netmodel_query_t* query;
} syslog_config_source_network_interface_t;

#define HAS_PREFIX(str, prefix) (strncmp(str, prefix, strlen(prefix)) == 0)

static amxc_var_t* add_source_endpoint(const char* type, amxc_var_t* endpoints) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* endpoint = amxc_var_add_new(endpoints);
    amxc_var_set_type(endpoint, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, endpoint, "type", type);
    SAH_TRACEZ_OUT(ME);
    return endpoint;
}

static void syslog_config_source_network_interface_query(UNUSED const char* sig_name, const amxc_var_t* data, void* priv) {
    SAH_TRACEZ_IN(ME);
    syslog_config_source_network_interface_t* interface = priv;
    amxc_var_t* if_var = NULL;
    when_null(interface, exit);

    if_var = &(interface->var);
    amxc_var_for_each_reverse(ip, data) {
        const char* if_name = GET_CHAR(ip, "NetDevName");
        if(if_name == NULL) {
            continue;
        }
        amxc_var_set_type(if_var, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, if_var, "name", if_name);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void syslog_config_source_network_interface_renew(syslog_config_source_network_interface_t** interface_ptr, const char* interface_path) {
    SAH_TRACEZ_IN(ME);
    syslog_config_source_network_interface_t* interface = NULL;
    when_null_trace(interface_ptr, exit, ERROR, "no interface ref");
    interface = *interface_ptr;

    if(interface != NULL) {
        when_true(streq(interface->path, interface_path), exit);
        free(interface->path);
        netmodel_closeQuery(interface->query);
        interface->query = NULL;
    } else {
        interface = *interface_ptr = calloc(1, sizeof(syslog_config_source_network_interface_t));
        when_null_trace(interface, exit, ERROR, "Failed to allocate source network interface struct");

        amxc_var_init(&(interface->var));
        SAH_TRACEZ_INFO(ME, "start listening for netmodel interface: %s", interface_path);
    }
    interface->path = strdup(interface_path);
    interface->query = netmodel_openQuery_getAddrs(interface_path, "tr181-syslog", "ipv4 || ipv6", "down", syslog_config_source_network_interface_query, interface);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void syslog_config_source_network_interface_delete(syslog_config_source_network_interface_t** interface_ptr) {
    SAH_TRACEZ_IN(ME);
    syslog_config_source_network_interface_t* interface = NULL;
    when_null(interface_ptr, exit);
    interface = *interface_ptr;
    *interface_ptr = NULL;
    when_null(interface, exit);

    free(interface->path);
    netmodel_closeQuery(interface->query);
    amxc_var_clean(&(interface->var));

    free(interface);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static amxc_var_t* syslog_config_source_network_interface_var(syslog_config_source_network_interface_t* interface) {
    amxc_var_t* var = NULL;
    when_null(interface, exit);
    when_true(amxc_var_is_null(&(interface->var)), exit)
    var = &(interface->var);
exit:
    return var;
}

static amxc_var_t* network_lookup_interface(amxd_object_t* network, const char* src_id) {
    const char* interface_path = GET_CHAR(amxd_object_get_param_value(network, "Interface"), NULL);

    if(EMPTY_STRING(interface_path)) {
        SAH_TRACEZ_WARNING(ME, "source %s network missing interface", src_id);
        syslog_config_source_network_interface_delete((syslog_config_source_network_interface_t**) &network->priv);
        goto exit;
    }
    syslog_config_source_network_interface_renew((syslog_config_source_network_interface_t**) &network->priv, interface_path);
exit:
    return syslog_config_source_network_interface_var((syslog_config_source_network_interface_t*) network->priv);
}

amxd_status_t _syslog_source_network_cleanup(amxd_object_t* object,
                                             UNUSED amxd_param_t* param,
                                             UNUSED amxd_action_t reason,
                                             UNUSED const amxc_var_t* const args,
                                             UNUSED amxc_var_t* const retval,
                                             UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    syslog_config_source_network_interface_delete((syslog_config_source_network_interface_t**) &object->priv);
    SAH_TRACEZ_OUT(ME);
    return amxd_status_ok;
}

static bool syslog_config_source_network_parse_tls(amxd_object_t* network, const char* src_id, amxc_var_t* tls) {
    bool rv = false;
    const char* certificate = GET_CHAR(amxd_object_get_param_value(network, syslog_get_prefixed_name("Certificate")), NULL);
    const char* ca_certificate = GET_CHAR(amxd_object_get_param_value(network, syslog_get_prefixed_name("CACertificate")), NULL);
    bool peer_verify = amxd_object_get_bool(network, syslog_get_prefixed_name("PeerVerify"), NULL);

    when_null_trace(tls, exit, ERROR, "");
    amxc_var_set_type(tls, AMXC_VAR_ID_HTABLE);

    when_str_empty_trace(certificate, exit, WARNING, "Certificate required to create a TLS network source");
    when_false_trace(syslog_config_tls_parse_certificate(certificate, amxc_var_add_new_key(tls, "certificate"), SYSLOG_TLS_CERT_PRIVATE),
                     exit, ERROR, "Failed to parse source %s certificate: %s", src_id, certificate);

    if(!EMPTY_STRING(ca_certificate)) {
        when_false_trace(syslog_config_tls_parse_certificate(ca_certificate, amxc_var_add_new_key(tls, "ca_certificate"), SYSLOG_TLS_CERT_PUBLIC),
                         exit, ERROR, "Failed to parse source %s CA certificate: %s", src_id, ca_certificate);
    }

    amxc_var_add_key(bool, tls, "verify", peer_verify);

    rv = true;
exit:
    return rv;
}

static bool add_source_network_endpoint(amxd_object_t* network, const char* src_id, amxc_var_t* endpoints) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    amxc_var_t* interface = network_lookup_interface(network, src_id);
    uint16_t port = amxd_object_get_uint16_t(network, "Port", NULL);
    const char* protocol = GET_CHAR(amxd_object_get_param_value(network, "Protocol"), NULL);
    bool structured = amxd_object_get_bool(network, syslog_get_prefixed_name("StructuredData"), NULL);
    amxc_var_t* endpoint = NULL;

    when_null(interface, exit);
    when_str_empty_trace(protocol, exit, WARNING, "source %s network missing protocol", src_id);
    when_false_trace(port > 0, exit, WARNING, "source %s network missing port", src_id);

    endpoint = add_source_endpoint("network", endpoints);
    amxc_var_add_key(bool, endpoint, "type", "network");
    amxc_var_set_key(endpoint, "interface", interface, AMXC_VAR_FLAG_COPY);
    amxc_var_add_key(uint16_t, endpoint, "port", port);
    amxc_var_add_key(cstring_t, endpoint, "protocol", protocol);
    amxc_var_add_key(bool, endpoint, "structured", structured);

    if(strcmp(protocol, "TLS") == 0) {
        when_false_trace(syslog_config_source_network_parse_tls(network, src_id, amxc_var_add_new_key(endpoint, "tls")),
                         exit, ERROR, "Failed to parse source %s TLS settings", src_id);
    }

    rv = true;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool syslog_config_source_parse_instance_endpoints(amxd_object_t* src_inst, const char* src_id, amxc_var_t* endpoints) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    bool kernel = amxd_object_get_bool(src_inst, "KernelMessages", NULL);
    bool system = amxd_object_get_bool(src_inst, "SystemMessages", NULL);
    amxd_object_t* network = amxd_object_get_child(src_inst, "Network");

    when_null_trace(network, exit, ERROR, "source %s missing network template", src_id);
    rv = true;

    if(kernel) {
        add_source_endpoint("kernel", endpoints);
    }
    if(system) {
        add_source_endpoint("system", endpoints);
    }
    if(syslog_netmodel_is_available() && amxd_object_get_bool(network, "Enable", NULL)) {
        add_source_network_endpoint(network, src_id, endpoints);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool syslog_config_source_parse_instance(amxd_object_t* src_inst, amxc_var_t* config_sources) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    const char* id = NULL;
    const char* severity = NULL;
    const char* facility = NULL;
    amxc_var_t* endpoints;
    amxc_var_t source;

    amxc_var_init(&source);
    amxc_var_set_type(&source, AMXC_VAR_ID_HTABLE);

    id = GET_CHAR(amxd_object_get_param_value(src_inst, "Alias"), NULL);
    when_str_empty_trace(id, exit, ERROR, "source has no Id/Alias");

    severity = GET_CHAR(amxd_object_get_param_value(src_inst, "Severity"), NULL);
    when_str_empty_trace(severity, exit, ERROR, "source %s Severity empty", id);
    amxc_var_add_key(cstring_t, &source, "severity", severity);

    facility = GET_CHAR(amxd_object_get_param_value(src_inst, "FacilityLevel"), NULL);
    when_str_empty_trace(facility, exit, ERROR, "source %s FacilityLevel empty", id);
    amxc_var_add_key(cstring_t, &source, "facility", facility);

    endpoints = amxc_var_add_new_key(&source, "endpoints");
    amxc_var_set_type(endpoints, AMXC_VAR_ID_LIST);
    when_false(syslog_config_source_parse_instance_endpoints(src_inst, id, endpoints), exit);

    amxc_var_set_key(config_sources, id, &source, AMXC_VAR_FLAG_COPY);
    rv = true;
exit:
    amxc_var_clean(&source);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

bool syslog_config_source_parse(amxc_var_t* config) {
    SAH_TRACEZ_IN(ME);
    bool res = false;
    amxc_var_t* sources = amxc_var_add_new_key(config, "sources");
    amxc_var_set_type(sources, AMXC_VAR_ID_HTABLE);

    res = syslog_config_parse_instances("Source", syslog_config_source_parse_instance, sources);
    SAH_TRACEZ_OUT(ME);
    return res;
}

void syslog_config_source_clean(void) {
    amxd_object_t* sources = amxd_dm_findf(syslog_get_dm(), "Syslog.Source.");
    amxd_object_for_each(instance, it, sources) {
        amxd_object_t* source = amxc_container_of(it, amxd_object_t, it);
        amxd_object_t* network = amxd_object_get(source, "Network");
        syslog_config_source_network_interface_delete((syslog_config_source_network_interface_t**) &network->priv);
    }
}
