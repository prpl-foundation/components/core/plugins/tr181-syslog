/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "tr181-syslog.h"
#include "tr181-syslog_main.h"
#include "tr181-syslog_dm.h"

static uint32_t changed_counter = 0;

static bool syslog_dm_events_allowed(void) {
    SAH_TRACEZ_IN(ME);
    bool res = syslog_get_dm() != NULL;
    SAH_TRACEZ_OUT(ME);
    return res;
}

void syslog_dm_emit_changed(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t var;
    amxd_object_t* syslog_object = amxd_dm_findf(syslog_get_dm(), "Syslog.");

    amxc_var_init(&var);
    amxc_var_set_type(&var, AMXC_VAR_ID_HTABLE);

    when_false_trace(syslog_dm_events_allowed(), exit, INFO, "syslog not ready yet ignoring");
    when_null_trace(syslog_object, exit, ERROR, "Could not get Syslog object");

    amxc_var_add_new_key_uint32_t(&var, "counter", ++changed_counter);

    amxd_object_emit_signal(syslog_object, "Changed", &var);
exit:
    amxc_var_clean(&var);
    SAH_TRACEZ_OUT(ME);
}

amxd_status_t syslog_dm_set_status(const char* status) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t trans;
    amxd_status_t res = amxd_status_unknown_error;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    res = amxd_trans_select_pathf(&trans, "Syslog.");
    when_failed_trace(res, exit, ERROR, "failed(%d) to select Syslog.", res);

    res = amxd_trans_set_value(cstring_t, &trans, "Status", status);
    when_failed_trace(res, exit, ERROR, "failed(%d) to set status change: %s", res, status);

    res = amxd_trans_apply(&trans, syslog_get_dm());
    when_failed_trace(res, exit, ERROR, "failed(%d) to apply status change: %s", res, status);
exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return res;
}

bool syslog_dm_get_enable(void) {
    SAH_TRACEZ_IN(ME);
    bool enable = false;
    amxd_object_t* syslog_object = NULL;

    syslog_object = amxd_dm_findf(syslog_get_dm(), "Syslog.");
    when_null_trace(syslog_object, exit, ERROR, "could not get Syslog object");

    enable = amxd_object_get_bool(syslog_object, "Enable", NULL);

exit:
    SAH_TRACEZ_OUT(ME);
    return enable;
}

void _syslog_changed(UNUSED const char* const event_name,
                     const amxc_var_t* const event_data,
                     UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    uint32_t counter = GET_UINT32(event_data, "counter");
    when_false(counter >= changed_counter || counter == 0, exit);
    changed_counter = 0;

    syslog_update();
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static const char* str_null_as_empty(const char* str) {
    return str ? str : "";
}

void _syslog_update_config(const char* const event_name,
                           const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "config updated by %s %s %s",
                    event_name,
                    GET_CHAR(event_data, "object"),
                    str_null_as_empty(GET_CHAR(event_data, "name")));

    syslog_dm_emit_changed();
    SAH_TRACEZ_OUT(ME);
}

amxd_status_t _Reload(UNUSED amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      UNUSED amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_ok;
    syslog_update();
    SAH_TRACEZ_OUT(ME);
    return status;
}

void _syslog_app_start(UNUSED const char* const event_name,
                       UNUSED const amxc_var_t* const event_data,
                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "app-start");
    syslog_start();
    SAH_TRACEZ_OUT(ME);
}

void _syslog_app_stop(UNUSED const char* const event_name,
                      UNUSED const amxc_var_t* const event_data,
                      UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_INFO(ME, "app-stop");
    syslog_stop();
    SAH_TRACEZ_OUT(ME);
}
