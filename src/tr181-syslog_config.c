/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "tr181-syslog.h"
#include "tr181-syslog_main.h"
#include "tr181-syslog_config_action.h"
#include "tr181-syslog_config_source.h"
#include "tr181-syslog_config_template.h"
#include "tr181-syslog_config_filter.h"
#include "tr181-syslog_config.h"

syslog_config_instances_t syslog_config_instances[] = {
    {.name = "Template", .parse = syslog_config_template_parse},
    {.name = "Filter", .parse = syslog_config_filter_parse},
    {.name = "Source", .parse = syslog_config_source_parse},
    {.name = "Action", .parse = syslog_config_action_parse},
    {0}
};

bool syslog_config_parse_instances(const char* template, syslog_config_parse_instance_t parse, amxc_var_t* config) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    amxd_object_t* instances = amxd_dm_findf(syslog_get_dm(), "Syslog.%s.", template);
    when_null_trace(instances, exit, ERROR, "Could not get %s object", template);

    amxd_object_for_each(instance, it, instances) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        when_null_trace(instance, next, ERROR, "Instance is NULL, should not happen");

        when_false_trace(parse(instance, config), exit,
                         WARNING, "failed to parse %s", template);
next:
        continue;
    }
    rv = true;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void syslog_config_parse_options(amxc_var_t* config) {
    SAH_TRACEZ_IN(ME);
    const char* timezone = NULL;
    const char* recv_timezone = NULL;
    const char* send_timezone = NULL;
    const char* dir_group = NULL;
    const char* dir_perm = NULL;
    const char* file_group = NULL;
    const char* file_perm = NULL;
    amxc_var_t* options = NULL;
    amxd_object_t* syslog_object = amxd_dm_findf(syslog_get_dm(), "Syslog.");

    when_null_trace(syslog_object, exit, ERROR, "could not get Syslog object");

    timezone = GET_CHAR(amxd_object_get_param_value(syslog_object, "TimeZone"), NULL);
    when_null_trace(timezone, exit, ERROR, "could not get Syslog.TimeZone");

    recv_timezone = GET_CHAR(amxd_object_get_param_value(syslog_object, "DefaultReceiveTimeZone"), NULL);
    when_null_trace(recv_timezone, exit, ERROR, "could not get Syslog.DefaultReceiveTimeZone");

    send_timezone = GET_CHAR(amxd_object_get_param_value(syslog_object, "DefaultSendTimeZone"), NULL);
    when_null_trace(send_timezone, exit, ERROR, "could not get Syslog.DefaultSendTimeZone");

    dir_group = GET_CHAR(amxd_object_get_param_value(syslog_object, "DirectoryGroup"), NULL);
    when_null_trace(send_timezone, exit, ERROR, "could not get Syslog.DirectoryGroup");

    dir_perm = GET_CHAR(amxd_object_get_param_value(syslog_object, "DirectoryPermissions"), NULL);
    when_null_trace(send_timezone, exit, ERROR, "could not get Syslog.DirectoryPermissions");

    file_group = GET_CHAR(amxd_object_get_param_value(syslog_object, "FileGroup"), NULL);
    when_null_trace(send_timezone, exit, ERROR, "could not get Syslog.FileGroup");

    file_perm = GET_CHAR(amxd_object_get_param_value(syslog_object, "FilePermissions"), NULL);
    when_null_trace(send_timezone, exit, ERROR, "could not get Syslog.FilePermissions");

    options = amxc_var_add_new_key(config, "options");
    amxc_var_set_type(options, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, options, "timezone", timezone);
    amxc_var_add_key(cstring_t, options, "recv-timezone", recv_timezone);
    amxc_var_add_key(cstring_t, options, "send-timezone", send_timezone);
    amxc_var_add_key(cstring_t, options, "dir-group", dir_group);
    amxc_var_add_key(cstring_t, options, "dir-perm", dir_perm);
    amxc_var_add_key(cstring_t, options, "file-group", file_group);
    amxc_var_add_key(cstring_t, options, "file-perm", file_perm);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

bool syslog_config_parse_dm(amxc_var_t* config) {
    SAH_TRACEZ_IN(ME);
    bool rv = true;
    amxc_var_t* destinations = NULL;
    amxc_var_set_type(config, AMXC_VAR_ID_HTABLE);

    destinations = amxc_var_add_new_key(config, "destinations");
    amxc_var_set_type(destinations, AMXC_VAR_ID_HTABLE);

    syslog_config_parse_options(config);

    for(syslog_config_instances_t* i = syslog_config_instances; i->name != NULL; i++) {
        if(i->parse == NULL) {
            continue;
        }
        if(!i->parse(config)) {
            SAH_TRACEZ_WARNING(ME, "failed to parse %s", i->name);
            rv = false;
        }
    }

    SAH_TRACEZ_OUT(ME);
    return rv;
}

void syslog_config_clean(void) {
    syslog_config_source_clean();
}
