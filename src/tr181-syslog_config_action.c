/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "tr181-syslog.h"
#include "tr181-syslog_utils.h"
#include "tr181-syslog_main.h"
#include "tr181-syslog_config.h"
#include "tr181-syslog_config_tls.h"
#include "tr181-syslog_config_action.h"

#define ACTION_PRINTF_FMT "Action" OBJECT_REF_PRINTF_FMT
#define ACTION_PRINTF_ARG(act) OBJECT_REF_PRINTF_ARG(act)

static const char* syslog_config_ref_clip_prefix(const char* ref) {
    SAH_TRACEZ_IN(ME);
    const char* const device_prefix = "Device.";
    const char* clipped = ref;
    when_str_empty(ref, exit);
    when_false(strncmp(clipped, device_prefix, strlen(device_prefix)) == 0, exit);
    clipped += strlen(device_prefix);
exit:
    SAH_TRACEZ_OUT(ME);
    return clipped;
}

static void syslog_config_action_set_logremote_status(amxd_object_t* act_inst, const char* status) {
    amxd_object_t* remote = NULL;
    const char* status_name = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_null_trace(act_inst, exit, ERROR, "Can't set status to %s if null Action given", status);

    remote = amxd_object_get_child(act_inst, "LogRemote");
    when_null_trace(remote, exit, ERROR, "Failed to get actions LogRemote");

    status_name = syslog_get_prefixed_name("Status");
    when_str_empty_trace(status_name, exit, ERROR, "Failed to get '${prefix_}Status'");

    when_failed_trace(amxd_trans_select_object(&trans, remote), exit, ERROR, "Failed to select transaction object");

    when_failed_trace(amxd_trans_set_cstring_t(&trans, status_name, status), exit, ERROR, "Failed to set LogRemote.%s='%s'", status_name, status);

    when_failed_trace(amxd_trans_apply(&trans, syslog_get_dm()), exit, ERROR, "Failed to set LogRemote status to %s", status);
exit:
    amxd_trans_clean(&trans);
    return;
}

static const char* syslog_config_lookup_ref_id(const char* ref) {
    SAH_TRACEZ_IN(ME);
    const char* id = "";
    const amxc_var_t* id_var = NULL;
    amxd_object_t* obj = amxd_dm_findf(syslog_get_dm(), "%s.", syslog_config_ref_clip_prefix(ref));
    when_null(obj, exit);
    id_var = amxd_object_get_param_value(obj, "Alias");
    when_null(id_var, exit);
    id = GET_CHAR(id_var, NULL);
exit:
    SAH_TRACEZ_OUT(ME);
    return id;
}

static bool syslog_config_action_parse_refs(amxd_object_t* act_inst, const char* in_name, const char* out_name, amxc_var_t* config_out, size_t* count) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    amxc_var_t* config_refs = NULL;
    const amxc_var_t* act_ref = amxd_object_get_param_value(act_inst, in_name);
    uint32_t type_id;
    amxc_var_t refs;
    amxc_var_init(&refs);

    when_str_empty(GET_CHAR(act_ref, NULL), exit);
    type_id = amxc_var_type_of(act_ref);

    config_refs = amxc_var_add_new_key(config_out, out_name);
    if(type_id == AMXC_VAR_ID_CSTRING) {
        const char* id = syslog_config_lookup_ref_id(GET_CHAR(act_ref, NULL));
        amxc_var_set(cstring_t, config_refs, id);
    } else if(type_id == AMXC_VAR_ID_CSV_STRING) {
        when_failed_trace(amxc_var_convert(&refs, amxd_object_get_param_value(act_inst, in_name), AMXC_VAR_ID_LIST),
                          exit, ERROR, "Failed to convert %s", in_name);
        amxc_var_set_type(config_refs, AMXC_VAR_ID_LIST);

        amxc_var_for_each(ref, &refs) {
            const char* id = syslog_config_lookup_ref_id(GET_CHAR(ref, NULL));
            if(EMPTY_STRING(id)) {
                continue;
            }
            amxc_var_add(cstring_t, config_refs, id);
        }

        if(count != NULL) {
            *count = amxc_llist_size(amxc_var_get_const_amxc_llist_t(config_refs));
        }
    }

    rv = true;
exit:
    amxc_var_clean(&refs);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool syslog_config_action_parse_logfile(amxd_object_t* logfile, syslog_object_ref_t* act, amxc_var_t* act_dest, amxc_var_t* cfg_dest) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    const char* filepath = NULL;
    const char* dest_id_str = NULL;
    amxc_var_t* dest = NULL;
    amxc_string_t dest_id;
    amxc_string_init(&dest_id, 0);

    filepath = file_uri_to_file(GET_CHAR(amxd_object_get_param_value(logfile, "FilePath"), NULL));
    when_str_empty_trace(filepath, exit, WARNING,
                         ACTION_PRINTF_FMT " has no valid filepath",
                         ACTION_PRINTF_ARG(act));

    amxc_string_setf(&dest_id, "action_%d_file", act->index);
    dest_id_str = amxc_string_get(&dest_id, 0);

    dest = amxc_var_add_new_key(cfg_dest, dest_id_str);
    amxc_var_set_type(dest, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, dest, "type", "file");
    amxc_var_add_key(cstring_t, dest, "filepath", filepath);
    if(!syslog_config_action_parse_refs(act->obj, "TemplateRef", "template", dest, NULL)) {
        amxc_var_add_key(cstring_t, dest, "template", "");
    }

    amxc_var_add(cstring_t, act_dest, dest_id_str);

    rv = true;
exit:
    SAH_TRACEZ_INFO(ME, "parsed " ACTION_PRINTF_FMT " rv=%d file=%s",
                    ACTION_PRINTF_ARG(act), rv, filepath);
    amxc_string_clean(&dest_id);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool syslog_config_action_parse_logremote(amxd_object_t* logremote,
                                                 syslog_object_ref_t* act,
                                                 amxc_var_t* act_dest,
                                                 amxc_var_t* cfg_dest,
                                                 const char** log_remote_status) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    const char* address = NULL;
    const char* protocol = NULL;
    const char* dest_id_str = NULL;
    const char* certificate = NULL;
    const char* ca_certificate = NULL;
    uint16_t port = 0;
    bool tls_verify = false;
    bool structured = false;
    amxc_var_t* dest = NULL;
    amxc_var_t* tls = NULL;
    amxc_string_t dest_id;
    amxc_string_init(&dest_id, 0);

    address = GET_CHAR(amxd_object_get_param_value(logremote, "Address"), NULL);
    when_str_empty_trace(address, exit, WARNING, ACTION_PRINTF_FMT " has no valid address",
                         ACTION_PRINTF_ARG(act));

    protocol = GET_CHAR(amxd_object_get_param_value(logremote, "Protocol"), NULL);
    when_str_empty_trace(protocol, exit, WARNING, ACTION_PRINTF_FMT " has no valid protocol",
                         ACTION_PRINTF_ARG(act));

    if(strcmp(protocol, "TLS") == 0) {
        when_false_status(syslog_security_is_available(), exit, rv = true);
        tls_verify = amxd_object_get_bool(logremote, "PeerVerify", NULL);
        certificate = GET_CHAR(amxd_object_get_param_value(logremote, "Certificate"), NULL);
        ca_certificate = GET_CHAR(amxd_object_get_param_value(logremote, syslog_get_prefixed_name("CACertificate")), NULL);
    }

    port = amxd_object_get_uint16_t(logremote, "Port", NULL);
    when_false_trace(port != 0, exit, ERROR, "Port can't be 0");

    structured = amxd_object_get_bool(act->obj, "StructuredData", NULL);

    amxc_string_setf(&dest_id, "action_%d_network", act->index);
    dest_id_str = amxc_string_get(&dest_id, 0);

    dest = amxc_var_add_new_key(cfg_dest, dest_id_str);
    amxc_var_set_type(dest, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, dest, "type", "network");
    amxc_var_add_key(cstring_t, dest, "address", address);
    amxc_var_add_key(uint16_t, dest, "port", port);
    amxc_var_add_key(bool, dest, "structured", structured);
    amxc_var_add_key(cstring_t, dest, "protocol", protocol);
    if(!syslog_config_action_parse_refs(act->obj, "TemplateRef", "template", dest, NULL)) {
        amxc_var_add_key(cstring_t, dest, "template", "");
    }

    if(strcmp(protocol, "TLS") == 0) {
        tls = amxc_var_add_new_key(dest, "tls");
        amxc_var_set_type(tls, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(bool, tls, "verify", tls_verify);

        if(!EMPTY_STRING(certificate)) {
            when_false_trace(syslog_config_tls_parse_certificate(certificate, amxc_var_add_new_key(tls, "certificate"), SYSLOG_TLS_CERT_PRIVATE),
                             exit, ERROR, "Failed to parse certificate (%s) of "ACTION_PRINTF_FMT, certificate, ACTION_PRINTF_ARG(act));
        }

        if(!EMPTY_STRING(ca_certificate)) {
            when_false_trace(syslog_config_tls_parse_certificate(ca_certificate, amxc_var_add_new_key(tls, "ca_certificate"), SYSLOG_TLS_CERT_PUBLIC),
                             exit, ERROR, "Failed to parse CA certificate (%s) of "ACTION_PRINTF_FMT, ca_certificate, ACTION_PRINTF_ARG(act));
        }
    }

    amxc_var_add(cstring_t, act_dest, dest_id_str);

    rv = true;
exit:
    *log_remote_status = rv ? "Enabled" : "Error";
    SAH_TRACEZ_INFO(ME, "parsed "ACTION_PRINTF_FMT " rv=%d",
                    ACTION_PRINTF_ARG(act), rv);
    amxc_string_clean(&dest_id);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool syslog_config_action_parse_destinations(syslog_object_ref_t* act, amxc_var_t* action, amxc_var_t* config, const char** log_remote_status) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    amxd_object_t* logfile = amxd_object_get_child(act->obj, "LogFile");
    amxd_object_t* logremote = amxd_object_get_child(act->obj, "LogRemote");
    bool logfile_enable = amxd_object_get_bool(logfile, "Enable", NULL);
    bool logremote_enable = amxd_object_get_bool(logremote, "Enable", NULL);
    amxc_var_t* cfg_dest = GET_ARG(config, "destinations");
    amxc_var_t* act_dest = amxc_var_add_new_key(action, "destinations");
    amxc_var_set_type(act_dest, AMXC_VAR_ID_LIST);

    when_null_trace(logfile, exit, ERROR, ACTION_PRINTF_FMT " missing LogFile template", ACTION_PRINTF_ARG(act));
    when_null_trace(logremote, exit, ERROR, ACTION_PRINTF_FMT " missing LogRemote object", ACTION_PRINTF_ARG(act));
    when_false_trace(logfile_enable || logremote_enable, exit, WARNING, ACTION_PRINTF_FMT " has no destination", ACTION_PRINTF_ARG(act));

    if(logfile_enable) {
        rv |= syslog_config_action_parse_logfile(logfile, act, act_dest, cfg_dest);
    }
    if(logremote_enable) {
        rv |= syslog_config_action_parse_logremote(logremote, act, act_dest, cfg_dest, log_remote_status);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static bool syslog_config_action_parse_instance(amxd_object_t* act_inst, amxc_var_t* config) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    syslog_object_ref_t act = syslog_object_get_ref(act_inst);
    size_t sources = 0;
    amxc_var_t action;
    const char* log_remote_status = "Disabled";

    amxc_var_init(&action);
    amxc_var_set_type(&action, AMXC_VAR_ID_HTABLE);

    when_str_empty_trace(act.alias, exit, ERROR, "source has no Alias");

    when_false_trace(syslog_config_action_parse_refs(act.obj, "SourceRef", "sources", &action, &sources),
                     exit, ERROR, "failed to parse action %s source reference(s)", act.alias);
    when_false_trace(sources > 0, exit, WARNING, "action %s has no sources", act.alias);
    syslog_config_action_parse_refs(act.obj, "FilterRef", "filters", &action, NULL);
    syslog_config_action_parse_refs(act.obj, "TemplateRef", "template", &action, NULL);

    rv = true;
    when_false_trace(syslog_config_action_parse_destinations(&act, &action, config, &log_remote_status),
                     exit, WARNING, ACTION_PRINTF_FMT " has no destinations", ACTION_PRINTF_ARG(&act));

    amxc_var_set_key(GET_ARG(config, "actions"), act.alias, &action, AMXC_VAR_FLAG_COPY);
exit:
    syslog_config_action_set_logremote_status(act_inst, log_remote_status);
    amxc_var_clean(&action);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

bool syslog_config_action_parse(amxc_var_t* config) {
    SAH_TRACEZ_IN(ME);
    bool res = false;
    amxc_var_t* actions = amxc_var_add_new_key(config, "actions");
    amxc_var_set_type(actions, AMXC_VAR_ID_HTABLE);

    res = syslog_config_parse_instances("Action", syslog_config_action_parse_instance, config);
    SAH_TRACEZ_OUT(ME);
    return res;
}
