/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_common.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "tr181-syslog.h"
#include "tr181-syslog_hostname.h"

#define DEVICEINFO_SYSTEM "System"
#define HOSTNAME_PARAM "HostName"


static void hostname_changed_cb(UNUSED const char* const signal,
                                UNUSED const amxc_var_t* const date,
                                UNUSED void* const priv) {
    syslog_update();
}

static void system_available_cb(UNUSED const char* const signal,
                                UNUSED const amxc_var_t* const date,
                                UNUSED void* const priv) {
    int rv = -1;
    amxb_bus_ctx_t* ctx = amxb_be_who_has(DEVICEINFO_SYSTEM ".");
    when_null_trace(ctx, exit, ERROR, "Failed to get bus ctx of " DEVICEINFO_SYSTEM ".");

    SAH_TRACEZ_INFO(ME, DEVICEINFO_SYSTEM ". available");

    rv = amxb_subscribe(ctx,
                        DEVICEINFO_SYSTEM ".",
                        "(notification == 'dm:object-changed') && (contains('parameters." HOSTNAME_PARAM "'))",
                        hostname_changed_cb, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to subscribe to System." HOSTNAME_PARAM " changes");
exit:
    return;
}

int syslog_hostname_start(void) {
    int rv = -1;
    rv = amxp_slot_connect_filtered(NULL, "^wait:" DEVICEINFO_SYSTEM "\\.$", NULL, system_available_cb, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to connect to wait:" DEVICEINFO_SYSTEM ". %d", rv);
    rv = amxb_wait_for_object(DEVICEINFO_SYSTEM ".");
    when_failed_trace(rv, exit, ERROR, "Failed to wait for " DEVICEINFO_SYSTEM ".: %d", rv);
exit:
    return rv;
}

void syslog_hostname_stop(void) {
    amxp_slot_disconnect_all(system_available_cb);
}
