/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_common.h>
#include <amxb/amxb.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <netmodel/client.h>

#include "tr181-syslog.h"
#include "tr181-syslog_utils.h"
#include "tr181-syslog_main.h"
#include "tr181-syslog_dm.h"
#include "tr181-syslog_config.h"
#include "tr181-syslog_controller.h"
#include "tr181-syslog_hostname.h"
#include "tr181-syslog_vendor_log_file.h"

typedef struct {
    bool dm_valid;
    bool updated;
    char* controller;
} syslog_statuses_t;

typedef struct {
    amxm_module_t* mod;
    syslog_statuses_t status;
    bool netmodel_available;
    bool security_available;
} syslog_t;
static syslog_t syslog = {.mod = NULL, .status = {.updated = true, .controller = NULL}};

syslog_object_ref_t syslog_object_get_ref(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    syslog_object_ref_t ref = {
        .index = amxd_object_get_index(obj),
        .alias = GET_CHAR(amxd_object_get_param_value(obj, "Alias"), NULL),
        .obj = obj,
    };
    SAH_TRACEZ_OUT(ME);
    return ref;
}

static void syslog_status_update(void) {
    SAH_TRACEZ_IN(ME);
    const char* status = "Error";
    when_false_status(syslog_dm_get_enable(), exit, status = "Disabled");
    when_false(syslog.status.dm_valid, exit);
    when_false(syslog.status.updated, exit);
    when_str_empty(syslog.status.controller, exit);
    status = syslog.status.controller;
exit:
    syslog_dm_set_status(status);
    SAH_TRACEZ_OUT(ME);
}

void syslog_update(void) {
    SAH_TRACEZ_IN(ME);
    bool enable = syslog_dm_get_enable();
    amxc_var_t config;
    amxc_var_init(&config);

    syslog.status.dm_valid = syslog_config_parse_dm(&config);
    if(!syslog.status.dm_valid) {
        SAH_TRACEZ_ERROR(ME, "something was misconfigured");
        // Keep going so we can still start syslog with the partial or default config
    }

    syslog.status.updated = syslog_controller_update(enable, &config);
    when_false_trace(syslog.status.updated, exit, ERROR, "failed to update syslog controller");
exit:
    syslog_status_update();
    amxc_var_clean(&config);
    SAH_TRACEZ_OUT(ME);
}

void syslog_start(void) {
    SAH_TRACEZ_IN(ME);
    syslog_controller_load();
    syslog_hostname_start();
    syslog_vendor_log_file_start();
    syslog_update();
    SAH_TRACEZ_OUT(ME);
}

void syslog_stop(void) {
    SAH_TRACEZ_IN(ME);
    syslog_vendor_log_file_stop();
    syslog_hostname_stop();
    syslog_controller_unload();
    SAH_TRACEZ_OUT(ME);
}

static int syslog_set_controller_status(UNUSED const char* const function_name,
                                        amxc_var_t* args,
                                        UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    free(syslog.status.controller);
    syslog.status.controller = strdup(GET_CHAR(args, NULL));
    syslog_status_update();
    SAH_TRACEZ_OUT(ME);
    return 0;
}

bool syslog_netmodel_is_available(void) {
    return syslog.netmodel_available;
}

bool syslog_security_is_available(void) {
    return syslog.security_available;
}

static void syslog_dependencies_update(void) {
    when_false(syslog.netmodel_available, exit);
    when_false(syslog.security_available, exit);
    syslog_update();
exit:
    return;
}

static void syslog_netmodel_turned_available(UNUSED const char* const event_name,
                                             UNUSED const amxc_var_t* const event_data,
                                             UNUSED void* const priv) {
    if(!syslog.netmodel_available) {
        SAH_TRACEZ_INFO(ME, "NetModel. available");
        netmodel_initialize();
    }
    syslog.netmodel_available = true;

    syslog_dependencies_update();
}

static void syslog_security_turned_available(UNUSED const char* const event_name,
                                             UNUSED const amxc_var_t* const event_data,
                                             UNUSED void* const priv) {
    if(!syslog.security_available) {
        SAH_TRACEZ_INFO(ME, "Security. available");
    }
    syslog.security_available = true;

    syslog_dependencies_update();
}

static void syslog_dependencies_start_watching_availability(void) {
    amxp_slot_connect_filtered(NULL, "^wait:NetModel\\.$", NULL, syslog_netmodel_turned_available, NULL);
    amxb_wait_for_object("NetModel.");
    amxp_slot_connect_filtered(NULL, "^wait:Security\\.$", NULL, syslog_security_turned_available, NULL);
    amxb_wait_for_object("Security.");
}

static void syslog_dependencies_stop_watching_availability(void) {
    amxp_slot_disconnect_all(syslog_netmodel_turned_available);
    amxp_slot_disconnect_all(syslog_security_turned_available);
}

int syslog_init(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    rv = amxm_module_register(&syslog.mod, NULL, "tr181");
    when_failed_trace(rv, exit, ERROR, "failed to register syslog as module");
    amxm_module_add_function(syslog.mod, "controller-status", syslog_set_controller_status);
    when_failed_trace(rv, exit, ERROR, "failed to add set controller status syslog function");

    syslog_dependencies_start_watching_availability();
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int syslog_teardown(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    syslog_dependencies_stop_watching_availability();

    syslog_controller_unload();
    rv = amxm_module_deregister(&syslog.mod);
    when_failed_trace(rv, exit, ERROR, "failed to deregister syslog as module");

    syslog_config_clean();

    if(syslog.netmodel_available) {
        netmodel_cleanup();
    }
    syslog.netmodel_available = false;
    syslog.security_available = false;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
