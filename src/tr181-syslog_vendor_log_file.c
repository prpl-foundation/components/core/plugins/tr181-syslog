/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "tr181-syslog.h"
#include "tr181-syslog_utils.h"
#include "tr181-syslog_main.h"
#include "tr181-syslog_vendor_log_file.h"

static void syslog_vendor_log_file_ref_set(amxd_object_t* object, const char* ref) {
    amxd_status_t rv = amxd_status_unknown_error;
    amxc_string_t full_ref;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxc_string_init(&full_ref, 0);

    if(!EMPTY_STRING(ref)) {
        amxc_string_setf(&full_ref, "Device.%s", ref);
    }

    rv = amxd_trans_select_object(&trans, object);
    when_failed_trace(rv, exit, ERROR, "Failed to select object for transaction (%d)", rv);
    rv = amxd_trans_set_cstring_t(&trans, "VendorLogFileRef", amxc_string_get(&full_ref, 0));
    when_failed_trace(rv, exit, ERROR, "Failed to set VendorLogFileRef param (%d)", rv);
    rv = amxd_trans_apply(&trans, syslog_get_dm());
    when_failed_trace(rv, exit, ERROR, "Transaction to set VendorLogFileRef param failed (%d)", rv);
exit:
    amxc_string_clean(&full_ref);
    amxd_trans_clean(&trans);
}

static void syslog_vendor_log_file_update(amxd_object_t* object,
                                          UNUSED amxc_var_t* vendor_log_files) {
    const char* file_path = GET_CHAR(amxd_object_get_param_value(object, "FilePath"), NULL);

    when_str_empty(file_path, exit);

    amxc_var_for_each(vlf, vendor_log_files) {
        const char* vlf_file = GET_CHAR(vlf, "Name");
        if(EMPTY_STRING(vlf_file)) {
            continue;
        }

        if(!streq(vlf_file, file_path)) {
            continue;
        }

        syslog_vendor_log_file_ref_set(object, amxc_var_key(vlf));
        goto exit;
    }
    syslog_vendor_log_file_ref_set(object, "");
exit:
    return;
}

static int syslog_vendor_log_file_update_cb(UNUSED amxd_object_t* object,
                                            amxd_object_t* mobject,
                                            void* priv) {
    amxc_var_t* vendor_log_files = priv;
    syslog_vendor_log_file_update(mobject, vendor_log_files);
    return 0;
}

static void syslog_vendor_log_file_update_all(amxb_bus_ctx_t* ctx) {
    int rv = -1;
    amxd_object_t* actions = amxd_dm_findf(syslog_get_dm(), "Syslog.Action.");
    amxc_var_t* vendor_log_files = NULL;
    amxc_var_t get_ret;

    amxc_var_init(&get_ret);

    when_null_trace(actions, exit, ERROR, "Failed to lookup Syslog.Action.");

    rv = amxb_get(ctx, "DeviceInfo.VendorLogFile.*.", 1, &get_ret, 5);
    when_failed_trace(rv, exit, ERROR, "Failed to lookup \"DeviceInfo.VendorLogFile.*.\" (%d)", rv);

    vendor_log_files = amxc_var_get_first(&get_ret);
    when_null_trace(vendor_log_files, exit, ERROR, "amxb_get return value was empty");

    when_null(amxc_var_get_first(vendor_log_files), exit);

    amxd_object_for_all(actions, "*.LogFile.", syslog_vendor_log_file_update_cb, vendor_log_files);
exit:
    amxc_var_clean(&get_ret);
}

static void deviceinfo_vendor_log_file_instance_changed_cb(UNUSED const char* const signal,
                                                           UNUSED const amxc_var_t* const data,
                                                           UNUSED void* const priv) {
    amxb_bus_ctx_t* ctx = amxb_be_who_has("DeviceInfo.");
    when_null_trace(ctx, exit, ERROR, "Failed to get bus ctx of DeviceInfo.");

    syslog_vendor_log_file_update_all(ctx);
exit:
    return;
}

static void deviceinfo_available_cb(UNUSED const char* const signal,
                                    UNUSED const amxc_var_t* const data,
                                    UNUSED void* const priv) {
    int rv = -1;
    amxb_bus_ctx_t* ctx = amxb_be_who_has("DeviceInfo.");
    when_null_trace(ctx, exit, ERROR, "Failed to get bus ctx of DeviceInfo.");

    SAH_TRACEZ_INFO(ME, "DeviceInfo. available");

    rv = amxb_subscribe(ctx,
                        "DeviceInfo.VendorLogFile",
                        "(notification == 'dm:instance-added') || (notification == 'dm:object-changed')",
                        deviceinfo_vendor_log_file_instance_changed_cb, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to subscribe to DeviceInfo.VendorLogFile changes (%d)", rv);

    syslog_vendor_log_file_update_all(ctx);
exit:
    return;
}

int syslog_vendor_log_file_start(void) {
    int rv = -1;
    rv = amxp_slot_connect_filtered(NULL, "^wait:DeviceInfo\\.$", NULL, deviceinfo_available_cb, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to connect to wait:DeviceInfo. (%d)", rv);
    amxb_wait_for_object("DeviceInfo.");
exit:
    return rv;
}

void syslog_vendor_log_file_stop(void) {
    amxp_slot_disconnect_all(deviceinfo_vendor_log_file_instance_changed_cb);
    amxp_slot_disconnect_all(deviceinfo_available_cb);
}
