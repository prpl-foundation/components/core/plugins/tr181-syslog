/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "tr181-syslog.h"
#include "tr181-syslog_main.h"
#include "tr181-syslog_config.h"
#include "tr181-syslog_config_filter.h"


static bool syslog_config_filter_parse_instance(amxd_object_t* instance, amxc_var_t* filters) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    const char* id = NULL;
    const char* facility = NULL;
    const char* severity = NULL;
    const char* severity_cmp = NULL;
    const char* severity_cmp_act = NULL;
    const char* pattern = NULL;
    amxc_var_t* filter = NULL;

    id = GET_CHAR(amxd_object_get_param_value(instance, "Alias"), NULL);
    when_str_empty_trace(id, exit, ERROR, "filter has no Id/Alias");

    facility = GET_CHAR(amxd_object_get_param_value(instance, "FacilityLevel"), NULL);
    when_str_empty_trace(facility, exit, ERROR, "filter %s FacilityLevel empty", id);

    severity = GET_CHAR(amxd_object_get_param_value(instance, "Severity"), NULL);
    when_str_empty_trace(severity, exit, ERROR, "filter %s Severity empty", id);

    severity_cmp = GET_CHAR(amxd_object_get_param_value(instance, "SeverityCompare"), NULL);
    when_str_empty_trace(severity_cmp, exit, ERROR, "filter %s SeverityCompare empty", id);

    severity_cmp_act = GET_CHAR(amxd_object_get_param_value(instance, "SeverityCompareAction"), NULL);
    when_str_empty_trace(severity_cmp_act, exit, ERROR, "filter %s SeverityCompareAction empty", id);

    pattern = GET_CHAR(amxd_object_get_param_value(instance, "PatternMatch"), NULL);
    when_null_trace(pattern, exit, ERROR, "filter %s PatternMatch empty", id);

    filter = amxc_var_add_new_key(filters, id);
    when_null_trace(filter, exit, ERROR, "failed to add new filter: %s", id);
    amxc_var_set_type(filter, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, filter, "facility", facility);
    amxc_var_add_key(cstring_t, filter, "severity", severity);
    amxc_var_add_key(cstring_t, filter, "severity_cmp", severity_cmp);
    amxc_var_add_key(cstring_t, filter, "severity_cmp_act", severity_cmp_act);
    amxc_var_add_key(cstring_t, filter, "pattern", pattern);

    rv = true;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

bool syslog_config_filter_parse(amxc_var_t* config) {
    SAH_TRACEZ_IN(ME);
    bool res = false;
    amxc_var_t* filters = amxc_var_add_new_key(config, "filters");
    amxc_var_set_type(filters, AMXC_VAR_ID_HTABLE);

    res = syslog_config_parse_instances("Filter", syslog_config_filter_parse_instance, filters);
    SAH_TRACEZ_OUT(ME);
    return res;
}
