/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "tr181-syslog.h"
#include "tr181-syslog_main.h"
#include "tr181-syslog_controller.h"

typedef struct {
    bool loaded;
    amxm_module_t* mod;
    amxm_shared_object_t* so;
} syslog_controller_t;
static syslog_controller_t syslog_controller;

static const char* syslog_controller_name(void) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* syslog_obj = amxd_dm_get_object(syslog_get_dm(), "Syslog");
    const amxc_var_t* controller = amxd_object_get_param_value(syslog_obj, "Controller");
    SAH_TRACEZ_OUT(ME);
    return GET_CHAR(controller, NULL);
}

static const char* syslog_mod_dir(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* mod_dir = amxo_parser_get_config(syslog_get_parser(), "mod-dir");
    SAH_TRACEZ_OUT(ME);
    return GET_CHAR(mod_dir, NULL);
}

static int syslog_controller_execute(const char* func, amxc_var_t* args, amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = amxm_so_execute_function(syslog_controller.so, syslog_controller_name(), func, args, ret);
    SAH_TRACEZ_INFO(ME, "executed syslog module function(%s) %d", func, rv);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

bool syslog_controller_load(void) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    const char* controller = syslog_controller_name();
    const char* mod_dir = syslog_mod_dir();
    amxc_string_t so_path;
    amxc_string_init(&so_path, 0);

    when_str_empty_trace(controller, exit, ERROR, "No syslog controller configured!");
    when_str_empty_trace(mod_dir, exit, ERROR, "No syslog controller mod_dir configured!");

    amxc_string_setf(&so_path, "%s/%s.so", mod_dir, controller);

    when_failed_trace(amxm_so_open(&syslog_controller.so, "syslog", amxc_string_get(&so_path, 0)),
                      exit, ERROR, "failed to load controller(%s)!", controller);

    SAH_TRACEZ_INFO(ME, "Loaded module[%s]", controller);
    rv = true;
exit:
    amxc_string_clean(&so_path);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

bool syslog_controller_unload(void) {
    SAH_TRACEZ_IN(ME);
    if(syslog_controller.so != NULL) {
        amxm_so_close(&syslog_controller.so);
    }
    SAH_TRACEZ_OUT(ME);
    return true;
}

bool syslog_controller_update(bool enable, amxc_var_t* config) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "enable", enable);
    amxc_var_set_key(&args, "config", config, AMXC_VAR_FLAG_COPY);

    rv = syslog_controller_execute("update", &args, &ret);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv == 0;
}

bool syslog_controller_is_running(void) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&ret, AMXC_VAR_ID_BOOL);

    when_null(syslog_controller.so, exit);
    when_failed(syslog_controller_execute("is-running", &args, &ret), exit);

    rv = GET_BOOL(&ret, NULL);
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return rv;
}
