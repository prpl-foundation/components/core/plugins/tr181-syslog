/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "tr181-syslog.h"
#include "tr181-syslog_main.h"
#include "tr181-syslog_config.h"
#include "tr181-syslog_config_tls.h"

typedef struct {
    bool enabled;
    const char* dir;
    const char* file;
    const char* serial;
} security_certificate_t;
static security_certificate_t security_certificate_zeroed;

static bool syslog_config_tls_parse_private_certificate(amxc_var_t* cfg_cert, const char* cert_file) {
    bool rv = false;
    int replacements = 0;
    amxc_string_t key_file;

    amxc_string_init(&key_file, 0);
    amxc_string_set(&key_file, cert_file);

    replacements = amxc_string_replace(&key_file, ".crt", ".key", 2);
    when_false_trace(replacements == 1, exit, ERROR, "Not a know certificate extension to get key from: %s %d", cert_file, replacements);

    amxc_var_push(amxc_string_t, amxc_var_add_new_key(cfg_cert, "key"), &key_file);
    rv = true;
exit:
    amxc_string_clean(&key_file);
    return rv;
}

bool syslog_config_tls_parse_certificate(const char* obj_path, amxc_var_t* cfg_cert, syslog_config_tls_certificate_type_t type) {
    bool rv = false;
    int res = -1;
    amxb_bus_ctx_t* bus_ctx = amxb_be_who_has("Security.");
    security_certificate_t cert = security_certificate_zeroed;
    amxc_var_t* obj = NULL;
    amxc_var_t ret;
    amxc_string_t dotted_path_build;
    const char* dotted_path = NULL;

    amxc_var_init(&ret);

    amxc_string_init(&dotted_path_build, 0);
    amxc_string_setf(&dotted_path_build, "%s.", obj_path);
    dotted_path = amxc_string_get(&dotted_path_build, 0);

    when_null_trace(cfg_cert, exit, ERROR, "No certificate config var provided");
    when_null_trace(bus_ctx, exit, ERROR, "No bus ctx found for Security.");

    res = amxb_get(bus_ctx, dotted_path, 0, &ret, 5);
    when_failed_trace(res, exit, ERROR, "Failed(%d) to lookup: %s", res, dotted_path);

    obj = GETP_ARG(&ret, "0.0.");
    when_null_trace(obj, exit, ERROR, "Failed to get first item from get result: %s", dotted_path);

    cert.enabled = GET_BOOL(obj, "Enable");
    cert.dir = GET_CHAR(obj, "CAPath");
    cert.file = GET_CHAR(obj, "CAFileName");

    when_false(cert.enabled, exit);
    when_str_empty_trace(cert.dir, exit, ERROR, "Certificate path empty: %s", dotted_path);
    when_str_empty_trace(cert.file, exit, ERROR, "Certificate file empty: %s", dotted_path);

    amxc_var_set_type(cfg_cert, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, cfg_cert, "enabled", cert.enabled);
    amxc_var_add_key(cstring_t, cfg_cert, "dir", cert.dir);
    amxc_var_add_key(cstring_t, cfg_cert, "file", cert.file);

    if(type == SYSLOG_TLS_CERT_PRIVATE) {
        when_false(syslog_config_tls_parse_private_certificate(cfg_cert, cert.file), exit);
    }

    rv = true;
exit:
    amxc_string_clean(&dotted_path_build);
    amxc_var_clean(&ret);
    return rv;
}

