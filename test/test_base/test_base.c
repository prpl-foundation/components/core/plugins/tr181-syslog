/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>

#include "tr181-syslog_controller.h"

#include "../mocks/mock.h"
#include "../common/test_utils.h"
#include "../common/test_mod_dummy.h"
#include "test_base.h"

static void remove_objects_matching_path(const char* path) {
    amxc_var_t ret;
    amxc_var_init(&ret);

    amxb_del(amxut_bus_ctx(), path, 0, NULL, &ret, 10);

    amxc_var_clean(&ret);
}

static amxd_object_t* syslog_object(void) {
    return amxd_dm_findf(amxut_bus_dm(), "Syslog.");
}

void test_syslog_can_be_started_and_stopped(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    assert_true(syslog_controller_is_running());

    test_mod_dummy_config_equals("../odl/test_empty.json", "start_stop");
}

void test_syslog_can_be_disabled_and_enabled(UNUSED void** state) {
    test_mod_dummy_expect_config_update();
    test_disable_object(syslog_object());
    amxut_bus_handle_events();
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Disabled");
    assert_false(syslog_controller_is_running());

    test_mod_dummy_expect_config_update();
    test_enable_object(syslog_object());
    amxut_bus_handle_events();
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    assert_true(syslog_controller_is_running());
}

void test_syslog_default_config_can_be_made(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    amxut_dm_param_equals_cstring_t("Syslog.Action.messages_remote.LogRemote", "Status", "Disabled");
    assert_true(syslog_controller_is_running());

    test_mod_dummy_config_equals("../odl/test_default.json", "defaults");
}

void test_syslog_basic_config_can_be_made(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    amxut_dm_param_equals_cstring_t("Syslog.Action.test_action.LogRemote", "Status", "Enabled");
    assert_true(syslog_controller_is_running());

    test_mod_dummy_config_equals("../odl/test_basic.json", "basic");
}

void test_syslog_can_handle_references_with_a_trailing_dot(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    amxut_dm_param_equals_cstring_t("Syslog.Action.test_action.LogRemote", "Status", "Enabled");
    assert_true(syslog_controller_is_running());

    test_mod_dummy_config_equals("../odl/test_basic.json", "basic");

    // Expect trailing dots removed
    amxut_dm_param_equals_cstring_t("Syslog.Source.test_source.Network", "Interface", "Device.Logical.Interface.1");
    amxut_dm_param_equals_csv_string_t("Syslog.Action.test_action", "SourceRef", "Device.Syslog.Source.1");
    amxut_dm_param_equals_cstring_t("Syslog.Action.test_action", "TemplateRef", "Device.Syslog.Template.1");
}

void test_syslog_basic_network_config_can_be_changed(UNUSED void** state) {
    test_mod_dummy_expect_config_update();
    amxut_dm_param_set(cstring_t, "Syslog.Source.1.Network.", "Interface", "Device.Logical.Interface.2");
    amxut_bus_handle_events();
    test_mod_dummy_config_equals("../odl/test_basic_lan.json", "basic_lan");

    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    amxut_dm_param_equals_cstring_t("Syslog.Action.test_action.LogRemote", "Status", "Enabled");
    assert_true(syslog_controller_is_running());

    test_mod_dummy_expect_config_update();
    amxut_dm_param_set(cstring_t, "Syslog.Source.1.Network.", "Interface", "");
    amxut_bus_handle_events();
    test_mod_dummy_config_equals("../odl/test_basic_without_network.json", "basic_without_network");

    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    amxut_dm_param_equals_cstring_t("Syslog.Action.test_action.LogRemote", "Status", "Enabled");
    assert_true(syslog_controller_is_running());
}

void test_syslog_basic_config_with_tls_can_be_made(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    amxut_dm_param_equals_cstring_t("Syslog.Action.unverified_tls.LogRemote", "Status", "Enabled");
    amxut_dm_param_equals_cstring_t("Syslog.Action.verified_tls.LogRemote", "Status", "Enabled");
    amxut_dm_param_equals_cstring_t("Syslog.Action.mutual_tls.LogRemote", "Status", "Enabled");
    assert_true(syslog_controller_is_running());

    test_mod_dummy_config_equals("../odl/test_basic_with_tls.json", "basic_with_tls");
}

void test_syslog_basic_config_can_handle_invalid_logremotes(UNUSED void** state) {
    test_mod_dummy_expect_config_update();
    amxut_dm_param_set(cstring_t, "Syslog.Action.test_action.LogRemote", "Address", "");
    amxut_bus_handle_events();

    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    amxut_dm_param_equals_cstring_t("Syslog.Action.test_action.LogRemote", "Status", "Error");
    assert_true(syslog_controller_is_running());

    test_mod_dummy_config_equals("../odl/test_basic_without_logremote.json", "basic_invalid_logremote");
}

void test_syslog_basic_config_can_be_made_with_a_structured_remote(UNUSED void** state) {
    test_mod_dummy_expect_config_update();
    amxut_dm_param_set(bool, "Syslog.Action.test_action.", "StructuredData", true);
    amxut_dm_param_set(bool, "Syslog.Source.test_source.Network.", "StructuredData", true);
    amxut_bus_handle_events();

    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    amxut_dm_param_equals_cstring_t("Syslog.Action.test_action.LogRemote", "Status", "Enabled");
    assert_true(syslog_controller_is_running());

    test_mod_dummy_config_equals("../odl/test_basic_with_structured_remote.json", "basic_with_structured_remote");
}

void test_syslog_basic_config_can_be_removed(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    assert_true(syslog_controller_is_running());

    test_mod_dummy_expect_config_update();
    remove_objects_matching_path("Syslog.Template.*.");
    remove_objects_matching_path("Syslog.Action.*.");
    remove_objects_matching_path("Syslog.Filter.*.");
    remove_objects_matching_path("Syslog.Source.*.");

    amxut_bus_handle_events();
    test_mod_dummy_config_equals("../odl/test_empty.json", "start_stop");

    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    assert_true(syslog_controller_is_running());
}

void test_syslog_basic_filtered_config_can_be_made(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    assert_true(syslog_controller_is_running());

    test_mod_dummy_config_equals("../odl/test_filter.json", "filter");
}

void test_syslog_ip_config_can_be_loaded_later(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    assert_true(syslog_controller_is_running());
    test_mod_dummy_config_equals("../odl/test_basic_without_network.json", "basic_without_network");

    // Load NetModel.Intf. somewhere after start to simulate boot order availability
    test_mod_dummy_expect_config_update();
    make_dm_available("../odl/test_netmodel.odl", "NetModel.");
    make_dm_available("../odl/test_security.odl", "Security.");

    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    assert_true(syslog_controller_is_running());
    test_mod_dummy_config_equals("../odl/test_basic.json", "basic_with_network");
}

void test_syslog_reloads_if_hostname_changed(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    assert_true(syslog_controller_is_running());
    test_mod_dummy_config_equals("../odl/test_basic.json", "basic");

    test_mod_dummy_expect_config_update(); // Reload syslog controller to load new hostname
    amxut_dm_param_set(cstring_t, "System.", "HostName", "new-hostname");
    amxut_bus_handle_events();
}

void test_syslog_vendor_log_file_can_be_updated_at_boot(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    assert_true(syslog_controller_is_running());

    amxut_dm_param_equals(cstring_t, "Syslog.Action.test_action.LogFile", "VendorLogFileRef", "Device.DeviceInfo.VendorLogFile.1");
}

void test_syslog_vendor_log_file_can_be_updated_after_boot(UNUSED void** state) {
    amxut_dm_param_equals_cstring_t("Syslog.", "Status", "Enabled");
    assert_true(syslog_controller_is_running());

    // Start empty if no reference found
    amxut_dm_param_equals(cstring_t, "Syslog.Action.test_action.LogFile", "VendorLogFileRef", "");

    make_dm_available("../odl/test_deviceinfo.odl", "DeviceInfo.");
    amxut_dm_load_odl("../odl/test_deviceinfo_vendorlogfile.odl");
    amxut_bus_handle_events();

    amxut_dm_param_equals(cstring_t, "Syslog.Action.test_action.LogFile", "VendorLogFileRef", "Device.DeviceInfo.VendorLogFile.1");
}
