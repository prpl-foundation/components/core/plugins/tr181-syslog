/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_netmodel.h"

#include <setjmp.h>
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct {
    const char* address;
    const char* family;
    const char* flags;
    const char* netdev_name;
    const char* netmodel_intf;
} mock_netmodel_getAddrs_ip_t;

typedef struct {
    netmodel_callback_t handler;
    mock_netmodel_getAddrs_ip_t* ips;
} mock_netmodel_getAddrs_intf_t;

mock_netmodel_getAddrs_ip_t getAddr_ips_loopback[] = {
    {
        .address = "127.0.0.1",
        .family = "IPv4",
        .flags = "permanent",
        .netdev_name = "lo",
        .netmodel_intf = "ip-loopback",
    },
    {
        .address = "::1",
        .family = "IPv6",
        .flags = "permanent",
        .netdev_name = "lo",
        .netmodel_intf = "ip-loopback",
    },
    {
        0
    }
};

mock_netmodel_getAddrs_ip_t getAddr_ips_lan[] = {
    {
        .address = "192.168.1.1",
        .family = "IPv4",
        .flags = "permanent",
        .netdev_name = "br-lan",
        .netmodel_intf = "ip-lan",
    },
    {
        .address = "2a02:f0f0:f0:f0f0::1",
        .family = "IPv6",
        .flags = "permanent",
        .netdev_name = "br-lan",
        .netmodel_intf = "ip-lan",
    },
    {
        0
    }
};

typedef struct {
    mock_netmodel_getAddrs_intf_t loopback;
    mock_netmodel_getAddrs_intf_t lan;
} mock_netmodel_getAddrs_t;

typedef struct {
    void* init;
    mock_netmodel_getAddrs_t getAddrs;
} mock_netmodel_t;
static mock_netmodel_t mock_netmodel = {
    .getAddrs = {
        .loopback = {
            .handler = NULL,
            .ips = getAddr_ips_loopback,
        },
        .lan = {
            .handler = NULL,
            .ips = getAddr_ips_lan,
        },
    }
};

#define STR_HAS_PREFIX(str, prefix) (strncmp(str, prefix, strlen(prefix)) == 0)

bool __wrap_netmodel_initialize(void) {
    mock_netmodel.init = malloc(sizeof(void*)); // warn that netmodel is not cleaned by showing mem leak
    return true;
}

void __wrap_netmodel_cleanup(void) {
    free(mock_netmodel.init);
    return;
}

static void mock_load_getAddrs_intf(amxc_var_t* data, mock_netmodel_getAddrs_intf_t* interface) {
    for(mock_netmodel_getAddrs_ip_t* ip = interface->ips; ip->address != NULL; ip++) {
        amxc_var_t* var = amxc_var_add_new(data);
        amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, var, "Address", ip->address);
        amxc_var_add_key(cstring_t, var, "Family", ip->family);
        amxc_var_add_key(cstring_t, var, "Flags", ip->flags);
        amxc_var_add_key(cstring_t, var, "NetDevName", ip->netdev_name);
        amxc_var_add_key(cstring_t, var, "NetModelIntf", ip->netmodel_intf);
    }
}

netmodel_query_t* __wrap_netmodel_openQuery_getAddrs(const char* intf,
                                                     const char* subscriber,
                                                     UNUSED const char* flag,
                                                     const char* traverse,
                                                     netmodel_callback_t handler,
                                                     void* userdata) {
    mock_netmodel_getAddrs_intf_t* interface = NULL;
    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_LIST);

    assert_non_null(intf);
    assert_non_null(subscriber);
    assert_non_null(traverse);
    assert_non_null(handler);
    assert_true(strcmp(subscriber, "tr181-syslog") == 0);

    assert_true(STR_HAS_PREFIX(intf, "Device.IP.Interface.") || STR_HAS_PREFIX(intf, "Device.Logical.Interface."));

    if(strcmp(intf, "Device.IP.Interface.1") == 0) {
        interface = &mock_netmodel.getAddrs.loopback;
    } else if(strcmp(intf, "Device.Logical.Interface.1") == 0) {
        interface = &mock_netmodel.getAddrs.loopback;
    } else if(strcmp(intf, "Device.Logical.Interface.2") == 0) {
        interface = &mock_netmodel.getAddrs.lan;
    } else {
        fail_msg("No known interface given: %s", intf);
    }

    interface->handler = handler;
    mock_load_getAddrs_intf(&data, interface);
    handler("", &data, userdata);

    amxc_var_clean(&data);
    return malloc(sizeof(netmodel_query_t*));
}

void __wrap_netmodel_closeQuery(netmodel_query_t* query) {
    assert_non_null(query);
    free(query);
}

