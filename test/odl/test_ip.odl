%config {
    %global ip_intf_lo = "Device.IP.Interface.1.";
}

%define {
    /**
     * Define dummy Device.IP.Interface
     */
    %persistent object Device {
        %persistent object IP {
            %persistent object Interface[] {
                %unique %key %persistent string Alias;
                %persistent bool Enable = true;
                %persistent string Name;
                %persistent bool Loopback;
                %persistent string Status = "Up";
                %persistent string Type;
                %persistent object IPv4Address[] {
                    %unique %key %persistent string Alias;
                    %persistent bool Enable = true;
                    %persistent string IPAddress;
                    %persistent string Status = "Enabled";
                    %persistent string SubnetMask;
                }
                %persistent object IPv6Address[] {
                    %unique %key %persistent string Alias;
                    %persistent bool Enable = true;
                    %persistent string IPAddress;
                    %persistent string Status = "Enabled";
                }
            }
        }
        %persistent object Logical {
            %persistent object Interface[] {
                %unique %key %persistent string Alias;
                %persistent bool Enable = true;
                %persistent string Name;
                %persistent string LowerLayers;
                %persistent string Status = "Up";
            }
        }
        %persistent object NetModel {
            %persistent object Intf[] {
                %unique %key string Alias;
            }
        }
    }
}
%populate {
    object Device {
        object IP.Interface {
            instance add(1, 'loopback', 'Alias' = "loopback") {
                parameter 'Name' = "lo";
                parameter 'Loopback' = true;
                parameter 'Type' = "loopback";
                object IPv4Address {
                    instance add(1, 'loopback_ipv4', 'Alias' = "loopback_ipv4") {
                        parameter 'IPAddress' = "127.0.0.1";
                        parameter 'SubnetMask' = "255.0.0.0";
                    }
                }
                object IPv6Address {
                    instance add(1, 'loopback_ipv6', 'Alias' = "loopback_ipv6") {
                        parameter 'IPAddress' = "::1";
                    }
                }
            }
        }
        object Logical.Interface {
            instance add(1, 'loopback', 'Alias' = "loopback") {
                parameter 'Name' = "";
                parameter 'LowerLayers' = "${ip_intf_lo}";
            }
        }
    }
}
