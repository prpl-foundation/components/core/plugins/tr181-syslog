/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_macros.h>

#include "../mocks/mock.h"
#include "test_defines.h"
#include "test_setup_teardown.h"
#include "test_utils.h"
#include "test_mod_dummy.h"

#include "tr181-syslog.h"
#include "tr181-syslog_dm.h"
#include "tr181-syslog_main.h"
#include "tr181-syslog_config.h"
#include "tr181-syslog_config_source.h"
#include "tr181-syslog_controller.h"
#include "tr181-syslog_hostname.h"

static test_setup_teardown_t test_default_state = {0};

static void load_external_datamodels(test_setup_teardown_t* test_state) {
    test_mod_dummy_expect_config_update();

    amxp_sigmngr_enable(&(amxut_bus_dm()->sigmngr), false);
    if(!test_state->without.system_dm) {
        amxut_dm_load_odl("../odl/test_system.odl");
    }
    if(!test_state->without.netmodel_dm) {
        amxut_dm_load_odl("../odl/test_netmodel.odl");
    }
    if(!test_state->without.security_dm) {
        amxut_dm_load_odl("../odl/test_security.odl");
    }
    if(!test_state->without.deviceinfo_dm) {
        amxut_dm_load_odl("../odl/test_deviceinfo.odl");
        if(!test_state->without.deviceinfo_dm_vlf) {
            amxut_dm_load_odl("../odl/test_deviceinfo_vendorlogfile.odl");
        }
    }
    amxp_sigmngr_enable(&(amxut_bus_dm()->sigmngr), true);
}

static void load_internal_datamodel(test_setup_teardown_t* test_state) {
    bool expect_update = false;

    amxp_sigmngr_enable(&(amxut_bus_dm()->sigmngr), false);
    amxut_dm_load_odl("../odl/tr181-syslog.odl");
    amxp_sigmngr_enable(&(amxut_bus_dm()->sigmngr), true);

    if(test_state->with.populate_odl != NULL) {
        amxut_dm_load_odl(test_state->with.populate_odl);
        expect_update = true;
    }

    assert_int_equal(_syslog_main(AMXO_START, amxut_bus_dm(), amxut_bus_parser()), 0);
    test_mod_dummy_init();

    if(!test_state->without.app_start) {
        amxd_object_emit_signal(amxd_dm_get_object(amxut_bus_dm(), "Syslog"), "app:start", NULL);
        expect_update = true;
    }

    if(expect_update) {
        test_mod_dummy_expect_config_update();
    }
    amxut_bus_handle_events();
}

int test_setup(void** state) {
    test_setup_teardown_t* test_state = NULL;
    if(*state == NULL) {
        *state = &test_default_state;
    }
    test_state = *state;

    amxut_bus_setup(NULL);
    sahTraceSetLevel(TRACE_LEVEL_INFO);
    sahTraceAddZone(TRACE_LEVEL_INFO, "syslog");
    sahTraceAddZone(TRACE_LEVEL_INFO, "mod-dummy");

    amxut_resolve_function("syslog_changed", _syslog_changed);
    amxut_resolve_function("syslog_update_config", _syslog_update_config);
    amxut_resolve_function("syslog_app_start", _syslog_app_start);
    amxut_resolve_function("syslog_app_stop", _syslog_app_stop);
    amxut_resolve_function("syslog_source_network_cleanup", _syslog_source_network_cleanup);
    amxut_resolve_function("Reload", _Reload);

    amxp_sigmngr_add_signal(NULL, "wait:done");

    load_external_datamodels(test_state);
    load_internal_datamodel(test_state);

    assert_true(syslog_controller_is_running());
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxd_object_emit_signal(amxd_dm_get_object(amxut_bus_dm(), "Syslog"), "app:stop", NULL);
    assert_int_equal(_syslog_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_handle_events();
    test_mod_dummy_teardown();
    amxut_bus_teardown(NULL);
    assert_false(syslog_controller_is_running());
    return 0;
}
