/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <unistd.h>
#include <time.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_macros.h>

#include "../mocks/mock.h"
#include "test_utils.h"

#include "tr181-syslog.h"
#include "tr181-syslog_main.h"

static bool test_running_as_root(void) {
    const uid_t root_uid = 0;
    return getuid() == root_uid;
}

void skip_test_if_not_root(void) {
    if(!test_running_as_root()) {
        print_message("Not running as root (skipping)\n");
        skip();
    }
}

amxd_object_t* test_find_instance(const char* object_path, const char* id) {
    amxc_var_t params;
    const char* currentID;
    amxd_object_t* result = NULL;
    amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), "%s", object_path);
    assert_non_null(object);
    amxd_object_for_each(instance, it, object) {
        amxc_var_init(&params);
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        assert_non_null(instance);
        amxd_object_get_params(instance, &params, amxd_dm_access_private);
        currentID = GET_CHAR(&params, "Alias");
        if(currentID && (strcmp(id, currentID) == 0)) {
            result = instance;
        }
        amxc_var_clean(&params);
        if(result) {
            goto exit;
        }
    }
exit:
    return result;
}

static void test_toggle_object(bool enable, amxd_object_t* object) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", enable);
    amxd_trans_apply(&trans, amxut_bus_dm());
    amxd_trans_clean(&trans);
}

void test_enable_object(amxd_object_t* object) {
    test_toggle_object(true, object);
}

void test_disable_object(amxd_object_t* object) {
    test_toggle_object(false, object);
}

void test_write_to_file(const char* file_path, const char* text) {
    FILE* file = fopen(file_path, "a");
    assert_non_null(file);
    fprintf(file, "%s", text);
    fclose(file);
}

static int test_check_file_has_last_line(const char* file_path, const char* expected_text,
                                         int skip_characters) {
    int rv = 1;
    size_t buffer_length = 1024;
    char* buffer = NULL;
    char* line = NULL;

    FILE* file = fopen(file_path, "r");
    when_null(file, exit);

    buffer = calloc(1, buffer_length);
    while((getline(&buffer, &buffer_length, file)) != -1) {
    }
    line = buffer + skip_characters;
    when_false_status(strncmp(expected_text, line, buffer_length - skip_characters) == 0, exit, rv = 2);
    rv = 0;
exit:
    free(buffer);
    if(file != NULL) {
        fclose(file);
    }
    return rv;
}

void test_check_file_last_line(const char* file_path, const char* expected_text,
                               int skip_characters) {
    assert_int_equal(test_check_file_has_last_line(file_path, expected_text, skip_characters), 0);
}

void test_wait_for_file_line(const char* file_path, const char* expected_text, int timeout) {
    for(int i = 0; i < timeout; i += (timeout / 20)) {
        if(test_check_file_has_last_line(file_path, expected_text, 0) == 0) {
            return;
        }
        usleep(timeout * 50);
    }
    fail_msg("file: %s doesn't contain %s", file_path, expected_text);
}

void _test_trigger_wait(const char* signal) {
    amxp_sigmngr_emit_signal(NULL, signal, NULL);
}

void _make_dm_available(const char* odl, const char* signal) {
    amxp_sigmngr_enable(&(amxut_bus_dm()->sigmngr), false);
    amxut_dm_load_odl(odl);
    amxp_sigmngr_enable(&(amxut_bus_dm()->sigmngr), true);
    amxp_sigmngr_emit_signal(NULL, signal, NULL);
    amxut_bus_handle_events();
}
