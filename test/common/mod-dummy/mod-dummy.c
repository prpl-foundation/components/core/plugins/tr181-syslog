/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxut/amxut_bus.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod-dummy.h"

#define ME "mod-dummy"
#define MOD_SYSLOG "mod-dummy"

typedef struct {
    amxm_module_t* mod;
    amxm_shared_object_t* so;
    bool enable;
} mod_dummy_t;
mod_dummy_t mod_dummy;

static int mod_dummy_call_tr181(const char* const func_name, amxc_var_t* args, amxc_var_t* ret) {
    amxm_module_t* mod_tr181 = amxm_get_module("self", "tr181");
    return amxm_module_execute_function(mod_tr181, func_name, args, ret);
}

static void mod_dummy_status_update(const char* status) {
    amxc_var_t args, ret;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set(cstring_t, &args, status);

    assert_int_equal(mod_dummy_call_tr181("controller-status", &args, &ret), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void mod_dummy_trigger_config_update(const amxc_var_t* config) {
    amxc_var_t args, ret;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_copy(&args, config);

    assert_int_equal(mod_dummy_call_tr181("test_mod_dummy_config_updated", &args, &ret), 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static int mod_dummy_update(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    const amxc_var_t* config = GET_ARG(args, "config");
    mod_dummy.enable = GET_BOOL(args, "enable");

    SAH_TRACEZ_INFO(ME, "mod-dummy updated: %d", mod_dummy.enable);

    mod_dummy_status_update(mod_dummy.enable ? "Enabled" : "Disabled");
    mod_dummy_trigger_config_update(config);
    return 0;
}

static int mod_dummy_is_running(UNUSED const char* function_name,
                                UNUSED amxc_var_t* args,
                                amxc_var_t* ret) {
    amxc_var_set(bool, ret, mod_dummy.enable);
    return 0;
}

/**
 * @brief
 * Initialize syslog-ng mod
 *
 * @return AMXM_CONSTRUCTOR always return 0.
 */
static AMXM_CONSTRUCTOR mod_dummy_init(void) {
    int rv = 0;
    mod_dummy.mod = NULL;
    mod_dummy.so = amxm_so_get_current();
    rv = amxm_module_register(&mod_dummy.mod, mod_dummy.so, MOD_SYSLOG);
    when_failed_trace(rv, exit, ERROR, "failed to register mod-dummy.so");
    rv = amxm_module_add_function(mod_dummy.mod, "update", mod_dummy_update);
    when_failed_trace(rv, exit, ERROR, "failed to add update function to mod-dummy.so");
    rv = amxm_module_add_function(mod_dummy.mod, "is-running", mod_dummy_is_running);
    when_failed_trace(rv, exit, ERROR, "failed to add is running function to mod-dummy.so");

    SAH_TRACEZ_INFO(ME, "mod-dummy loaded");
exit:
    return rv;
}

/**
 * @brief
 * Teardown syslog-ng mod
 *
 * @return AMXM_DESTRUCTOR always return 0.
 */
static AMXM_DESTRUCTOR mod_dummy_teardown(void) {
    mod_dummy.mod = NULL;
    mod_dummy.so = NULL;
    SAH_TRACEZ_INFO(ME, "mod-dummy unloaded");
    return 0;
}
