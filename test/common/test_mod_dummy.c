/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <fcntl.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <yajl/yajl_gen.h>
#include <amxj/amxj_variant.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_macros.h>

#include "../mocks/mock.h"
#include "test_defines.h"
#include "test_mod_dummy.h"
#include "test_utils.h"

#include "tr181-syslog.h"

#define origin_to_cmocka(origin) origin.file, origin.line
#define fail_from_origin(origin) _fail(origin_to_cmocka(origin))
#define fail_msg_from_origin(origin, msg, ...) do { print_error("ERROR: " msg "\n", ## __VA_ARGS__); fail_from_origin(origin); } while (0)

typedef struct {
    bool expecting;
    amxc_var_t config;
} test_mod_dummy_t;
static test_mod_dummy_t test_mod_dummy;

static int test_mod_dummy_config_updated(UNUSED const char* function_name,
                                         amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    amxc_var_copy(&test_mod_dummy.config, args);

    function_called();
    return 0;
}

void test_mod_dummy_init(void) {
    int rv = 0;
    amxm_module_t* mod = amxm_get_module("self", "tr181");
    amxc_var_init(&test_mod_dummy.config);

    rv = amxm_module_add_function(mod, "test_mod_dummy_config_updated", test_mod_dummy_config_updated);
    if(rv != 0) {
        fail_msg("failed to add update function to mod-dummy.so");
    }
}

void test_mod_dummy_teardown(void) {
    amxc_var_clean(&test_mod_dummy.config);
    test_mod_dummy.expecting = false;
}

void _test_mod_dummy_expect_config_update(amxut_dm_origin_t origin) {
    _expect_function_call("test_mod_dummy_config_updated", origin.file, origin.line, 1);
}

static void test_amxc_var_to_json_file(amxc_var_t* var, const char* fname) {
    int fd = -1;
    variant_json_t* writer = NULL;

    if(amxj_writer_new(&writer, var) != 0) {
        fail_msg("Failed to create json file writer");
    }

    fd = open(fname, O_CREAT | O_WRONLY | O_TRUNC, 0660);
    if(fd == -1) {
        fail_msg("File open file %s - error 0x%8.8X %s", fname, errno, strerror(errno));
    }

    amxj_write(writer, fd);

    close(fd);
    amxj_writer_delete(&writer);
}

void test_mod_dummy_config_equals_(const char* file, const char* name, amxut_dm_origin_t origin) {
    int res = 1;
    amxc_string_t actual_file;
    amxc_string_t cmd;
    amxc_string_init(&actual_file, 0);
    amxc_string_init(&cmd, 0);

    amxc_string_setf(&actual_file, "/tmp/tr181-syslog-var-cmp-%s.json", name);

    test_amxc_var_to_json_file(&test_mod_dummy.config, amxc_string_get(&actual_file, 0));

    amxc_string_setf(&cmd, "diff '%s' '%s'", file, amxc_string_get(&actual_file, 0));

    res = system(amxc_string_get(&cmd, 0));
    if(res != 0) {
        fail_msg_from_origin(origin, "Config cmp failed %s != %s", file, amxc_string_get(&actual_file, 0));
    }

    amxc_string_clean(&cmd);
    amxc_string_clean(&actual_file);
}
