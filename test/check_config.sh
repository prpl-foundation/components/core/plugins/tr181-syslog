#!/bin/bash
set -e

if [[ -f /tmp/test_tr181-syslog/etc/syslog-ng.tmp.conf ]]; then
    SYSLOG_CONFIG=/tmp/test_tr181-syslog/etc/syslog-ng.tmp.conf
else
    SYSLOG_CONFIG=/tmp/test_tr181-syslog/etc/syslog-ng.conf
fi
echo "Checking: ${SYSLOG_CONFIG}"
cat "${SYSLOG_CONFIG}"
/usr/sbin/syslog-ng --syntax-only --no-caps -f "${SYSLOG_CONFIG}"
echo "Ok"
