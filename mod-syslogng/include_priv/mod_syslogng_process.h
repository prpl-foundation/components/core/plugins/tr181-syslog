/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_SYSLOGNG_PROCESS_H__)
#define __MOD_SYSLOGNG_PROCESS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>

#include "mod_syslogng.h"

#define SYSLOG_PROC_BIN "/usr/sbin/syslog-ng"
#define SYSLOG_PROC_CTL "/usr/sbin/syslog-ng-ctl"

typedef struct {
    int out;
    int err;
} syslogng_process_fd_t;

#define SYSLOG_PROCESS_CMD_LEN 16
typedef char* syslogng_process_cmd_t[SYSLOG_PROCESS_CMD_LEN];

typedef struct {
    amxp_subproc_t* proc;
    syslogng_process_fd_t fd;
    syslogng_process_cmd_t cmd;
} syslogng_process_main_t;

typedef struct {
    syslogng_process_cmd_t cmd;
    int timeout;
} syslogng_process_ctl_t;

typedef struct {
    syslogng_process_cmd_t cmd;
    int timeout;
} syslogng_process_validate_t;

typedef struct {
    syslogng_process_main_t main;
    syslogng_process_ctl_t ctl;
    syslogng_process_validate_t validate;
} syslogng_process_t;

extern syslogng_process_t syslogng_process;

int syslogng_process_init(void);
int syslogng_process_teardown(void);

int syslogng_process_start(void);
int syslogng_process_stop(void);
int syslogng_process_reload(void);

int syslogng_process_validate_config(const char* config);

int syslogng_process_control(const char* ctl_cmd, const syslogng_process_cmd_t* args);

bool syslogng_process_is_running(void);

size_t syslogng_process_cmd_lookup_end_offset(const syslogng_process_cmd_t* cmd);
bool syslogng_process_cmd_add_at_offset(syslogng_process_cmd_t* cmd, const syslogng_process_cmd_t* args, size_t offset);
bool syslogng_process_cmd_append(syslogng_process_cmd_t* cmd, const syslogng_process_cmd_t* args);

#ifdef __cplusplus
}
#endif

#endif // __MOD_SYSLOGNG_PROCESS_H__