/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_SYSLOG_NG_CONFIG_H__)
#define __MOD_SYSLOG_NG_CONFIG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include "mod_syslogng.h"

typedef struct {
    uint32_t major;
    uint32_t minor;
} syslogng_version_t;
typedef struct {
    char file[100];    // main config file "<base>.conf" e.g. "/etc/syslog-ng.conf"
    char inc_dir[100]; // config include directory "<base>.d" e.g. "/etc/syslog-ng.d"
    char staging[100]; // staged config file (for population and validation) before committing to syslogng_file e.g. "/etc/syslog-ng.tmp.conf"
    char run_dir[100]; // directory to store tr181-syslog related runtime files
    bool configured;
    syslogng_version_t version;
} syslogng_config_t;
extern syslogng_config_t syslogng_config;

typedef struct {
    FILE* fp;
    amxc_var_t* config;
    amxc_var_t persist_names;
} syslogng_config_populator_t;

typedef enum {
    INSTANCE_TYPE_HEADER,
    INSTANCE_TYPE_TEMPLATE,
    INSTANCE_TYPE_FILTER,
    INSTANCE_TYPE_SOURCE,
    INSTANCE_TYPE_DESTINATION,
    INSTANCE_TYPE_ACTION,
    INSTANCE_TYPE_FOOTER,
} syslogng_config_instance_index_t;

typedef bool (syslogng_parse_t)(amxc_var_t* config);
typedef bool (syslogng_parse_instance_t)(amxd_object_t* instance, amxc_var_t* config);
typedef bool (syslogng_populate_t)(syslogng_config_populator_t* populator);
typedef struct syslogng_instances_s {
    const char* name;
    syslogng_populate_t* populate;
} syslogng_config_instances_t;

#if defined(__clang__) || defined(__GNUC__)
#define CHECK_PRINTF_ARGS(fmt, first) __attribute__((format(printf, fmt, first)))
#else
#define CHECK_PRINTF_ARGS(fmt, first)
#endif
bool syslogng_populate(syslogng_config_populator_t* populator, const char* fmt, ...) CHECK_PRINTF_ARGS(2, 3);

bool syslogng_config_populate(amxc_var_t* config, const char* file);
bool syslogng_config_commit(const char* src, const char* dst);

bool syslogng_config_is_configured(void);

bool syslogng_filters_populate(syslogng_config_populator_t* populator);
bool syslogng_templates_populate(syslogng_config_populator_t* populator);
bool syslogng_sources_populate(syslogng_config_populator_t* populator);
bool syslogng_destinations_populate(syslogng_config_populator_t* populator);
bool syslogng_actions_populate(syslogng_config_populator_t* populator);

bool syslogng_network_populate(amxc_var_t* network, bool destination, const char* id, syslogng_config_populator_t* populator);

bool syslogng_config_populate_unique_persist_name(syslogng_config_populator_t* populator,
                                                  syslogng_config_instance_index_t instance_idx,
                                                  const char* type,
                                                  const char* implicit_id);

#ifdef __cplusplus
}
#endif

#endif // __MOD_SYSLOG_NG_CONFIG_H__