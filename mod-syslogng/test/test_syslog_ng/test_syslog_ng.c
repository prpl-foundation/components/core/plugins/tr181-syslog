/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include "mod_syslogng.h"
#include "mod_syslogng_config.h"
#include "mod_syslogng_process.h"

#include "../common/test_syslogng_defines.h"
#include "../common/test_syslogng_setup_teardown.h"
#include "../common/test_syslogng_utils.h"
#include "../common/test_syslogng_config.h"
#include "test_syslog_ng.h"

#define TEST_FILE_IN TEST_BASE_DIR "/test_file_in"
#define TEST_FILE_OUT TEST_BASE_DIR "/test_file_out"

amxc_var_t test_config;

int test_syslog_ng_setup(UNUSED void** state) {
    amxc_var_init(&test_config);
    create_empty_config(&test_config);
    return 0;
}

int test_syslog_ng_teardown(UNUSED void** state) {
    amxc_var_clean(&test_config);
    return 0;
}

void test_syslogng_allows_an_empty_config(UNUSED void** state) {
    assert_int_equal(test_trigger_config_update(true, &test_config), 0);
}

void test_syslogng_supports_a_basic_config(UNUSED void** state) {
    // GIVEN: a config file_in => file_out
    amxc_var_t* test_action = NULL;

    config_add_source_file(&test_config, "test_file_in", "User", "Notice", TEST_FILE_IN "_basic");
    config_add_source_local_udp(&test_config, "test_network_in", "User", "All", 8514, false);

    config_add_template(&test_config, "test_template", "\"TEST_OUT ${MESSAGE}\\n\"", false);
    config_add_destination_file(&test_config, "test_file_out", "test_template", TEST_FILE_OUT "_basic");

    test_action = config_add_action(&test_config, "test_do", "test_file_in", "test_file_out", NULL);
    config_add_action_source(test_action, "test_network_in");

    assert_int_equal(test_trigger_config_update(true, &test_config), 0);


    // WHEN: the input file is appended
    test_append_str_to_file("User.Notice Hello basic world!", TEST_FILE_IN "_basic");


    // THEN: the output file should contain our (templated) message
    test_wait_for_file_line(TEST_FILE_OUT "_basic", "TEST_OUT Hello basic world!\n", 5000);
    test_assert_file_line_count(TEST_FILE_OUT "_basic", 1);
}

void test_syslogng_supports_system_and_kernel_sources(UNUSED void** state) {
    // GIVEN: a config system & kernel => file_out
    skip_test_if_not_root();

    config_add_source_system(&test_config, "test_system", "All", "All");
    config_add_source_kernel(&test_config, "test_kernel", "Kern", "All"); //Can't use in amxdev so just checking if can be created as source

    config_add_template(&test_config, "test_template", "\"TEST_OUT ${MESSAGE}\\n\"", false);
    config_add_destination_file(&test_config, "test_file_out", "test_template", TEST_FILE_OUT "_system");

    config_add_action(&test_config, "test_do", "test_system", "test_file_out", NULL);


    // THEN: don't fail creating config
    int success = test_trigger_config_update(true, &test_config);
    system("cp " TEST_CONFIG_BASE_PATH ".conf "TEST_CONFIG_BASE_PATH ".conf.system");
    assert_int_equal(success, 0);


    // WHEN: a system message is logged
    assert_int_equal(system("logger \"Hello world! system\""), 0);


    // THEN: the message should appear in log destination file
    test_wait_for_file_line(TEST_FILE_OUT "_system", "TEST_OUT Hello world! system\n", 5000);
}

void test_syslogng_by_passing_message_through_own_udp(UNUSED void** state) {
    // GIVEN: a config (file_in => network_out) => (network_in => file_out)
    config_add_source_file(&test_config, "test_file_in", "User", "Notice", TEST_FILE_IN "_udp");
    config_add_destination_network_local_udp(&test_config, "test_network_out", NULL, 8514, false);

    config_add_source_local_udp(&test_config, "test_network_in", "User", "All", 8514, false);
    config_add_template(&test_config, "test_template", "\"TEST_OUT ${MESSAGE}\\n\"", false);
    config_add_destination_file(&test_config, "test_file_out", "test_template", TEST_FILE_OUT "_udp");

    config_add_action(&test_config, "test_action_1", "test_network_in", "test_file_out", NULL);
    assert_int_equal(test_trigger_config_update(true, &test_config), 0);
    sleep(1);

    // Create network_out after network_in to avoid potential reconnect delays
    config_add_action(&test_config, "test_action_2", "test_file_in", "test_network_out", NULL);
    assert_int_equal(test_trigger_config_update(true, &test_config), 0);
    sleep(1);


    // WHEN: the input file is appended
    test_append_str_to_file("User.Notice Hello udp world!", TEST_FILE_IN "_udp");


    // THEN: the output file should contain our (templated) message
    test_wait_for_file_line(TEST_FILE_OUT "_udp", "TEST_OUT Hello udp world!\n", 5000);
    test_assert_file_line_count(TEST_FILE_OUT "_udp", 1);
}

void test_syslogng_by_passing_structured_message_through_own_udp(UNUSED void** state) {
    // GIVEN: a config (file_in => network_out) => (network_in => file_out)
    config_add_source_file(&test_config, "test_file_in", "User", "Notice", TEST_FILE_IN "_udp_structured");
    config_add_destination_network_local_udp(&test_config, "test_network_out", NULL, 8514, true);

    config_add_source_local_udp(&test_config, "test_network_in", "User", "All", 8514, true);
    config_add_template(&test_config, "test_template", "\"TEST_OUT ${MESSAGE}\\n\"", false);
    config_add_destination_file(&test_config, "test_file_out", "test_template", TEST_FILE_OUT "_udp_structured");

    config_add_action(&test_config, "test_action_1", "test_network_in", "test_file_out", NULL);
    assert_int_equal(test_trigger_config_update(true, &test_config), 0);
    sleep(1);

    // Create network_out after network_in to avoid potential reconnect delays
    config_add_action(&test_config, "test_action_2", "test_file_in", "test_network_out", NULL);
    assert_int_equal(test_trigger_config_update(true, &test_config), 0);
    sleep(1);


    // WHEN: the input file is appended
    test_append_str_to_file("User.Notice Hello structured udp world!", TEST_FILE_IN "_udp_structured");


    // THEN: the output file should contain our (templated) message
    test_wait_for_file_line_containing(TEST_FILE_OUT "_udp_structured", "Hello structured udp world!\n", 5000);
    test_assert_file_line_count(TEST_FILE_OUT "_udp_structured", 1);
}

void test_syslogng_by_passing_message_through_own_tls(UNUSED void** state) {
    // GIVEN: a config (file_in => network_out) => (network_in => file_out)
    config_add_source_file(&test_config, "test_file_in", "User", "Notice", TEST_FILE_IN "_tls");
    config_add_destination_network_local_tls(&test_config, "test_network_out", NULL, 8514, false, true);

    config_add_source_local_tls(&test_config, "test_network_in", "User", "All", 8514, false, true);
    config_add_template(&test_config, "test_template", "\"TEST_OUT ${MESSAGE}\\n\"", false);
    config_add_destination_file(&test_config, "test_file_out", "test_template", TEST_FILE_OUT "_tls");

    config_add_action(&test_config, "test_action_1", "test_network_in", "test_file_out", NULL);
    assert_int_equal(test_trigger_config_update(true, &test_config), 0);
    sleep(1);

    // Create network_out after network_in to avoid potential reconnect delays
    config_add_action(&test_config, "test_action_2", "test_file_in", "test_network_out", NULL);
    assert_int_equal(test_trigger_config_update(true, &test_config), 0);
    sleep(1);


    // WHEN: the input file is appended
    test_append_str_to_file("User.Notice Hello tls world!", TEST_FILE_IN "_tls");

    // THEN: the output file should contain our (templated) message
    test_wait_for_file_line(TEST_FILE_OUT "_tls", "TEST_OUT Hello tls world!\n", 5000);
    test_assert_file_line_count(TEST_FILE_OUT "_tls", 1);
}

void test_syslogng_supports_2_actions_logging_to_the_same_file(UNUSED void** state) {
    // GIVEN: a config
    // file_in_1 => file_out
    // file_in_2 => file_out
    // GIVEN: a config file_in => file_out
    config_add_source_file(&test_config, "test_file_in_1", "User", "Notice", TEST_FILE_IN "_double_dest_1");
    config_add_source_file(&test_config, "test_file_in_2", "User", "Notice", TEST_FILE_IN "_double_dest_2");

    config_add_template(&test_config, "test_template_1", "\"TEST_OUT ${MESSAGE}\\n\"", false);
    config_add_template(&test_config, "test_template_2", "\"TEST_OUT ${MESSAGE}\\n\"", false);

    config_add_destination_file(&test_config, "test_file_out_1", "test_template_1", TEST_FILE_OUT "_double_dest");
    config_add_destination_file(&test_config, "test_file_out_2", "test_template_2", TEST_FILE_OUT "_double_dest");

    config_add_action(&test_config, "test_double_dest_1", "test_file_in_1", "test_file_out_1", NULL);
    config_add_action(&test_config, "test_double_dest_2", "test_file_in_2", "test_file_out_2", NULL);

    assert_int_equal(test_trigger_config_update(true, &test_config), 0);


    // WHEN: the first input file is appended
    test_append_str_to_file("User.Notice Hello first world!", TEST_FILE_IN "_double_dest_1");

    // THEN: the output file should contain our first message
    test_wait_for_file_line(TEST_FILE_OUT "_double_dest", "TEST_OUT Hello first world!\n", 5000);
    test_assert_file_line_count(TEST_FILE_OUT "_double_dest", 1);

    // WHEN: the second input file is appended
    test_append_str_to_file("User.Notice Hello second world!", TEST_FILE_IN "_double_dest_2");

    // THEN: the output file should contain our second message
    test_wait_for_file_line(TEST_FILE_OUT "_double_dest", "TEST_OUT Hello second world!\n", 5000);
    test_assert_file_line_count(TEST_FILE_OUT "_double_dest", 2);
}

void test_syslogng_can_filter_messages(UNUSED void** state) {
    // GIVEN: a config file_in => filter => file_out
    amxc_var_t* test_action = NULL;

    config_add_source_file(&test_config, "test_file_in", "Daemon", "Notice", TEST_FILE_IN "_filter");
    config_add_source_local_udp(&test_config, "test_network_in", "User", "All", 8514, false);

    config_add_template(&test_config, "test_template", "\"TEST_OUT ${MESSAGE} ${FACILITY}.${SEVERITY}\\n\"", false);
    config_add_destination_file(&test_config, "test_file_out", "test_template", TEST_FILE_OUT "_filter");

    config_add_filter(&test_config, "test_filter", "User", "Notice", "EqualOrHigher", "Log", "^myprogram: Hello");
    test_action = config_add_action(&test_config, "test_do", "test_file_in", "test_file_out", "test_filter");
    config_add_action_source(test_action, "test_network_in");

    assert_int_equal(test_trigger_config_update(true, &test_config), 0);


    // WHEN: when multiple messages are logged
    test_append_str_to_file("<13>Feb 12 13:14:15 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter");    //user notice                    [pass]
    test_append_str_to_file("<15>Feb 12 13:14:16 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter");    //user debug                     [filtered]
    test_append_str_to_file("<13>Feb 12 13:14:17 prplOS myprogram: Hiya world!", TEST_FILE_IN "_filter");     //user notice + pattern mismatch [filtered]
    test_append_str_to_file("<21>Feb 12 13:14:18 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter");    //mail notice                    [filtered]
    test_append_str_to_file("<13>Feb 12 13:14:19 prplOS otherprogram: Hello world!", TEST_FILE_IN "_filter"); //user notice + wrong program    [filtered]
    test_append_str_to_file("<11>Feb 12 13:14:19 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter");    //user error                     [pass]


    // THEN: the output file should (only) contain our unfiltered logs
    test_wait_for_file_line(TEST_FILE_OUT "_filter", "TEST_OUT Hello world! user.err\n", 5000);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter", "TEST_OUT Hello world! user.notice\n", 0);
    test_assert_file_line_count(TEST_FILE_OUT "_filter", 2);

    // THEN: the output file should not contain our filtered logs
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter", "TEST_OUT Hello world! user.debug\n", 0);
}

void test_syslogng_can_use_multiple_filters(UNUSED void** state) {
    // GIVEN: a config file_in => (filter || filter2) => file_out
    amxc_var_t* test_action = NULL;

    config_add_source_file(&test_config, "test_file_in", "Daemon", "Notice", TEST_FILE_IN "_filter_multi");
    config_add_source_local_udp(&test_config, "test_network_in", "User", "All", 8514, false);

    config_add_template(&test_config, "test_template", "\"TEST_OUT ${MESSAGE} ${FACILITY}.${SEVERITY}\\n\"", false);
    config_add_destination_file(&test_config, "test_file_out", "test_template", TEST_FILE_OUT "_filter_multi");

    config_add_filter(&test_config, "test_filter", "User", "Notice", "EqualOrHigher", "Log", "^myprogram: Hello");
    config_add_filter(&test_config, "test_filter2", "Kern", "All", "EqualOrHigher", "Log", "^myprogram: Hello");

    test_action = config_add_action(&test_config, "test_do", "test_file_in", "test_file_out", "test_filter");
    config_add_action_source(test_action, "test_network_in");
    config_add_action_filter(test_action, "test_filter2");

    assert_int_equal(test_trigger_config_update(true, &test_config), 0);


    // WHEN: when multiple messages are logged
    test_append_str_to_file("<13>Feb 12 13:14:15 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter_multi");    //user notice                    [log]
    test_append_str_to_file("<15>Feb 12 13:14:16 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter_multi");    //user debug                     [x]
    test_append_str_to_file("<13>Feb 12 13:14:17 prplOS myprogram: Hiya world!", TEST_FILE_IN "_filter_multi");     //user notice + pattern mismatch [x]
    test_append_str_to_file("<07>Feb 12 13:14:18 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter_multi");    //kern debug                     [log]
    test_append_str_to_file("<21>Feb 12 13:14:18 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter_multi");    //mail notice                    [x]
    test_append_str_to_file("<13>Feb 12 13:14:19 prplOS otherprogram: Hello world!", TEST_FILE_IN "_filter_multi"); //user notice + wrong program    [x]
    test_append_str_to_file("<11>Feb 12 13:14:19 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter_multi");    //user error                     [log]


    // THEN: the output file should (only) contain our unfiltered logs
    test_wait_for_file_line(TEST_FILE_OUT "_filter_multi", "TEST_OUT Hello world! user.err\n", 5000);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_multi", "TEST_OUT Hello world! user.notice\n", 0);
    test_assert_file_line_count(TEST_FILE_OUT "_filter_multi", 3);

    // THEN: the output file should not contain our filtered logs
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_multi", "TEST_OUT Hello world! user.debug\n", 0);
}

void test_syslogng_can_block_messages_based_on_severity_action(UNUSED void** state) {
    // GIVEN: a config file_in => filter_stop  (Stop)  => file_out_stop
    //                         => filter_block (Block) => file_out_block
    //                         => filter_log   (Log)   => file_out_log
    config_add_source_file(&test_config, "test_file_in", "Daemon", "Notice", TEST_FILE_IN "_filter_severity");
    config_add_template(&test_config, "test_template", "\"TEST_OUT ${PROGRAM} ${MESSAGE} ${FACILITY}.${SEVERITY}\\n\"", false);

    // A Stop action
    config_add_destination_file(&test_config, "test_file_out_stop", "test_template", TEST_FILE_OUT "_filter_severity_stop");
    config_add_filter(&test_config, "test_filter_stop", "All", "All", "EqualOrHigher", "Stop", "^myprogram: Hello world2");
    config_add_action(&test_config, "test_severity_stop", "test_file_in", "test_file_out_stop", "test_filter_stop");

    // A Block action
    config_add_destination_file(&test_config, "test_file_out_block", "test_template", TEST_FILE_OUT "_filter_severity_block");
    config_add_filter(&test_config, "test_filter_block", "User", "Notice", "EqualOrHigher", "Block", "^myprogram: Hello");
    config_add_action(&test_config, "test_severity_block", "test_file_in", "test_file_out_block", "test_filter_block");

    // A Log action
    config_add_destination_file(&test_config, "test_file_out_log", "test_template", TEST_FILE_OUT "_filter_severity_log");
    config_add_filter(&test_config, "test_filter_log", "User,Kern", "Notice", "EqualOrHigher", "Log", "^myprogram: Hello");
    config_add_action(&test_config, "test_severity_log", "test_file_in", "test_file_out_log", "test_filter_log");

    assert_int_equal(test_trigger_config_update(true, &test_config), 0);


    // WHEN: when different messages are logged
    test_append_str_to_file("<13>Feb 12 13:14:15 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter_severity");    //user notice                    [Stop,Log]
    test_append_str_to_file("<15>Feb 12 13:14:16 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter_severity");    //user debug                     [Stop,Block]
    test_append_str_to_file("<13>Feb 12 13:14:17 prplOS myprogram: Hiya world!", TEST_FILE_IN "_filter_severity");     //user notice + pattern mismatch [Stop,Block]
    test_append_str_to_file("<07>Feb 12 13:14:18 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter_severity");    //kern debug                     [Stop,Block]
    test_append_str_to_file("<13>Feb 12 13:14:15 prplOS myprogram: Hello world2!", TEST_FILE_IN "_filter_severity");   //user notice                    []
    test_append_str_to_file("<21>Feb 12 13:14:18 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter_severity");    //mail notice                    [Stop,Block]
    test_append_str_to_file("<13>Feb 12 13:14:19 prplOS otherprogram: Hello world!", TEST_FILE_IN "_filter_severity"); //user notice + wrong program    [Stop,Block]
    test_append_str_to_file("<11>Feb 12 13:14:19 prplOS myprogram: Hello world!", TEST_FILE_IN "_filter_severity");    //user error                     [Stop,Log]

    // THEN: the Stop output file should (only) contain our unfiltered logs
    test_wait_for_file_line(TEST_FILE_OUT "_filter_severity_stop", "TEST_OUT myprogram Hello world! user.err\n", 5000);

    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_stop", "TEST_OUT myprogram Hello world! user.notice\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_stop", "TEST_OUT myprogram Hello world! user.debug\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_stop", "TEST_OUT myprogram Hiya world! user.notice\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_stop", "TEST_OUT myprogram Hello world! kern.debug\n", 0);
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_severity_stop", "TEST_myprogram OUT Hello world2! user.notice\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_stop", "TEST_OUT myprogram Hello world! mail.notice\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_stop", "TEST_OUT otherprogram Hello world! user.notice\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_stop", "TEST_OUT myprogram Hello world! user.err\n", 0);
    test_assert_file_line_count(TEST_FILE_OUT "_filter_severity_stop", 7);

    // THEN: the Block output file should (only) contain our unfiltered logs
    test_wait_for_file_line(TEST_FILE_OUT "_filter_severity_block", "TEST_OUT otherprogram Hello world! user.notice\n", 5000);

    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_severity_block", "TEST_OUT myprogram Hello world! user.notice\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_block", "TEST_OUT myprogram Hello world! user.debug\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_block", "TEST_OUT myprogram Hiya world! user.notice\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_block", "TEST_OUT myprogram Hello world! kern.debug\n", 0);
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_severity_block", "TEST_OUT myprogram Hello world2! user.notice\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_block", "TEST_OUT myprogram Hello world! mail.notice\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_block", "TEST_OUT otherprogram Hello world! user.notice\n", 0);
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_severity_block", "TEST_OUT myprogram Hello world! user.err\n", 0);
    test_assert_file_line_count(TEST_FILE_OUT "_filter_severity_block", 5);

    // THEN: the LOG output file should (only) contain our unfiltered logs
    test_wait_for_file_line(TEST_FILE_OUT "_filter_severity_log", "TEST_OUT myprogram Hello world! user.err\n", 5000);

    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_log", "TEST_OUT myprogram Hello world! user.notice\n", 0);
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_severity_log", "TEST_OUT myprogram Hello world! user.debug\n", 0);
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_severity_log", "TEST_OUT myprogram Hiya world! user.notice\n", 0);
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_severity_log", "TEST_OUT myprogram Hello world! kern.debug\n", 0);
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_severity_log", "TEST_OUT myprogram Hello world2! user.notice\n", 0);
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_severity_log", "TEST_OUT myprogram Hello world! mail.notice\n", 0);
    test_assert_file_not_contains_line(TEST_FILE_OUT "_filter_severity_log", "TEST_OUT otherprogram Hello world! user.notice\n", 0);
    test_assert_file_contains_line(TEST_FILE_OUT "_filter_severity_log", "TEST_OUT myprogram Hello world! user.err\n", 0);
    test_assert_file_line_count(TEST_FILE_OUT "_filter_severity_log", 2);
}
