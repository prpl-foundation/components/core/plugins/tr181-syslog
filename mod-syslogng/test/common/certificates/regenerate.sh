#!/bin/bash
set -e

if ! command -v openssl 2>&1 >/dev/null; then
    echo >&2 "Error: Please install openssl";
    exit 1;
fi

generate_root_conf() {
    local cnf="$1"
    local common_name="$2"

    cat >> ${1} << EOS
[ req ]
default_bits        = 4096
default_md          = sha256
prompt              = no
encrypt_key         = no
distinguished_name  = dn
attributes		    = req_attributes
x509_extensions     = v3_ca
req_extensions      = v3_req

[ dn ]
C                   = FR
ST                  = Hauts-de-Seine
L                   = Colombes
O                   = SoftAtHome
CN                  = ${common_name}

[ req_attributes ]

[ v3_req ]
basicConstraints = CA:TRUE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment, keyCertSign
subjectAltName = @alt_names
subjectKeyIdentifier=hash

[v3_ca]
basicConstraints = CA:TRUE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment, keyCertSign
subjectAltName = @alt_names
subjectKeyIdentifier=hash

[alt_names]
IP.1=127.0.0.1
DNS.1=localhost

EOS
}

generate_root_ca() {
    ROOT_KEY="root.key"
    ROOT_CERT="root.pem"
    local ROOT_CONF="root.cnf"
    ROOT_SERIAL="root.srl"

    generate_root_conf "${ROOT_CONF}" "SoftAtHome"

    openssl req -x509 -nodes \
            -days 5000 \
            -config ${ROOT_CONF} \
            -keyout ${ROOT_KEY} \
            -out ${ROOT_CERT}

    ROOT_HASH="$(openssl x509 -hash -in ${ROOT_CERT} -noout)"

    rm -f *.0
    ln -sf "${ROOT_CERT}" "${ROOT_HASH}.0"

    rm ${ROOT_CONF}
}

create_sub_config() {
    local cnf="$1"
    local common_name="$2"
    cat >> ${cnf} << EOS
[ req ]
default_bits        = 4096
default_md          = sha256
prompt              = no
encrypt_key         = no
string_mask         = utf8only
distinguished_name  = dn
attributes          = req_attributes
prompt              = no
req_extensions      = v3_req

[ dn ]
C                   = FR
ST                  = Hauts-de-Seine
L                   = Colombes
O                   = SoftAtHome
CN                  = ${common_name}

[ req_attributes ]

[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names

[alt_names]
IP.1=127.0.0.1
DNS.1=localhost

EOS
}

generate_sub_cert() {
    local SUB_KEY="${1}.key"
    local SUB_CERT="${1}.crt"
    local SUB_CONF="${1}.cnf"
    local SUB_CSR="${1}.csr"

    create_sub_config "${SUB_CONF}" "${1}"

    openssl req \
        -config ${SUB_CONF} -new \
        -keyout ${SUB_KEY} -out ${SUB_CSR}

    openssl x509 -req \
            -days 5000 \
            -in ${SUB_CSR} \
            -CA "${CA_DIR}/${ROOT_CERT}" -CAkey "${CA_DIR}/${ROOT_KEY}" -CAcreateserial \
            -extensions v3_req -extfile ${SUB_CONF} \
            -out ${SUB_CERT}

    rm ${SUB_CONF}
    rm ${SUB_CSR}
}

cd "$(dirname "$0")"

generate_root_ca
generate_sub_cert "source"
generate_sub_cert "destination"

chmod u+r *.key
rm "${ROOT_KEY}"
rm "${ROOT_SERIAL}"
