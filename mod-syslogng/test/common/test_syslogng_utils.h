/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__COMMON_TEST_SYSLOGNG_UTILS_H__)
#define __COMMON_TEST_SYSLOGNG_UTILS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

void skip_test_if_not_root(void);

int test_trigger_config_update(bool enable, amxc_var_t* config);

void test_append_str_to_file(const char* line, const char* file);

int test_file_count_lines(const char* file_path);
#define test_assert_file_line_count(file, count) assert_int_equal(test_file_count_lines(file), count)

int test_file_contains_line(const char* file_path, const char* expected_text, int skip_characters);
#define test_assert_file_contains_line(file, str, skip) assert_int_equal(test_file_contains_line(file, str, skip), 0)
#define test_assert_file_not_contains_line(file, str, skip) assert_int_not_equal(test_file_contains_line(file, str, skip), 0)

int test_check_file_has_last_line(const char* file_path, const char* expected_text, int skip_characters);
#define test_assert_file_last_line_equals(file, str, skip) assert_int_equal(test_check_file_has_last_line(file, str, skip), 0)
#define test_assert_file_last_line_not_equals(file, str, skip) assert_int_not_equal(test_check_file_has_last_line(file, str, skip), 0)

void test_wait_for_file_line_(const char* file_path, const char* expected_text, int timeout, const char* const origin_file, const int origin_line);
#define test_wait_for_file_line(file, expect, timeout) test_wait_for_file_line_(file, expect, timeout, __FILE__, __LINE__)

int test_check_file_has_last_line_containing(const char* file_path, const char* expected_text);
#define test_assert_file_last_line_contains(file, str) assert_int_equal(test_check_file_has_last_line_containing(file, str), 0)
#define test_assert_file_last_line_not_contains(file, str) assert_int_not_equal(test_check_file_has_last_line_containing(file, str), 0)

void test_wait_for_file_line_containing_(const char* file_path, const char* expected_text, int timeout, const char* const origin_file, const int origin_line);
#define test_wait_for_file_line_containing(file, expect, timeout) test_wait_for_file_line_containing_(file, expect, timeout, __FILE__, __LINE__)

#ifdef __cplusplus
}
#endif

#endif // __COMMON_TEST_SYSLOGNG_UTILS_H__

