/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxut/amxut_bus.h>

#include "mod_syslogng.h"
#include "mod_syslogng_config.h"
#include "mod_syslogng_process.h"

#include "test_syslogng_defines.h"
#include "test_syslogng_setup_teardown.h"

typedef struct {
    bool applied;
    syslogng_process_cmd_t* cmd;
    syslogng_process_cmd_t args;
} test_syslogng_cmd_modifier_t;
test_syslogng_cmd_modifier_t test_cmd_main = {
    .cmd = &syslogng_process.main.cmd,
    .args = {
        "--no-caps",
        "--persist-file=" TEST_CONFIG_BASE_PATH ".persist",
        "--control=" TEST_CONFIG_BASE_PATH ".ctl",
        "-p", TEST_CONFIG_BASE_PATH ".pid",
        NULL
    },
};
test_syslogng_cmd_modifier_t test_cmd_ctl = {
    .cmd = &syslogng_process.ctl.cmd,
    .args = {
        "--control=" TEST_CONFIG_BASE_PATH ".ctl",
        NULL
    },
};
test_syslogng_cmd_modifier_t test_cmd_validate = {
    .cmd = &syslogng_process.validate.cmd,
    .args = {
        "--no-caps",
        NULL
    },
};

static void test_setup_syslogng_test_dir(void) {
    // Test Certificates
    int res = mkdir(CERTIFICATES_LOCATION, S_IRWXU);
    if(res && (errno != EEXIST)) {
        fail_msg("Failed to create dir: "CERTIFICATES_LOCATION);
    }
    assert_int_equal(system("cp -dr ../common/certificates/{root,source,destination}* "CERTIFICATES_LOCATION "/"), 0);

    // Test /var/run dir
    assert_int_equal(system("mkdir -p " TEST_RUN_BASE_PATH), 0);
}

static void test_setup_syslogng_config(void) {
    assert_true(snprintf(syslogng_config.file, sizeof(syslogng_config.file), "%s",
                         TEST_CONFIG_BASE_PATH ".conf") > 0);
    assert_true(snprintf(syslogng_config.inc_dir, sizeof(syslogng_config.inc_dir), "%s",
                         TEST_CONFIG_BASE_PATH ".d") > 0);
    assert_true(snprintf(syslogng_config.staging, sizeof(syslogng_config.staging), "%s",
                         TEST_CONFIG_BASE_PATH ".tmp.conf") > 0);
    assert_true(snprintf(syslogng_config.run_dir, sizeof(syslogng_config.run_dir), "%s",
                         TEST_RUN_BASE_PATH) > 0);

    syslogng_config.version = (syslogng_version_t) { // syslog-ng version of amxdev
        .major = 3,
        .minor = 28,
    };
}

static void apply_process_cmd_modifier(test_syslogng_cmd_modifier_t* modifier) {
    if(modifier->applied) {
        return;
    }
    assert_true(syslogng_process_cmd_append(modifier->cmd, &modifier->args));
    modifier->applied = true;
}

static void test_setup_syslogng_proc_cmds(void) {
    apply_process_cmd_modifier(&test_cmd_main);
    apply_process_cmd_modifier(&test_cmd_ctl);
    apply_process_cmd_modifier(&test_cmd_validate);
}

int test_setup(UNUSED void** state) {
    test_setup_syslogng_config();
    test_setup_syslogng_test_dir();
    test_setup_syslogng_proc_cmds();

    amxut_bus_setup(NULL);
    sahTraceSetLevel(TRACE_LEVEL_CALLSTACK);
    sahTraceAddZone(TRACE_LEVEL_CALLSTACK, "syslog");
    sahTraceAddZone(TRACE_LEVEL_CALLSTACK, "syslogng");
    sahTraceAddZone(TRACE_LEVEL_CALLSTACK, "sub-syslogng");

    return 0;
}

int test_teardown(UNUSED void** state) {
    syslogng_stop();
    amxut_bus_teardown(NULL);
    return 0;
}
