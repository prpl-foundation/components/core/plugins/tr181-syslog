/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxut/amxut_bus.h>

#include "mod_syslogng.h"
#include "mod_syslogng_config.h"
#include "mod_syslogng_process.h"

#include "test_syslogng_defines.h"
#include "test_syslogng_utils.h"
#include "test_syslogng_config.h"

static amxc_var_t* add_config_sub_hash(amxc_var_t* config, const char* name) {
    amxc_var_t* sub = amxc_var_add_new_key(config, name);
    amxc_var_set_type(sub, AMXC_VAR_ID_HTABLE);
    return sub;
}
void create_empty_config(amxc_var_t* config) {
    amxc_var_t* options = NULL;
    amxc_var_set_type(config, AMXC_VAR_ID_HTABLE);
    add_config_sub_hash(config, "filters");
    add_config_sub_hash(config, "templates");
    add_config_sub_hash(config, "destinations");
    add_config_sub_hash(config, "sources");
    add_config_sub_hash(config, "actions");

    options = add_config_sub_hash(config, "options");
    amxc_var_add_key(cstring_t, options, "timezone", "+00:00");
    amxc_var_add_key(cstring_t, options, "recv-timezone", "+00:00");
    amxc_var_add_key(cstring_t, options, "send-timezone", "+00:00");
}


amxc_var_t* config_add_template(amxc_var_t* config, const char* id, const char* expression, bool escape) {
    amxc_var_t* templates = GET_ARG(config, "templates");
    amxc_var_t* template = amxc_var_add_new_key(templates, id);
    amxc_var_set_type(template, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, template, "expr", expression);
    amxc_var_add_key(bool, template, "escape", escape);
    return template;
}


amxc_var_t* config_add_filter(amxc_var_t* config, const char* id,
                              const char* facility,
                              const char* severity, const char* severity_cmp, const char* severity_cmp_act,
                              const char* pattern) {
    amxc_var_t* filters = GET_ARG(config, "filters");
    amxc_var_t* filter = amxc_var_add_new_key(filters, id);
    amxc_var_set_type(filter, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, filter, "facility", facility);
    amxc_var_add_key(cstring_t, filter, "severity", severity);
    amxc_var_add_key(cstring_t, filter, "severity_cmp", severity_cmp);
    amxc_var_add_key(cstring_t, filter, "severity_cmp_act", severity_cmp_act);
    amxc_var_add_key(cstring_t, filter, "pattern", pattern);
    return filter;
}

amxc_var_t* config_add_source(amxc_var_t* config, const char* id, const char* facility, const char* severity) {
    amxc_var_t* sources = GET_ARG(config, "sources");
    amxc_var_t* source = amxc_var_add_new_key(sources, id);
    amxc_var_t* endpoints = NULL;
    amxc_var_set_type(source, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, source, "facility", facility);
    amxc_var_add_key(cstring_t, source, "severity", severity);
    endpoints = amxc_var_add_new_key(source, "endpoints");
    amxc_var_set_type(endpoints, AMXC_VAR_ID_LIST);
    return source;
}

static amxc_var_t* config_add_source_endpoint(amxc_var_t* source) {
    amxc_var_t* endpoints = GET_ARG(source, "endpoints");
    amxc_var_t* endpoint = amxc_var_add_new(endpoints);
    amxc_var_set_type(endpoint, AMXC_VAR_ID_HTABLE);
    return endpoint;
}

amxc_var_t* config_add_source_file(amxc_var_t* config, const char* id, const char* facility, const char* severity, const char* filepath) {
    amxc_var_t* source = config_add_source(config, id, facility, severity);
    amxc_var_t* endpoint = config_add_source_endpoint(source);

    amxc_var_add_key(cstring_t, endpoint, "type", "file");
    amxc_var_add_key(cstring_t, endpoint, "filepath", filepath);
    return source;
}

amxc_var_t* config_add_source_system(amxc_var_t* config, const char* id, const char* facility, const char* severity) {
    amxc_var_t* source = config_add_source(config, id, facility, severity);
    amxc_var_t* endpoint = config_add_source_endpoint(source);

    amxc_var_add_key(cstring_t, endpoint, "type", "system");
    return source;
}

amxc_var_t* config_add_source_kernel(amxc_var_t* config, const char* id, const char* facility, const char* severity) {
    amxc_var_t* source = config_add_source(config, id, facility, severity);
    amxc_var_t* endpoint = config_add_source_endpoint(source);

    amxc_var_add_key(cstring_t, endpoint, "type", "kernel");
    return source;
}

static amxc_var_t* config_add_source_local_interface(amxc_var_t* endpoint) {
    amxc_var_t* interface = amxc_var_add_new_key(endpoint, "interface");
    amxc_var_set_type(interface, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, interface, "name", "lo");
    return interface;
}

static amxc_var_t* config_add_source_local_network_endpoint(amxc_var_t* source, const char* id, uint16_t port, const char* protocol, bool structured) {
    amxc_var_t* endpoint = config_add_source_endpoint(source);
    amxc_var_add_key(cstring_t, endpoint, "id", id);
    amxc_var_add_key(cstring_t, endpoint, "type", "network");
    amxc_var_add_key(cstring_t, endpoint, "protocol", protocol);
    config_add_source_local_interface(endpoint);
    amxc_var_add_key(uint16_t, endpoint, "port", port);
    amxc_var_add_key(uint16_t, endpoint, "structured", structured);
    return endpoint;
}

amxc_var_t* config_add_source_local_udp(amxc_var_t* config, const char* id, const char* facility, const char* severity, uint16_t port, bool structured) {
    amxc_var_t* source = config_add_source(config, id, facility, severity);
    config_add_source_local_network_endpoint(source, "test_local_udp", port, "UDP", structured);
    return source;
}

static amxc_var_t* config_set_tls_certificate(amxc_var_t* certificate, const char* dir, const char* file, const char* key) {
    amxc_var_set_type(certificate, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, certificate, "enabled", true);
    amxc_var_add_key(cstring_t, certificate, "dir", dir);
    amxc_var_add_key(cstring_t, certificate, "file", file);
    if(key != NULL) {
        amxc_var_add_key(cstring_t, certificate, "key", key);
    }
    return certificate;
}

amxc_var_t* config_add_source_local_tls(amxc_var_t* config, const char* id, const char* facility, const char* severity, uint16_t port, bool structured, bool peer_verify) {
    amxc_var_t* source = config_add_source(config, id, facility, severity);
    amxc_var_t* endpoint = config_add_source_local_network_endpoint(source, "test_local_tls", port, "TLS", structured);
    amxc_var_t* tls = amxc_var_add_new_key(endpoint, "tls");
    amxc_var_set_type(tls, AMXC_VAR_ID_HTABLE);
    config_set_tls_certificate(amxc_var_add_new_key(tls, "certificate"), CERTIFICATES_LOCATION, "source.crt", "source.key");
    config_set_tls_certificate(amxc_var_add_new_key(tls, "ca_certificate"), CERTIFICATES_LOCATION, "root.pem", NULL);
    amxc_var_add_key(uint32_t, tls, "verify", peer_verify);
    return source;
}


amxc_var_t* config_add_destination(amxc_var_t* config, const char* id, const char* type, const char* template) {
    amxc_var_t* destinations = GET_ARG(config, "destinations");
    amxc_var_t* destination = amxc_var_add_new_key(destinations, id);
    amxc_var_set_type(destination, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, destination, "type", type);
    amxc_var_add_key(cstring_t, destination, "template", !EMPTY_STRING(template) ? template : "");
    return destination;
}

amxc_var_t* config_add_destination_file(amxc_var_t* config, const char* id, const char* template, const char* filepath) {
    amxc_var_t* destination = config_add_destination(config, id, "file", template);
    amxc_var_add_key(cstring_t, destination, "filepath", filepath);
    return destination;
}

static amxc_var_t* config_add_destination_network(amxc_var_t* config, const char* id, const char* template, const char* addr, uint32_t port, const char* protocol, bool structured) {
    amxc_var_t* destination = config_add_destination(config, id, "network", template);
    amxc_var_add_key(cstring_t, destination, "protocol", protocol);
    amxc_var_add_key(cstring_t, destination, "address", addr);
    amxc_var_add_key(uint32_t, destination, "port", port);
    amxc_var_add_key(bool, destination, "structured", structured);
    return destination;
}

amxc_var_t* config_add_destination_network_local_udp(amxc_var_t* config, const char* id, const char* template, uint32_t port, bool structured) {
    amxc_var_t* destination = config_add_destination_network(config, id, template, "localhost", port, "UDP", structured);
    return destination;
}

amxc_var_t* config_add_destination_network_local_tls(amxc_var_t* config, const char* id, const char* template, uint32_t port, bool structured, bool peer_verify) {
    amxc_var_t* destination = config_add_destination_network(config, id, template, "localhost", port, "TLS", structured);
    amxc_var_t* tls = amxc_var_add_new_key(destination, "tls");
    amxc_var_set_type(tls, AMXC_VAR_ID_HTABLE);

    config_set_tls_certificate(amxc_var_add_new_key(tls, "certificate"), CERTIFICATES_LOCATION, "destination.crt", "destination.key");
    config_set_tls_certificate(amxc_var_add_new_key(tls, "ca_certificate"), CERTIFICATES_LOCATION, "root.pem", NULL);
    amxc_var_add_key(uint32_t, tls, "verify", peer_verify);
    return destination;
}


amxc_var_t* config_add_action(amxc_var_t* config, const char* id, const char* source, const char* destination, const char* filter) {
    amxc_var_t* sources = NULL;
    amxc_var_t* destinations = NULL;
    amxc_var_t* filters = NULL;
    amxc_var_t* actions = GET_ARG(config, "actions");
    amxc_var_t* action = amxc_var_add_new_key(actions, id);
    amxc_var_set_type(action, AMXC_VAR_ID_HTABLE);

    sources = amxc_var_add_new_key(action, "sources");
    amxc_var_set_type(sources, AMXC_VAR_ID_LIST);
    if(!EMPTY_STRING(source)) {
        amxc_var_add(cstring_t, sources, source);
    }

    destinations = amxc_var_add_new_key(action, "destinations");
    amxc_var_set_type(destinations, AMXC_VAR_ID_LIST);
    if(!EMPTY_STRING(destination)) {
        amxc_var_add(cstring_t, destinations, destination);
    }

    filters = amxc_var_add_new_key(action, "filters");
    amxc_var_set_type(filters, AMXC_VAR_ID_LIST);
    if(!EMPTY_STRING(filter)) {
        amxc_var_add(cstring_t, filters, filter);
    }

    return action;
}

void config_add_action_source(amxc_var_t* action, const char* source) {
    amxc_var_t* sources = GET_ARG(action, "sources");
    amxc_var_add(cstring_t, sources, source);
}

void config_add_action_filter(amxc_var_t* action, const char* filter) {
    amxc_var_t* filters = GET_ARG(action, "filters");
    amxc_var_add(cstring_t, filters, filter);
}

