/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxut/amxut_bus.h>

#include "mod_syslogng.h"
#include "mod_syslogng_config.h"
#include "mod_syslogng_process.h"

#include "test_syslogng_defines.h"
#include "test_syslogng_utils.h"

static bool test_running_as_root(void) {
    const uid_t root_uid = 0;
    return getuid() == root_uid;
}

void skip_test_if_not_root(void) {
    if(!test_running_as_root()) {
        print_message("Not running as root (skipping)\n");
        skip();
    }
}

int test_trigger_config_update(bool enable, amxc_var_t* config) {
    int rv = 1;
    amxc_var_t ret, args;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    rv = amxc_var_set_key(&args, "config", config, AMXC_VAR_FLAG_COPY);
    when_failed(rv, exit);

    amxc_var_add_new_key_bool(&args, "enable", enable);

    rv = amxm_execute_function(syslogng.so->name, MOD_SYSLOG, "update", &args, &ret);
    when_failed(rv, exit);

    amxut_bus_handle_events();
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

void test_append_str_to_file(const char* line, const char* file) {
    FILE* fp = fopen(file, "a");
    assert_non_null(fp);

    assert_true(fprintf(fp, "%s\n", line) > 0);

    if(fp != NULL) {
        fclose(fp);
    }
}

int test_file_count_lines(const char* file_path) {
    int rv = -1;

    size_t buffer_length = 1024;
    char* buffer = NULL;
    FILE* file = fopen(file_path, "r");
    when_null(file, exit);

    rv = 0;
    buffer = calloc(1, buffer_length);
    while((getline(&buffer, &buffer_length, file)) != -1) {
        rv++;
    }
exit:
    free(buffer);
    if(file != NULL) {
        fclose(file);
    }
    return rv;
}

int test_file_contains_line(const char* file_path, const char* expected_text, int skip_characters) {
    int rv = 1;
    size_t buffer_length = 1024;
    char* buffer = NULL;
    char* line = NULL;
    FILE* file = fopen(file_path, "r");
    when_null(file, exit);

    buffer = calloc(1, buffer_length);
    while((getline(&buffer, &buffer_length, file)) != -1) {
        line = buffer + skip_characters;
        when_true_status(strncmp(expected_text, line, buffer_length - skip_characters) == 0, exit, rv = 0);
    }
    rv = 2;
exit:
    free(buffer);
    if(file != NULL) {
        fclose(file);
    }
    return rv;
}

int test_check_file_has_last_line(const char* file_path, const char* expected_text, int skip_characters) {
    int rv = 1;
    size_t buffer_length = 1024;
    char* buffer = NULL;
    char* line = NULL;

    FILE* file = fopen(file_path, "r");
    when_null(file, exit);

    buffer = calloc(1, buffer_length);
    while((getline(&buffer, &buffer_length, file)) != -1) {
    }
    line = buffer + skip_characters;
    when_false_status(strncmp(expected_text, line, buffer_length - skip_characters) == 0, exit, rv = 2);
    rv = 0;
exit:
    free(buffer);
    if(file != NULL) {
        fclose(file);
    }
    return rv;
}

void test_wait_for_file_line_(const char* file_path, const char* expected_text, int timeout, const char* const origin_file, const int origin_line) {
    for(int i = 0; i < timeout; i += (timeout / 20)) {
        if(test_check_file_has_last_line(file_path, expected_text, 0) == 0) {
            return;
        }
        usleep(timeout * 50);
    }
    fail_msg("file: %s doesn't contain %s", file_path, expected_text);
    print_error("ERROR: " "file: %s doesn't contain %s" "\n", file_path, expected_text);
    _fail(origin_file, origin_line);
}

int test_check_file_has_last_line_containing(const char* file_path, const char* expected_text) {
    int rv = 1;
    size_t buffer_length = 1024;
    char* buffer = NULL;

    FILE* file = fopen(file_path, "r");
    when_null(file, exit);

    buffer = calloc(1, buffer_length);
    while((getline(&buffer, &buffer_length, file)) != -1) {
    }
    when_null_status(strstr(buffer, expected_text), exit, rv = 2);
    rv = 0;
exit:
    free(buffer);
    if(file != NULL) {
        fclose(file);
    }
    return rv;
}

void test_wait_for_file_line_containing_(const char* file_path, const char* expected_text, int timeout, const char* const origin_file, const int origin_line) {
    for(int i = 0; i < timeout; i += (timeout / 20)) {
        if(test_check_file_has_last_line_containing(file_path, expected_text) == 0) {
            return;
        }
        usleep(timeout * 50);
    }
    fail_msg("file: %s doesn't contain %s", file_path, expected_text);
    print_error("ERROR: " "file: %s doesn't contain %s" "\n", file_path, expected_text);
    _fail(origin_file, origin_line);
}
