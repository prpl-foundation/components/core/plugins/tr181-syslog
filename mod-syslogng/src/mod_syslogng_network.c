/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/pem.h>


#include <amxc/amxc.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_syslogng_config.h"
#include "mod_syslogng_utils.h"


typedef unsigned long certificate_hash_t;

typedef struct {
    bool enabled;
    const char* cert;
    const char* key;
    const char* dir;
} syslogng_network_tls_certificate_t;

typedef struct {
    syslogng_network_tls_certificate_t certificate;
    syslogng_network_tls_certificate_t ca;
    amxc_string_t ca_dir;
    bool peer_verify;
} syslogng_network_tls_t;

static syslogng_network_tls_t tls_zeroed;

static bool syslogng_network_tls_certificate_parse(amxc_var_t* certificate, syslogng_network_tls_certificate_t* cert) {
    bool rv = false;
    cert->enabled = GET_BOOL(certificate, "enabled");
    cert->dir = GET_CHAR(certificate, "dir");
    cert->cert = GET_CHAR(certificate, "file");
    cert->key = GET_CHAR(certificate, "key");

    when_false_status(cert->enabled, exit, rv = true);

    when_str_empty_trace(cert->dir, exit, ERROR, "Missing certificate dir");
    when_str_empty_trace(cert->cert, exit, ERROR, "Missing certificate file");

    rv = true;
exit:
    return rv;
}

static bool syslogng_network_tls_calculate_certificate_hash(const char* cert_path, certificate_hash_t* hash) {
    bool rv = false;
    X509* cert = X509_new();
    BIO* bio_cert = BIO_new_file(cert_path, "rb");

    when_null_trace(hash, exit, ERROR, "no hash ref provided");
    when_null_trace(bio_cert, exit, ERROR, "Failed to open certificate: %s", cert_path);
    when_true_trace(PEM_read_bio_X509(bio_cert, &cert, NULL, NULL) == 0,
                    exit, ERROR, "Failed to read x509 cert: %s", cert_path);

    *hash = X509_subject_name_hash(cert);
    rv = true;
exit:
    BIO_free(bio_cert);
    X509_free(cert);
    return rv;
}

static bool syslogng_network_tls_provide_ca_dir(amxc_var_t* network, bool destination, const char* id, syslogng_network_tls_t* tls) {
    bool rv = false;
    int res = -1;
    certificate_hash_t cert_hash = 0;
    amxc_string_t hash_path;
    amxc_string_t cert_path;

    amxc_string_init(&hash_path, 0);
    amxc_string_init(&cert_path, 0);

    when_null_trace(network, exit, ERROR, "Missing network argument");
    when_null_trace(tls, exit, ERROR, "Missing tls argument");

    when_false_status(tls->ca.enabled, exit, rv = true);

    amxc_string_setf(&cert_path, "%s/%s", tls->ca.dir, tls->ca.cert);
    when_false_trace(syslogng_network_tls_calculate_certificate_hash(amxc_string_get(&cert_path, 0), &cert_hash), exit, ERROR, "Failed to calculate certificate hash: %s", amxc_string_get(&cert_path, 0));

    res = mkdir(syslogng_config.run_dir, S_IRWXU);
    when_true_trace(res != 0 && errno != EEXIST,
                    exit, ERROR, "Failed to create dir %s", syslogng_config.run_dir);

    when_failed_trace(amxc_string_setf(&hash_path, "%s/ca", syslogng_config.run_dir),
                      exit, ERROR, "Failed to set ca dir base hash_path");
    res = mkdir(amxc_string_get(&hash_path, 0), S_IRWXU);
    when_true_trace(res != 0 && errno != EEXIST,
                    exit, ERROR, "Failed to create dir %s", syslogng_config.run_dir);

    when_failed_trace(amxc_string_appendf(&hash_path, "/%c_%s", destination ? 'd' : 's', id),
                      exit, ERROR, "Failed to set ca_dir base hash_path");
    res = mkdir(amxc_string_get(&hash_path, 0), S_IRWXU);
    when_true_trace(res != 0 && errno != EEXIST,
                    exit, ERROR, "Failed to create dir %s", syslogng_config.run_dir);

    when_failed_trace(amxc_string_copy(&tls->ca_dir, &hash_path),
                      exit, ERROR, "Failed to copy ca_dir: %s", amxc_string_get(&hash_path, 0));

    when_failed_trace(amxc_string_appendf(&hash_path, "/%08lx.0", cert_hash),
                      exit, ERROR, "Failed to set hash_path");
    res = symlink(amxc_string_get(&cert_path, 0), amxc_string_get(&hash_path, 0));
    when_true_trace(res != 0 && errno != EEXIST,
                    exit, ERROR, "Failed to create certificate hash symlink: %s => %s", amxc_string_get(&hash_path, 0), amxc_string_get(&cert_path, 0));

    rv = true;
exit:
    amxc_string_clean(&cert_path);
    amxc_string_clean(&hash_path);
    return rv;
}

static bool syslogng_network_tls_parse(amxc_var_t* network, bool destination, const char* id, syslogng_network_tls_t* tls) {
    bool rv = false;
    amxc_var_t* network_tls = GET_ARG(network, "tls");
    amxc_var_t* peer_verify = NULL;
    amxc_var_t* certificate = NULL;
    amxc_var_t* ca_certificate = NULL;

    when_null_trace(network_tls, exit, ERROR, "Missing \"tls\" var");

    peer_verify = GET_ARG(network_tls, "verify");
    if(peer_verify != NULL) {
        tls->peer_verify = GET_BOOL(peer_verify, NULL);
    }

    certificate = GET_ARG(network_tls, "certificate");
    if(certificate != NULL) {
        when_false_trace(syslogng_network_tls_certificate_parse(certificate, &tls->certificate), exit, ERROR, "Failed to parse TLS certificate");
        when_str_empty_trace(tls->certificate.key, exit, ERROR, "Missing certificate key file");
    }

    ca_certificate = GET_ARG(network_tls, "ca_certificate");
    if(ca_certificate != NULL) {
        when_false_trace(syslogng_network_tls_certificate_parse(ca_certificate, &tls->ca), exit, ERROR, "Failed to parse TLS CA certificate");
        when_false_trace(syslogng_network_tls_provide_ca_dir(network, destination, id, tls), exit, ERROR, "Failed to parse TLS");
    }

    if(!tls->ca.enabled && tls->peer_verify) {
        amxc_string_set(&tls->ca_dir, "/etc/ssl");
    }

    rv = true;
exit:
    return rv;
}

static void syslogng_network_tls_populate(syslogng_network_tls_t* tls, syslogng_config_populator_t* populator) {
    const char* ca_dir = amxc_string_get(&tls->ca_dir, 0);
    syslogng_populate(populator, "\t\ttls(\n");
    if(!EMPTY_STRING(ca_dir)) {
        syslogng_populate(populator, "\t\t\tca_dir(\"%s\")\n", ca_dir);
    }
    if(!EMPTY_STRING(tls->certificate.dir) &&
       !EMPTY_STRING(tls->certificate.cert) &&
       !EMPTY_STRING(tls->certificate.key)) {
        syslogng_populate(populator, "\t\t\tcert-file(\"%s/%s\")\n", tls->certificate.dir, tls->certificate.cert);
        syslogng_populate(populator, "\t\t\tkey-file(\"%s/%s\")\n", tls->certificate.dir, tls->certificate.key);
    }
    if(!tls->peer_verify) {
        syslogng_populate(populator, "\t\t\tpeer-verify(no)\n");
    }
    syslogng_populate(populator, "\t\t)\n");
}

static void syslogng_network_tls_init(syslogng_network_tls_t* tls) {
    amxc_string_init(&tls->ca_dir, 0);
}

static void syslogng_network_tls_clean(syslogng_network_tls_t* tls) {
    amxc_string_clean(&tls->ca_dir);
}

bool syslogng_network_populate(amxc_var_t* network, bool destination, const char* id, syslogng_config_populator_t* populator) {
    bool rv = false;
    bool structured = GET_BOOL(network, "structured");
    const char* addr = GET_CHAR(network, "address");
    const char* interface = GETP_CHAR(network, "interface.name");
    const char* protocol = GET_CHAR(network, "protocol");
    uint32_t port = GET_UINT32(network, "port");
    const char* driver = NULL;
    syslogng_network_tls_t tls = tls_zeroed;

    syslogng_network_tls_init(&tls);

    if(destination) {
        when_str_empty(addr, exit);
    }
    rv = true;

    if(structured) {
        // This driver uses the RFC5424 message format with RFC6587 framing
        driver = "syslog";
    } else {
        // This driver uses the RFC3164 message format without framing
        driver = "network";
    }

    syslogng_populate(populator, "\t%s(\n", driver);
    if(destination) {
        syslogng_populate(populator, "\t\t\"%s\"\n", addr);
    } else if(!EMPTY_STRING(interface)) {
        syslogng_populate(populator, "\t\tinterface(%s)\n", interface);
    } else if(!EMPTY_STRING(addr)) {
        syslogng_populate(populator, "\t\tip(%s)\n", addr);
    } else {
        syslogng_populate(populator, "\t\tip(0.0.0.0)\n");
    }
    syslogng_populate(populator, "\t\tport(%d)\n", port);
    syslogng_populate(populator, "\t\ttransport(\"%s\")\n", protocol);
    if(streq("TLS", protocol)) {
        when_false_trace(syslogng_network_tls_parse(network, destination, id, &tls), exit, ERROR, "Failed to parse TLS");
        syslogng_network_tls_populate(&tls, populator);
    }
exit:
    syslogng_network_tls_clean(&tls);
    return rv;
}
