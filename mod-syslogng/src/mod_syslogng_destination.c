/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_syslogng_config.h"
#include "mod_syslogng_utils.h"

typedef enum {
    DESTINATION_TYPE_UNKNOWN,
    DESTINATION_TYPE_FILE,
    DESTINATION_TYPE_NETWORK,
} syslogng_destination_type_t;

static bool make_entity_multi_line(bool* multiline, syslogng_config_populator_t* populator) {
    bool rv = true;
    when_true(*multiline, exit);
    rv &= syslogng_populate(populator, "\n");
    *multiline = true;
exit:
    return rv;
}

static syslogng_destination_type_t syslogng_destination_parse(const char* str) {
    syslogng_destination_type_t type = DESTINATION_TYPE_UNKNOWN;
    when_str_empty(str, exit);

    if(streq(str, "file")) {
        type = DESTINATION_TYPE_FILE;
    } else if(streq(str, "network")) {
        type = DESTINATION_TYPE_NETWORK;
    }
exit:
    return type;
}

static bool syslogng_destination_populate_file(amxc_var_t* destination, syslogng_config_populator_t* populator) {
    bool rv = false;
    const char* filepath = GET_CHAR(destination, "filepath");

    when_false(syslogng_populate(populator, "\tfile(\"%s\"", filepath), exit);
    when_false(syslogng_config_populate_unique_persist_name(populator, INSTANCE_TYPE_DESTINATION,
                                                            "file", filepath),
               exit);

    rv = true;
exit:
    return rv;
}

static bool syslogng_destination_populate_template(amxc_var_t* destination, bool* multiline, syslogng_config_populator_t* populator) {
    bool rv = true;
    const char* template = GET_CHAR(destination, "template");
    when_str_empty(template, exit);
    rv &= make_entity_multi_line(multiline, populator);
    rv &= syslogng_populate(populator, "\t\ttemplate(t_%s)\n", template);
exit:
    return rv;
}

static bool syslogng_destination_populate(const char* id, amxc_var_t* destination, syslogng_config_populator_t* populator) {
    bool rv = false;
    bool multiline = false;
    syslogng_destination_type_t type = syslogng_destination_parse(GET_CHAR(destination, "type"));
    when_str_empty_trace(id, exit, ERROR, "destination missing id");
    when_true_trace(type == DESTINATION_TYPE_UNKNOWN, exit, ERROR, "invalid destination(%s) type %s", id, GET_CHAR(destination, "type"));
    rv = true;

    rv &= syslogng_populate(populator,
                            "\n"
                            "destination d_%s {\n", id);
    switch(type) {
    case DESTINATION_TYPE_NETWORK:
        rv &= syslogng_network_populate(destination, true, id, populator);
        multiline = true;
        break;
    case DESTINATION_TYPE_FILE:
        rv &= syslogng_destination_populate_file(destination, populator);
        break;
    case DESTINATION_TYPE_UNKNOWN:
        break;
    }
    rv &= syslogng_destination_populate_template(destination, &multiline, populator);
    if(multiline) {
        rv &= syslogng_populate(populator, "\t");
    }
    rv &= syslogng_populate(populator,
                            ");\n"
                            "};\n");
exit:
    return rv;
}

bool syslogng_destinations_populate(syslogng_config_populator_t* populator) {
    bool rv = false;
    when_null(populator, exit);
    when_null(populator->config, exit);
    when_null(populator->fp, exit);
    rv = true;

    amxc_var_for_each(destination, GET_ARG(populator->config, "destinations")) {
        const char* id = amxc_var_key(destination);
        if(!syslogng_destination_populate(id, destination, populator)) {
            rv = false;
            SAH_TRACEZ_WARNING(ME, "populate destination failed: %s", id);
        }
    }
exit:
    return rv;
}
