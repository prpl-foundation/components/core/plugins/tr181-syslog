/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mod_syslogng.h"
#include "mod_syslogng_config.h"
#include "mod_syslogng_process.h"

mod_syslogng_t syslogng;

int syslogng_update_status(const char* status) {
    int rv = 1;
    amxm_module_t* mod_tr181 = amxm_get_module("self", "tr181");
    amxc_var_t args, ret;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set(cstring_t, &args, status);

    rv = amxm_module_execute_function(mod_tr181, "controller-status", &args, &ret);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rv;
}

static int syslogng_update_config(amxc_var_t* config) {
    int rv = 1;

    when_false_trace(syslogng_config_populate(config, syslogng_config.staging),
                     exit, ERROR, "failed to create tmp config file: %s", syslogng_config.staging);
    if(!syslogng.skip_validation) {
        when_failed_trace(syslogng_process_validate_config(syslogng_config.staging),
                          exit, ERROR, "validate tmp syslog config failed: %s", syslogng_config.staging);
    }
    syslogng.skip_validation = false;
    when_false_trace(syslogng_config_commit(syslogng_config.staging, syslogng_config.file),
                     exit, ERROR, "failed to commit config %s to %s", syslogng_config.staging, syslogng_config.file);

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_syslogng_update(UNUSED const char* function_name, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int rv = 2;
    int configured = 0;
    amxc_var_t* config = GET_ARG(args, "config");
    syslogng.enable = GET_BOOL(args, "enable");

    SAH_TRACEZ_INFO(ME, "updating syslogng config");

    configured = syslogng_update_config(config);

    if(!syslogng.enable) {
        when_failed_trace(syslogng_process_stop(),
                          exit, ERROR, "failed to stop syslog-ng process");
    } else if(!syslogng_process_is_running()) {
        when_failed_trace(syslogng_process_start(),
                          exit, ERROR, "failed to start syslog-ng process");
    } else {
        when_failed(configured, exit);
        when_failed_trace(syslogng_process_reload(),
                          exit, ERROR, "failed to reload config: %s", syslogng_config.file);
    }
    rv = configured;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int mod_syslogng_is_running(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    amxc_var_set(bool, ret, syslogng_process_is_running());
    return 0;
}

int syslogng_stop(void) {
    syslogng.enable = false;
    if(syslogng_process.main.proc != NULL) {
        syslogng_process_stop();
    }
    amxc_var_set_type(&syslogng.config, AMXC_VAR_ID_HTABLE);
    return 0;
}

/**
 * @brief
 * Initialize syslog-ng mod
 *
 * @return AMXM_CONSTRUCTOR always return 0.
 */
static AMXM_CONSTRUCTOR syslogng_construct(void) {
    syslogng.enable = false;
    syslogng.skip_validation = true; // Skip first validation (assumed validated to speedup start at boot)
    syslogng.mod = NULL;
    syslogng.so = amxm_so_get_current();
    amxc_var_init(&syslogng.config);
    amxc_var_set_type(&syslogng.config, AMXC_VAR_ID_HTABLE);

    amxm_module_register(&syslogng.mod, syslogng.so, MOD_SYSLOG);
    amxm_module_add_function(syslogng.mod, "update", mod_syslogng_update);
    amxm_module_add_function(syslogng.mod, "is-running", mod_syslogng_is_running);
    return 0;
}

/**
 * @brief
 * Teardown syslog-ng mod
 *
 * @return AMXM_DESTRUCTOR always return 0.
 */
static AMXM_DESTRUCTOR syslogng_teardown(void) {
    syslogng_stop();

    syslogng.mod = NULL;
    syslogng.so = NULL;
    amxc_var_clean(&syslogng.config);
    return 0;
}
