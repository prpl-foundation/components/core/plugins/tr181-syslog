/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_syslogng_config.h"
#include "mod_syslogng_utils.h"

typedef enum {
    SOURCE_TYPE_UNKNOWN,
    SOURCE_TYPE_FILE,
    SOURCE_TYPE_NETWORK,
    SOURCE_TYPE_SYSTEM,
    SOURCE_TYPE_KERNEL,
} syslogng_source_type_t;

static syslogng_source_type_t syslogng_source_type_parse(const char* str) {
    syslogng_source_type_t type = SOURCE_TYPE_UNKNOWN;
    when_str_empty(str, exit);

    if(streq(str, "file")) {
        type = SOURCE_TYPE_FILE;
    } else if(streq(str, "network")) {
        type = SOURCE_TYPE_NETWORK;
    } else if(streq(str, "system")) {
        type = SOURCE_TYPE_SYSTEM;
    } else if(streq(str, "kernel")) {
        type = SOURCE_TYPE_KERNEL;
    }
exit:
    return type;
}

static bool syslogng_source_populate_file(amxc_var_t* source, syslogng_config_populator_t* populator) {
    const char* filepath = GET_CHAR(source, "filepath");

    return syslogng_populate(populator, "\tfile(\"%s\"", filepath);
}

static bool syslogng_source_populate_system(syslogng_config_populator_t* populator) {
    bool rv = true;
    rv &= syslogng_populate(populator, "\tunix-dgram(\"/dev/log\"\n");
    /*
     * Messages in /dev/log arrive with UTC timestamps but without timezone information.
     * e.g. 10:15:11 (+00:00)
     *
     * Syslog-ng populates the unspecified timezone as the local timezone.
     * This results in the wrong actual timestamp.
     * e.g. 10:15:11-06:00
     *
     * Adding "keep-timestamp(no)" will drop the timestamp in the original message
     * and use the timestamp(+local timezone) associated with when syslog-ng received the messages instead.
     * e.g. 4:15:11-06:00
     */
    rv &= syslogng_populate(populator, "\t\tkeep-timestamp(no)\n");
    return rv;
}

static bool syslogng_source_populate_kernel(syslogng_config_populator_t* populator) {
    return syslogng_populate(populator, "\tfile(\"/proc/kmsg\" program_override(\"kernel\") flags(kernel)");
}

static bool make_entity_multi_line(bool* multiline, syslogng_config_populator_t* populator) {
    bool rv = true;
    when_true(*multiline, exit);
    rv = syslogng_populate(populator, "\n");
    *multiline = true;
exit:
    return rv;
}

static bool syslogng_source_populate_endpoint(const char* id, amxc_var_t* source, const char* default_facility, const char* default_severity, syslogng_config_populator_t* populator) {
    bool rv = false;
    bool multiline = false;
    syslogng_source_type_t type = syslogng_source_type_parse(GET_CHAR(source, "type"));

    when_true_trace(type == SOURCE_TYPE_UNKNOWN, exit, ERROR, "invalid source(%s) type %s", id, GET_CHAR(source, "type"));
    rv = true;

    switch(type) {
    case SOURCE_TYPE_FILE:
        rv &= syslogng_source_populate_file(source, populator);
        break;
    case SOURCE_TYPE_NETWORK:
        rv &= syslogng_network_populate(source, false, id, populator);
        multiline = true;
        break;
    case SOURCE_TYPE_SYSTEM:
        rv &= syslogng_source_populate_system(populator);
        multiline = true;
        break;
    case SOURCE_TYPE_KERNEL:
        rv &= syslogng_source_populate_kernel(populator);
        break;
    case SOURCE_TYPE_UNKNOWN:
        break;
    }
    if(!EMPTY_STRING(default_facility) && !streq(default_facility, "All")) {
        rv &= make_entity_multi_line(&multiline, populator);
        rv &= syslogng_populate(populator, "\t\tdefault-facility(%s)\n", default_facility);
    }
    if(!EMPTY_STRING(default_severity) && !streq(default_severity, "All") && !streq(default_severity, "None")) {
        rv &= make_entity_multi_line(&multiline, populator);
        rv &= syslogng_populate(populator, "\t\tdefault-priority(%s)\n", default_severity);
    }
    if(multiline) {
        rv &= syslogng_populate(populator, "\t");
    }
    rv &= syslogng_populate(populator, ");\n");
exit:
    return rv;
}

static bool syslogng_source_populate(const char* id, amxc_var_t* source, syslogng_config_populator_t* populator) {
    uint32_t ep_id = 0;
    bool rv = false;
    const char* default_facility = GET_CHAR(source, "facility");
    const char* default_severity = GET_CHAR(source, "severity");

    when_str_empty_trace(id, exit, ERROR, "source missing id");
    rv = true;

    rv &= syslogng_populate(populator, "\n"
                            "source s_%s {\n", id);
    amxc_var_for_each(endpoint, GET_ARG(source, "endpoints")) {
        ep_id++;
        if(!syslogng_source_populate_endpoint(id, endpoint, default_facility, default_severity, populator)) {
            SAH_TRACEZ_WARNING(ME, "populate source endpoint failed: %s(%d)", id, ep_id);
            rv = false;
        }
    }
    rv &= syslogng_populate(populator, "};\n");
exit:
    return rv;
}

bool syslogng_sources_populate(syslogng_config_populator_t* populator) {
    bool rv = false;
    when_null_trace(populator, exit, ERROR, "");
    when_null_trace(populator->config, exit, ERROR, "");
    when_null_trace(populator->fp, exit, ERROR, "");
    rv = true;

    amxc_var_for_each(source, GET_ARG(populator->config, "sources")) {
        const char* id = amxc_var_key(source);
        if(!syslogng_source_populate(id, source, populator)) {
            SAH_TRACEZ_WARNING(ME, "populate source failed: %s", id);
            rv = false;
        }
    }

exit:
    return rv;
}
