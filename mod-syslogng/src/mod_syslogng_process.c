/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "mod_syslogng.h"
#include "mod_syslogng_process.h"
#include "mod_syslogng_config.h"

#define PROC_ZONE "sub-syslogng"

#define SYSLOG_PROC_CTL_TIMEOUT (10 * 1000)
#define SYSLOG_PROC_VALIDATE_TIMEOUT (10 * 1000)

syslogng_process_t syslogng_process = {
    .main = {
        .cmd = {
            (char*) SYSLOG_PROC_BIN,
            (char*) "--foreground",             // run in foreground
            (char*) "-f", syslogng_config.file, // config file
            (char*) "--stderr",                 // redirect logs to stderr instead of syslog
            NULL
        },
    },
    .ctl = {
        .cmd = {
            (char*) SYSLOG_PROC_CTL, // Syslog control binary
            (char*) "reload",        // control command
            NULL
        },
        .timeout = SYSLOG_PROC_CTL_TIMEOUT,
    },
    .validate = {
        .cmd = {
            (char*) SYSLOG_PROC_BIN,
            (char*) "--syntax-only",  // syntax only: validate/dry-run config
            (char*) "-f", (char*) "", // config file
            (char*) "--stderr",       // redirect validation logs to stderr instead of syslog
            NULL
        },
        .timeout = SYSLOG_PROC_VALIDATE_TIMEOUT,
    },
};

static void syslogng_process_stopped(UNUSED const char* const sig_name,
                                     UNUSED const amxc_var_t* const data,
                                     UNUSED void* const priv);

size_t syslogng_process_cmd_lookup_end_offset(const syslogng_process_cmd_t* cmd) {
    size_t offset = 0;
    when_null_trace(cmd, exit, ERROR, "No cmd provided");
    for(; offset < (SYSLOG_PROCESS_CMD_LEN - 1); offset++) {
        if((*cmd)[offset] == NULL) {
            return offset;
        }
    }
exit:
    return offset;
}

bool syslogng_process_cmd_add_at_offset(syslogng_process_cmd_t* cmd, const syslogng_process_cmd_t* args, size_t offset) {
    bool rv = false;

    when_null_trace(cmd, exit, ERROR, "No cmd provided");
    when_null_trace(args, exit, ERROR, "No args provided");

    for(size_t i = 0; (offset + i) < (SYSLOG_PROCESS_CMD_LEN - 1 - 1); i++) {
        (*cmd)[offset + i] = (*args)[i];
        if((*args)[i] == NULL) {
            rv = true;
            break;
        }
    }
    (*cmd)[SYSLOG_PROCESS_CMD_LEN - 1] = NULL;

    when_false_trace(rv, exit, ERROR, "Failed add all cmd args: No space left");
exit:
    return rv;
}

bool syslogng_process_cmd_append(syslogng_process_cmd_t* cmd, const syslogng_process_cmd_t* args) {
    bool rv = false;
    when_null_trace(cmd, exit, ERROR, "No cmd provided");
    when_null_trace(args, exit, ERROR, "No args provided");
    size_t offset = syslogng_process_cmd_lookup_end_offset((const syslogng_process_cmd_t*) cmd);
    rv = syslogng_process_cmd_add_at_offset(cmd, args, offset);
exit:
    return rv;
}

static void trace_cmd(syslogng_process_cmd_t* cmd, int rv) {
    ssize_t len = 256 - 1;
    char str[256] = {0};

    for(size_t i = 0; i < 20 && (*cmd)[i] != NULL; i++) {
        if(i != 0) {
            strncat(str, " ", len);
            len--;
        }
        if(len <= 0) {
            break;
        }
        strncat(str, (*cmd)[i], len);
        len -= strlen((*cmd)[i]);
        if(len <= 0) {
            break;
        }
    }
    SAH_TRACEZ_INFO(PROC_ZONE, "cmd: %s (%d)", str, rv);
}

static void subproc_read_fd(const char* origin, int fd) {
    char buf[512] = {0};
    FILE* fp = fdopen(fd, "r");
    when_null_trace(fp, exit, ERROR, "failed to open %s", origin);

    while(fgets(buf, sizeof(buf), fp) != NULL) {
        size_t len = strlen(buf);
        if(len == 0) {
            continue;
        }
        if(buf[len - 1] == '\n') {
            buf[len - 1] = '\0'; // ignore terminating newline
        }
        if(strstr(buf, "Error while JIT compiling regular expression") != NULL) {
            // This log was spammy at boot so moved it down to info level
            SAH_TRACEZ_INFO(PROC_ZONE, "sub %s: %s", origin, buf);
            continue;
        }
        if(strstr(buf, "Error opening plugin module; module='http', error='Error relocating /usr/lib/syslog-ng/libhttp.so: deflateEnd: symbol not found'") != NULL) {
            // This message is always logged at boot but doesn't cause issues.
            // Lowering log level so it causes less confusion during debugging
            SAH_TRACEZ_INFO(PROC_ZONE, "sub %s: %s", origin, buf);
            continue;
        }
        SAH_TRACEZ_WARNING(PROC_ZONE, "sub %s: %s", origin, buf);
    }
exit:
    if(fp != NULL) {
        fclose(fp);
    }
}

static void subproc_reader_cb(int fd, void* priv) {
    subproc_read_fd((const char*) priv, fd);
}

static int subproc_add_fd_reader(const char* origin, int fd) {
    int res = amxp_connection_add(fd, subproc_reader_cb, NULL, AMXP_CONNECTION_CUSTOM, (void*) origin);
    when_failed_trace(res, exit, ERROR, "Failed to add reader for %s: %d %d", origin, res, fd);
exit:
    return res;
}

static void syslogng_process_clean(void) {
    when_null_trace(syslogng_process.main.proc, exit, WARNING, "syslog not started (ignore)");

    amxp_slot_disconnect(amxp_subproc_get_sigmngr(syslogng_process.main.proc), "stop", syslogng_process_stopped);

    subproc_read_fd("syslog-ng stderr", syslogng_process.main.fd.err);
    subproc_read_fd("syslog-ng stdout", syslogng_process.main.fd.out);

    amxp_connection_remove(syslogng_process.main.fd.err);
    amxp_connection_remove(syslogng_process.main.fd.out);

    amxp_subproc_delete(&syslogng_process.main.proc);
    syslogng_process.main.proc = NULL;
exit:
    return;
}

static void syslogng_process_stopped(UNUSED const char* const sig_name,
                                     UNUSED const amxc_var_t* const data,
                                     UNUSED void* const priv) {
    if(syslogng.enable) {
        SAH_TRACEZ_ERROR(ME, "Syslog-ng stopped");
        syslogng_update_status("Error");
    } else {
        SAH_TRACEZ_INFO(ME, "Syslog-ng stopped");
        syslogng_update_status("Disabled");
    }
    syslogng_process_clean();
}

int syslogng_process_stop(void) {
    int res = 0;

    when_null_trace(syslogng_process.main.proc, exit, WARNING, "syslog not started (ignore)");

    when_false_trace(syslogng_process_is_running(), exit, WARNING, "syslog already stopped (ignore)");
    SAH_TRACEZ_WARNING(ME, "Stopping syslog");

    res = amxp_subproc_kill(syslogng_process.main.proc, SIGTERM);
    if(res != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to terminate syslog %d", res);
    }
    res = amxp_subproc_wait(syslogng_process.main.proc, 1000);
    when_true(res == 0, exit);

    res = amxp_subproc_kill(syslogng_process.main.proc, SIGKILL);
    when_failed_trace(res, exit, ERROR, "Failed to kill syslog %d", res);
exit:
    syslogng_update_status("Disabled");
    return res;
}

int syslogng_process_start(void) {
    int res = 0;

    when_not_null_trace(syslogng_process.main.proc, exit, WARNING, "syslog already started (ignore)");
    res = amxp_subproc_new(&syslogng_process.main.proc);
    when_failed_trace(res, exit, ERROR, "Failed to create syslog %d", res);

    when_failed(amxp_slot_connect(amxp_subproc_get_sigmngr(syslogng_process.main.proc), "stop", NULL, syslogng_process_stopped, NULL), exit);
    syslogng_process.main.fd.out = amxp_subproc_open_fd(syslogng_process.main.proc, STDOUT_FILENO);
    when_false_trace(syslogng_process.main.fd.out > 0, exit, ERROR, "Failed to acquire syslog-ng stdout");
    syslogng_process.main.fd.err = amxp_subproc_open_fd(syslogng_process.main.proc, STDERR_FILENO);
    when_false_trace(syslogng_process.main.fd.err > 0, exit, ERROR, "Failed to acquire syslog-ng stderr");

    subproc_add_fd_reader("syslog-ng stdout", syslogng_process.main.fd.out);
    subproc_add_fd_reader("syslog-ng stderr", syslogng_process.main.fd.err);

    res = amxp_subproc_vstart(syslogng_process.main.proc, syslogng_process.main.cmd);
    if(res == 0) {
        when_true(amxp_subproc_wait(syslogng_process.main.proc, 300) == 1, exit);
        res = -2;
        SAH_TRACEZ_ERROR(ME, "syslog failed while starting exit=%d cfg=%s", amxp_subproc_get_exitstatus(syslogng_process.main.proc), syslogng_config.file);
    } else {
        SAH_TRACEZ_ERROR(ME, "failed to start syslog %d", res);
    }

    subproc_read_fd("syslog-ng stderr", syslogng_process.main.fd.err);
    subproc_read_fd("syslog-ng stdout", syslogng_process.main.fd.out);
exit:
    trace_cmd(&syslogng_process.main.cmd, res);
    if(res == 0) {
        syslogng_update_status("Enabled");
        SAH_TRACEZ_INFO(ME, "syslog started");
    } else if(syslogng_process.main.proc != NULL) {
        syslogng_update_status("Error");
        syslogng_process_clean();
    }
    return res;
}

int syslogng_process_control(const char* ctl_cmd, const syslogng_process_cmd_t* args) {
    int res = 0;
    int fd_stdout = 0;
    int fd_stderr = 0;
    amxp_subproc_t* ctl = NULL;
    syslogng_process_cmd_t cmd = {0};
    syslogng_process_cmd_add_at_offset(&cmd, (const syslogng_process_cmd_t*) &syslogng_process.ctl.cmd, 0);
    cmd[1] = (char*) ctl_cmd;
    if(args != NULL) {
        when_false_trace(syslogng_process_cmd_append(&cmd, args),
                         exit, ERROR, "Failed to add args to control command");
    }

    when_null_trace(syslogng_process.main.proc, exit, WARNING, "syslog not started (ignore ctl %s)", ctl_cmd);
    when_false_trace(syslogng_process_is_running(), exit, ERROR, "syslog not running (ignore ctl %s)", ctl_cmd);

    res = amxp_subproc_new(&ctl);
    when_failed_trace(res, exit, ERROR, "failed to create syslog ctl %s: %d", ctl_cmd, res);

    fd_stdout = amxp_subproc_open_fd(ctl, STDOUT_FILENO);
    when_false_trace(fd_stdout > 0, exit, ERROR, "failed to acquire syslog-ng-ctl stdout");
    fd_stderr = amxp_subproc_open_fd(ctl, STDERR_FILENO);
    when_false_trace(fd_stderr > 0, exit, ERROR, "failed to acquire syslog-ng-ctl stderr");

    res = amxp_subproc_vstart_wait(ctl, syslogng_process.ctl.timeout, cmd);
    when_failed_trace(res, exit, ERROR, "syslog ctl %s %s", ctl_cmd, res == 1 ? "timed out" : "failed to start");
    res = amxp_subproc_get_exitstatus(ctl);
    if(res) {
        subproc_read_fd("ctl stderr", fd_stderr);
        subproc_read_fd("ctl stdout", fd_stdout);
    }
    when_failed_trace(res, exit, ERROR, "syslog ctl %s failed with exit code: %d", ctl_cmd, res);
exit:
    trace_cmd(&cmd, res);
    amxp_subproc_delete(&ctl);
    return res;
}

int syslogng_process_reload(void) {
    SAH_TRACEZ_INFO(ME, "reloading syslog");
    int res = syslogng_process_control("reload", NULL);
    if(res == 0) {
        syslogng_update_status("Enabled");
    } else {
        SAH_TRACEZ_ERROR(ME, "failed to reload syslog");
        syslogng_update_status("Error");
    }
    return res;
}

int syslogng_process_validate_config(const char* config) {
    int res = 0;
    int fd_stdout = 0;
    int fd_stderr = 0;
    amxp_subproc_t* validator = NULL;
    syslogng_process_cmd_t cmd = {0};
    syslogng_process_cmd_add_at_offset(&cmd, (const syslogng_process_cmd_t*) &syslogng_process.validate.cmd, 0);
    cmd[3] = (char*) config;

    res = amxp_subproc_new(&validator);
    when_failed_trace(res, exit, ERROR, "failed to create syslog validate config proc %d", res);

    fd_stdout = amxp_subproc_open_fd(validator, STDOUT_FILENO);
    when_false_trace(fd_stdout > 0, exit, ERROR, "failed to acquire syslog-ng stdout");
    fd_stderr = amxp_subproc_open_fd(validator, STDERR_FILENO);
    when_false_trace(fd_stderr > 0, exit, ERROR, "failed to acquire syslog-ng stderr");

    res = amxp_subproc_vstart_wait(validator, syslogng_process.validate.timeout, cmd);
    when_failed_trace(res, exit, ERROR, "validate syslog config %s", res == 1 ? "timed out" : "failed to start");
    res = amxp_subproc_get_exitstatus(validator);
    when_failed_trace(res, exit, ERROR, "validate syslog config failed with exit code: %d", res);
exit:
    trace_cmd(&cmd, res);
    if(res) {
        subproc_read_fd("validate stderr", fd_stderr);
        subproc_read_fd("validate stdout", fd_stdout);
    }
    amxp_subproc_delete(&validator);
    return res;
}

bool syslogng_process_is_running(void) {
    bool ret = false;
    when_null(syslogng_process.main.proc, exit);
    ret = amxp_subproc_is_running(syslogng_process.main.proc);
exit:
    return ret;
}
