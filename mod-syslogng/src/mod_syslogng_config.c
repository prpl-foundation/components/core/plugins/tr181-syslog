/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include "mod_syslogng_config.h"

#define SYSLOG_CONFIG_BASE "/etc/syslog-ng"
#define SYSLOG_RUN_DIR "/var/run/tr181-syslog"

static bool syslogng_config_populate_header(syslogng_config_populator_t* populator);
static bool syslogng_config_populate_footer(syslogng_config_populator_t* populator);

syslogng_config_t syslogng_config = {
    .file = SYSLOG_CONFIG_BASE ".conf",
    .inc_dir = SYSLOG_CONFIG_BASE ".d",
    .staging = SYSLOG_CONFIG_BASE ".tmp.conf",
    .run_dir = SYSLOG_RUN_DIR,
    .version = {
        .major = 4,
        .minor = 4,
    },
};

syslogng_config_instances_t syslogng_config_instances[] = {
    {.name = "Header", .populate = syslogng_config_populate_header},
    {.name = "Template", .populate = syslogng_templates_populate},
    {.name = "Filter", .populate = syslogng_filters_populate},
    {.name = "Source", .populate = syslogng_sources_populate},
    {.name = "Destination", .populate = syslogng_destinations_populate},
    {.name = "Action", .populate = syslogng_actions_populate},
    {.name = "Footer", .populate = syslogng_config_populate_footer},
    {0}
};

bool syslogng_populate(syslogng_config_populator_t* populator, const char* fmt, ...) {
    bool rv = false;
    va_list args;

    when_null(populator->fp, exit);

    va_start(args, fmt);
    rv = vfprintf(populator->fp, fmt, args) > 0;
    va_end(args);
exit:
    return rv;
}

static bool syslogng_config_populate_header(syslogng_config_populator_t* populator) {
    bool rv = true;
    amxc_var_t* options = GET_ARG(populator->config, "options");

    rv &= syslogng_populate(populator,
                            "#############################################\n"
                            "# tr181-syslog generated syslog-ng.conf file\n"
                            "#############################################\n"
                            "\n"
                            "@version: %d.%d\n"
                            "\n"
                            "@include \"scl.conf\"\n" // SCL: syslog configuration library (include extra functionality)
                            "\n",
                            syslogng_config.version.major,
                            syslogng_config.version.minor);
    when_false_trace(rv, exit, ERROR, "failed to write header to config file");

    rv &= syslogng_populate(populator,
                            "options {\n"
                            "\tchain_hostnames(no);\n"  // Enable or disable the chained hostname format.
                            "\tkeep_hostname(yes);\n"   // Enable or disable hostname rewriting."
                            "\tuse_fqdn(no);\n"         // Add Fully Qualified Domain Name instead of short hostname."
                            "\n"
                            "\tcreate_dirs(yes);\n"
                            "\tflush_lines(0);\n"       // How many lines are flushed to a destination at a time."
                            "\tlog_fifo_size(256);\n"   // The number of messages that the output queue can store."
                            "\tlog_msg_size(1024);\n"   // Maximum length of a message in bytes."
                            );

    // The period between two STATS messages (sent by syslog-ng, containing statistics about dropped logs) in seconds."
    if(syslogng_config.version.major >= 4) {
        rv &= syslogng_populate(populator, "\tstats(freq(0));\n");
    } else {
        rv &= syslogng_populate(populator, "\tstats_freq(0);\n");
    }

    if(options != NULL) {
        const char* timezone = GET_CHAR(options, "timezone");
        const char* recv_timezone = GET_CHAR(options, "recv-timezone");
        const char* send_timezone = GET_CHAR(options, "send-timezone");

        const char* dir_group = GET_CHAR(options, "dir-group");
        const char* dir_perm = GET_CHAR(options, "dir-perm");
        const char* file_group = GET_CHAR(options, "file-group");
        const char* file_perm = GET_CHAR(options, "file-perm");

        // Overwrite default local timezone in logs
        if(!EMPTY_STRING(timezone)) {
            rv &= syslogng_populate(populator, "\ttime-zone(\"%s\");\n", timezone);
        }
        // Overwrite default local timezone in incoming logs
        if(!EMPTY_STRING(recv_timezone)) {
            rv &= syslogng_populate(populator, "\trecv-time-zone(\"%s\");\n", recv_timezone);
        }
        // Overwrite default local timezone in outgoing logs
        if(!EMPTY_STRING(send_timezone)) {
            rv &= syslogng_populate(populator, "\tsend-time-zone(\"%s\");\n", send_timezone);
        }
        // Overwrite default group for new directories
        if(!EMPTY_STRING(dir_group)) {
            rv &= syslogng_populate(populator, "\tdir_group(\"%s\");\n", dir_group);
        }
        // Overwrite default permissions for new directories
        if(!EMPTY_STRING(dir_perm)) {
            rv &= syslogng_populate(populator, "\tdir_perm(%s);\n", dir_perm);
        }
        // Overwrite default group for files
        if(!EMPTY_STRING(file_group)) {
            rv &= syslogng_populate(populator, "\tgroup(\"%s\");\n", file_group);
        }
        // Overwrite default permissions for files
        if(!EMPTY_STRING(file_perm)) {
            rv &= syslogng_populate(populator, "\tperm(%s);\n", file_perm);
        }
    }

    rv &= syslogng_populate(populator,
                            "};\n"
                            "\n");
    when_false_trace(rv, exit, ERROR, "failed to write options to config file");
exit:
    return rv;
}

static bool syslogng_config_populate_footer(syslogng_config_populator_t* populator) {
    return syslogng_populate(populator,
                             "\n"
                             "@include \"%s\"\n", syslogng_config.inc_dir);
}

bool syslogng_config_populate_unique_persist_name(syslogng_config_populator_t* populator,
                                                  syslogng_config_instance_index_t instance_idx,
                                                  const char* type,
                                                  const char* implicit_id) {
    bool rv = false;
    const char* instance_name = (syslogng_config_instances[instance_idx]).name;
    const char* index_key = NULL;
    amxc_var_t* index_var = NULL;
    int32_t index = 0;
    amxc_string_t index_key_builder;
    amxc_string_init(&index_key_builder, 0);

    when_str_empty_trace(instance_name, exit, ERROR, "Failed to lookup instance(%d) name", instance_idx);

    when_failed_trace(amxc_string_setf(&index_key_builder, "%s_%s_%s", instance_name, type, implicit_id),
                      exit, ERROR, "Failed to build persist-name key: %d %s %s",
                      instance_idx, type, implicit_id);
    index_key = amxc_string_get(&index_key_builder, 0);
    when_str_empty_trace(index_key, exit, ERROR, "Failed to get persist-name key: %d %s %s",
                         instance_idx, type, implicit_id);

    index_var = GET_ARG(&populator->persist_names, index_key);
    if(index_var == NULL) {
        amxc_var_add_key(int32_t, &populator->persist_names, index_key, 1);
    } else {
        index = GET_INT32(index_var, NULL);
        amxc_var_set(int32_t, index_var, index + 1);

        when_false(syslogng_populate(populator, "\t\tpersist-name(\"%s\")\n", index_key), exit);
    }
    rv = true;
exit:
    amxc_string_clean(&index_key_builder);
    return rv;
}

static void syslogng_config_populator_init(syslogng_config_populator_t* populator, amxc_var_t* config) {
    *populator = (syslogng_config_populator_t) {
        .config = config,
        .fp = NULL,
    };
    amxc_var_init(&populator->persist_names);
    amxc_var_set_type(&populator->persist_names, AMXC_VAR_ID_HTABLE);
}

static bool syslogng_config_populator_open(syslogng_config_populator_t* populator, const char* file) {
    populator->fp = fopen(file, "w");
    when_null_trace(populator->fp, exit, ERROR, "Could not open config file '%s' for writing", file);
exit:
    return populator->fp != NULL;
}

static void syslogng_config_populator_clean(syslogng_config_populator_t* populator) {
    if(populator->fp != NULL) {
        fclose(populator->fp);
    }
    amxc_var_clean(&populator->persist_names);
}

bool syslogng_config_populate(amxc_var_t* config, const char* file) {
    bool result = false;
    syslogng_config_populator_t populator;
    syslogng_config_populator_init(&populator, config);

    syslogng_config.configured = false;

    when_false(syslogng_config_populator_open(&populator, file), exit);

    for(syslogng_config_instances_t* i = syslogng_config_instances; i->name != NULL; i++) {
        if(i->populate == NULL) {
            continue;
        }

        when_false_trace(i->populate(&populator), exit, ERROR, "failed to populate %s", i->name);
    }

    result = true;
exit:
    syslogng_config_populator_clean(&populator);
    return result;
}

bool syslogng_config_commit(const char* src, const char* dst) {
    bool result = false;
    when_false_trace(rename(src, dst) != -1,
                     exit, ERROR, "rename(%s, %s) failed: %s", src, dst, strerror(errno));
    syslogng_config.configured = true;
    result = true;
exit:
    return result;
}

bool syslogng_config_is_configured(void) {
    return syslogng_config.configured;
}
