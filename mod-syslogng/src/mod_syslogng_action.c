/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_syslogng_config.h"
#include "mod_syslogng_utils.h"

static bool syslogng_action_populate_sources(amxc_var_t* sources, syslogng_config_populator_t* populator) {
    bool rv = true;
    amxc_var_for_each(source, sources) {
        rv &= syslogng_populate(populator, "\tsource(s_%s);\n", GET_CHAR(source, NULL));
    }
    return rv;
}

static bool syslogng_action_populate_destinations(amxc_var_t* destinations, syslogng_config_populator_t* populator) {
    bool rv = true;
    amxc_var_for_each(destination, destinations) {
        rv &= syslogng_populate(populator, "\tdestination(d_%s);\n", GET_CHAR(destination, NULL));
    }
    return rv;
}

static const char* syslogng_action_filter_action(amxc_var_t* cfg_filter) {
    const char* severity_cmp_act = NULL;
    when_null_trace(cfg_filter, exit, ERROR, "no filter config found");

    severity_cmp_act = GET_CHAR(cfg_filter, "severity_cmp_act");
    when_str_empty_trace(severity_cmp_act, exit, ERROR, "severity_cmp_act empty");
exit:
    return severity_cmp_act;
}

static bool syslogng_action_populate_stop_filter(const char* filter_id, amxc_var_t* sources, syslogng_config_populator_t* populator) {
    bool rv = true;

    rv &= syslogng_populate(populator, "\nlog {\n");
    rv &= syslogng_action_populate_sources(sources, populator);
    rv &= syslogng_populate(populator,
                            "\tfilter(f_%s);\n" // Mark all messages matching "Stop" filter as "processed" in this log-path
                                                // No destination to send all "processed" messages to nowhere
                            "\tflags(final);\n" // Don't process all "processed" messages in any other log-paths after this
                            "};\n", filter_id);
    return rv;
}

static void syslogng_action_populate_stop_filters(const char* act_id, amxc_var_t* action, syslogng_config_populator_t* populator) {
    amxc_var_t* cfg_filters = GET_ARG(populator->config, "filters");
    amxc_var_t* act_filters = GET_ARG(action, "filters");
    amxc_var_t* sources = GET_ARG(action, "sources");

    amxc_var_for_each(act_filter, act_filters) {
        const char* filter_id = GET_CHAR(act_filter, NULL);
        amxc_var_t* cfg_filter = GET_ARG(cfg_filters, filter_id);
        const char* act = syslogng_action_filter_action(cfg_filter);

        if(!streq(act, "Stop")) {
            continue;
        }

        when_false_trace(syslogng_action_populate_stop_filter(filter_id, sources, populator),
                         exit, ERROR, "Failed to write action '%s' stop filter '%s'", act_id, filter_id);
    }

exit:
    return;
}

static void syslogng_action_populate_filters(amxc_var_t* act_filters, syslogng_config_populator_t* populator) {
    amxc_var_t* cfg_filters = GET_ARG(populator->config, "filters");
    const char* indent = "";
    size_t log_count = 0;
    size_t index = 0;

    // count "Log" filters + write all "Block" filters first
    amxc_var_for_each(act_filter, act_filters) {
        const char* filter_id = GET_CHAR(act_filter, NULL);
        amxc_var_t* cfg_filter = GET_ARG(cfg_filters, filter_id);
        const char* act = syslogng_action_filter_action(cfg_filter);

        if(streq(act, "Log")) {
            log_count++;
            continue;
        }

        if(!streq(act, "Block")) {
            continue;
        }

        syslogng_populate(populator, "\tfilter(f_%s);\n", filter_id);
    }

    if(log_count > 1) {
        indent = "\t";
    }

    // write all "Log" filters
    amxc_var_for_each(act_filter, act_filters) {
        const char* filter_id = GET_CHAR(act_filter, NULL);
        amxc_var_t* cfg_filter = GET_ARG(cfg_filters, filter_id);
        const char* act = syslogng_action_filter_action(cfg_filter);

        if(!streq(act, "Log")) {
            continue;
        }
        index++;

        if(log_count > 1) {
            if(index == 1) {
                syslogng_populate(populator, "\tif {\n");
            } else if(index == log_count) {
                syslogng_populate(populator, "\t} else {\n");
            } else {
                syslogng_populate(populator, "\t} elif {\n");
            }
        }

        syslogng_populate(populator, "%s\tfilter(f_%s);\n", indent, filter_id);
    }
    if(log_count > 1) {
        syslogng_populate(populator, "\t};\n");
    }

    return;
}

static bool syslogng_action_populate(const char* id, amxc_var_t* action, syslogng_config_populator_t* populator) {
    bool rv = false;
    amxc_var_t* sources = GET_ARG(action, "sources");
    amxc_var_t* filters = GET_ARG(action, "filters");
    amxc_var_t* destinations = GET_ARG(action, "destinations");

    when_str_empty_trace(id, exit, ERROR, "action missing id");

    rv = true;
    when_null_trace(amxc_var_get_first(sources), exit, INFO, "Action %s has no sources", id);
    when_null_trace(amxc_var_get_first(destinations), exit, INFO, "Action %s has no destinations", id);

    rv &= syslogng_populate(populator, "\nlog {\n");
    rv &= syslogng_action_populate_sources(sources, populator);
    syslogng_action_populate_filters(filters, populator);
    rv &= syslogng_action_populate_destinations(destinations, populator);
    rv &= syslogng_populate(populator, "};\n");
exit:
    return rv;
}

bool syslogng_actions_populate(syslogng_config_populator_t* populator) {
    bool rv = false;
    when_null(populator, exit);
    when_null(populator->config, exit);
    when_null(populator->fp, exit);
    rv = true;

    amxc_var_for_each(action, GET_ARG(populator->config, "actions")) {
        const char* id = amxc_var_key(action);
        syslogng_action_populate_stop_filters(id, action, populator);
    }

    amxc_var_for_each(action, GET_ARG(populator->config, "actions")) {
        const char* id = amxc_var_key(action);
        if(!syslogng_action_populate(id, action, populator)) {
            SAH_TRACEZ_WARNING(ME, "populate action failed: %s", id);
            rv = false;
        }
    }

exit:
    return rv;
}
