/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_syslogng_config.h"
#include "mod_syslogng_utils.h"


static bool syslogng_filter_populate(const char* id, amxc_var_t* filter, syslogng_config_populator_t* populator) {
    bool rv = false;
    bool has_rules = false;
    const char* indent = "\t";

    const char* facility = GET_CHAR(filter, "facility");
    const char* severity = GET_CHAR(filter, "severity");
    const char* severity_cmp = GET_CHAR(filter, "severity_cmp");
    const char* severity_cmp_act = GET_CHAR(filter, "severity_cmp_act");
    const char* pattern = GET_CHAR(filter, "pattern");

    bool has_facility = !EMPTY_STRING(facility) && !streq(facility, "All");
    bool has_severity = !EMPTY_STRING(severity) && !streq(severity, "All") && !streq(severity, "None");
    bool has_pattern = !EMPTY_STRING(pattern);
    bool negated = streq(severity_cmp_act, "Block");

    when_false_trace(has_facility || has_severity || has_pattern, exit, ERROR, "Filter has no rules. Dropping");

    when_false_trace(syslogng_populate(populator,
                                       "\n"
                                       "filter f_%s {\n", id),
                     exit, ERROR, "");

    if(negated) {
        when_false_trace(syslogng_populate(populator, "%snot (\n", indent), exit, ERROR, "");
        indent = "\t\t";
    }

    if(has_facility) {
        when_false_trace(syslogng_populate(populator, "%sfacility(%s)", indent, facility), exit, ERROR, "");
        has_rules = true;
    }

    if(has_severity) {
        if(has_rules) {
            when_false_trace(syslogng_populate(populator,
                                               "\n"
                                               "%sand\n", indent), exit, ERROR, "");
        }

        when_false_trace(syslogng_populate(populator, "%sseverity(%s%s)",
                                           indent,
                                           severity,
                                           streq(severity_cmp, "EqualOrHigher") ? "..Emergency" : ""
                                           ),
                         exit, ERROR, "");
        has_rules = true;
    }

    if(has_pattern) {
        if(has_rules) {
            when_false_trace(syslogng_populate(populator,
                                               "\n"
                                               "%sand\n", indent),
                             exit, ERROR, "");
        }
        when_false_trace(syslogng_populate(populator, "%smatch(\"%s\" template(\"${PROGRAM}: ${MESSAGE}\") flags(disable-jit))",
                                           indent, pattern),
                         exit, ERROR, "");
    }

    if(negated) {
        when_false_trace(syslogng_populate(populator,
                                           "\n"
                                           "\t)"),
                         exit, ERROR, "");
    }
    when_false_trace(syslogng_populate(populator,
                                       ";\n"
                                       "};\n"),
                     exit, ERROR, "");

    rv = true;
exit:
    return rv;
}

bool syslogng_filters_populate(syslogng_config_populator_t* populator) {
    bool rv = false;
    amxc_var_t* filters = amxc_var_get_key(populator->config, "filters", AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(filters, exit, ERROR, "filters key missing");
    when_null_trace(populator->fp, exit, ERROR, "fp is NULL");

    amxc_var_for_each(filter, filters) {
        const char* id = amxc_var_key(filter);
        when_false_trace(syslogng_filter_populate(id, filter, populator), exit,
                         ERROR, "failed to populate filter(%s)", id);
    }

    rv = true;
exit:
    return rv;
}
