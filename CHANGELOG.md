# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.6.12 - 2024-12-22(09:15:28 +0000)

### Other

- [SSH] Make getDebug working with non-root ssh user.

## Release v1.6.11 - 2024-12-09(09:57:05 +0000)

### Other

- (PCF-1498) - [tr181-syslog] Add Info option to Severity parameters

## Release v1.6.10 - 2024-09-11(16:09:38 +0000)

### Other

- - [Tr181-syslog] 2 actions should be able to log in the same log file

## Release v1.6.9 - 2024-09-10(07:11:21 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v1.6.8 - 2024-09-05(11:37:11 +0000)

### Other

- - [TR181-Syslog] Add odl validation for  Device.Syslog.Status

## Release v1.6.7 - 2024-09-05(11:13:48 +0000)

### Other

- - [Tr181-syslog] Implement Syslog.Action.{i}.StructuredData

## Release v1.6.6 - 2024-07-26(14:11:45 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v1.6.5 - 2024-07-26(13:50:56 +0000)

### Other

- tr181-syslog: package is missing dependency on logrotate

## Release v1.6.4 - 2024-06-20(08:36:24 +0000)

### Other

- - [TR181-Syslog] Implement TLS protocol for source and destination

## Release v1.6.3 - 2024-06-17(15:30:18 +0000)

### Other

- - [DebugInformation] Retrieve early syslog logs in debug file

## Release v1.6.2 - 2024-05-22(21:10:59 +0000)

### Fixes

- [TR181-Syslog] crash tr181-syslog

## Release v1.6.1 - 2024-05-17(13:42:07 +0000)

### Fixes

- FilterRef and SourceRef parameters under Syslog.Action.{i}. object are not accepting data model object path ending with dot

## Release v1.6.0 - 2024-05-15(14:20:33 +0000)

### New

- [TR181-Syslog] Add a Status parameter to Syslog.Action.{i}.LogRemote.

## Release v1.5.7 - 2024-05-11(07:10:41 +0000)

### Fixes

- [Syslog] Local timezone should be used for syslog logs

## Release v1.5.6 - 2024-05-09(07:46:43 +0000)

### Fixes

- [Tr181-Syslog] Action Filters should be OR'd

## Release v1.5.5 - 2024-05-09(07:35:25 +0000)

### Fixes

- [TR181-DeviceInfo] Hostname should be available sooner

## Release v1.5.4 - 2024-04-18(08:35:25 +0000)

### Fixes

- [tr181-syslog] Systematic crash when removing a Source instance with Network parameters

## Release v1.5.3 - 2024-04-12(13:47:35 +0000)

### Fixes

- [TR181-Syslog][DeviceInfo] At start DeviceInfo seems to hangs on tr181-syslog

## Release v1.5.2 - 2024-04-10(07:11:55 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.5.1 - 2024-03-29(16:11:22 +0000)

### Fixes

-  [Tr181-syslog] Improve config overwrite

## Release v1.5.0 - 2024-03-23(13:16:28 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v1.4.0 - 2024-03-20(21:31:55 +0000)

### New

- [tr181-syslog] Create remote messages action

## Release v1.3.0 - 2024-03-20(06:56:02 +0000)

### New

- Add tr181-device proxy odl files to components
- Revert "Add tr181-device proxy odl files to components"

### Other

- Rework TR-181 interface stacks indexing number

## Release v1.2.0 - 2024-03-14(12:11:31 +0000)

### New

- [tr181-syslog] Apply complex syslog configuration

## Release v1.1.2 - 2024-03-12(09:19:24 +0000)

### Fixes

- [TR181-Syslog] log syslog to a dedicated destination

## Release v1.1.1 - 2024-03-11(12:59:57 +0000)

### Fixes

-  [tr181-syslog] When hostname changes syslog needs to be restarted

## Release v1.1.0 - 2024-03-11(12:10:59 +0000)

### New

- [TR181-Syslog] log syslog to a dedicated destination

## Release v1.0.1 - 2024-02-21(16:51:14 +0000)

### Fixes

- [tr181-syslog] Required Information: The Home CPE SHALL have the ability to log the time as a four-digit year, month, day, hour, minute, and second.

## Release v1.0.0 - 2024-02-21(08:37:31 +0000)

### Breaking

- [TR181-Syslog] Implementation of the new 2.17 BBF Data model definition Syslog.

## Release v0.1.8 - 2024-01-26(14:05:59 +0000)

### Fixes

- [Syslog] Prpl logs should have UTC time format instead of local timezone format

## Release v0.1.7 - 2024-01-26(09:08:40 +0000)

### Fixes

- [TR181-Syslog] Rework plugin

## Release v0.1.6 - 2023-12-12(11:52:33 +0000)

### Fixes

- [tr181-syslog] Syslog plugin is not working

## Release v0.1.5 - 2023-11-28(10:55:36 +0000)

### Other

- Opensource component to prpl-foundation

## Release v0.1.4 - 2023-10-13(13:46:23 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.1.3 - 2023-10-04(16:33:11 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v0.1.2 - 2023-08-21(07:09:19 +0000)

### Other

- [tr181-syslog]Erros during start of syslog process

## Release v0.1.1 - 2023-07-31(14:02:18 +0000)

### Other

- - [tr181-syslog][prpl] Implementation of syslog plugin backend

## Release v0.1.0 - 2023-07-24(08:27:05 +0000)

### New

- - [tr181-syslog][prpl] Implementation of a syslog plugin exposing a TR181 DM

