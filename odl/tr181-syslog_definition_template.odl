%define {
    select Syslog {

        /**
         * This table the describes the templates that can be referenced by objects that can use the template syntax.
         * With templates, you have the flexibility to establish uniform message formats or even define standard filenames for different objects.
         *
         * @version V1.0
         */
        %persistent object Template[] {
             counted with TemplateNumberOfEntries;

            /**
             * A non-volatile unique key used to reference this instance. Alias provides a mechanism for a Controller to label this instance for future reference.
             *
             * The following mandatory constraints MUST be enforced:
             * The value MUST NOT be empty.
             * The value MUST start with a letter.
             * If the value is not assigned by the Controller at creation time, the Agent MUST assign a value with an “cpe-” prefix.
             * At most one entry in this table can exist with a given value for Alias. On creation of a new table entry, the Agent MUST (if not supplied by the Controller on creation) choose an initial value for Alias such that the new entry does not conflict with any existing entries.
             * If the value isn’t assigned by the Controller on creation, the Agent MUST choose an initial value that doesn’t conflict with any existing entries.
             * This is a non-functional key and its value MUST NOT change once it’s been assigned by the Controller or set internally by the Agent.
             *
             * @version V1.0
             */
            %write-once %unique %key %persistent string Alias {
                on action validate call check_maximum_length 64;
            }

            /**
             * Describes the template syntax used for transforming the syslog message.
             * The format is implementation specific and can consist of strings, macros, and template functions.
             *
             * E.g. "[UPTIME ${UPTIME}] [${S_DATE}] ${MSGHDR}${MSG}\n"
             *
             * @version V1.0
             */
            %persistent string Expression = "";

            /**
             * Enabling this option will cause the following characters to be escaped ', " and \. in the syslog message.
             *
             * @version V1.0
             */
            %persistent bool EscapeMessage = false;
        }
    }
}

%populate {
    on event "dm:object-changed" call syslog_update_config
        filter 'path matches "^Syslog\.Template\..*"';
}
