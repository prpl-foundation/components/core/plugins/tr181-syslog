%define {
    select Syslog {

        /**
         * This table describes where and how the syslog service receives log messages.
         *
         * @version V1
         */
        %persistent object Source[] {
            counted with SourceNumberOfEntries;

            /**
             * A non-volatile unique key used to reference this instance. Alias provides a mechanism for a Controller to label this instance for future reference.
             *
             * The following mandatory constraints MUST be enforced:
             * The value MUST NOT be empty.
             * The value MUST start with a letter.
             * If the value is not assigned by the Controller at creation time, the Agent MUST assign a value with an “cpe-” prefix.
             * At most one entry in this table can exist with a given value for Alias. On creation of a new table entry, the Agent MUST (if not supplied by the Controller on creation) choose an initial value for Alias such that the new entry does not conflict with any existing entries.
             * If the value isn’t assigned by the Controller on creation, the Agent MUST choose an initial value that doesn’t conflict with any existing entries.
             * This is a non-functional key and its value MUST NOT change once it’s been assigned by the Controller or set internally by the Agent.
             *
             * @version V1.0
             */
            %write-once %unique %key %persistent string Alias;

            /**
             * This option enables the collection of kernel log messages that are specific to the Device.
             *
             * @version V1.0
             */
            %persistent bool KernelMessages = false;

            /**
             * This option enables the collection of the system log messages that are specific to the Device.
             *
             * @version V1.0
             */
            %persistent bool SystemMessages = false;

            /**
             * Specifies the default Severity.
             *
             * When an incoming message lacks a valid syslog header, this parameter can be used to configure the required Severity level.
             *
             * @version V1.0
             */
            %persistent string Severity {
                default "All";
                on action validate call check_enum [
                    "Emergency",    /** Indicates that the system is unusable */
                    "Alert",        /** Indicating that an action must be taken immediately */
                    "Critical",     /** Indicates a critical condition */
                    "Error",        /** Indicates a errors condition */
                    "Warning",      /** Indicates a warning condition */
                    "Notice",       /** Indicates a notice message */
                    "Info",         /** Indicates a informational message */
                    "Debug",        /** Indicates a debug-level message */
                    "All",          /** All severities are selected */
                    "None"          /** No filtering will be applied */
                ];
            }

            /**
             * Specifies the default Facility Level.
             *
             * When an incoming message lacks a valid syslog header, this parameter can be used to configure the required Facility level.
             *
             * @version V1.0
             */
            %persistent string FacilityLevel {
                default "All";
                on action validate call check_enum [
                    "Kern",     /** Kernel messages (0) */
                    "User",     /** User-level messages (1) */
                    "Mail",     /** Mail system messages (2) */
                    "Daemon",   /** System daemons messages (3) */
                    "Auth",     /** Security/authorization messages (4) */
                    "Syslog",   /** Messages generated internally by syslogd (5) */
                    "LPR",      /** Line printer subsystem messages (6) */
                    "News",     /** Network news subsystem (7) */
                    "UUCP",     /** UUCP subsystem messages (8) */
                    "Cron",     /** Clock daemon messages (9) */
                    "AuthPriv", /** Security/authorization messages (10) */
                    "FTP",      /** FTP daemon messages (11) */
                    "NTP",      /** NTP subsystem messages (12) */
                    "Audit",    /** Log audit messages (13) */
                    "Console",  /** Log alert messages (14) */
                    "Cron2",    /** Second clock daemon messages (15) */
                    "Local0",   /** Local use 0 messages (16) */
                    "Local1",   /** Local use 1 messages (17) */
                    "Local2",   /** Local use 2 messages (18) */
                    "Local3",   /** Local use 3 messages (19) */
                    "Local4",   /** Local use 4 messages (20) */
                    "Local5",   /** Local use 5 messages (21) */
                    "Local6",   /** Local use 6 messages (22) */
                    "Local7",   /** Local use 7 messages (23) */
                    "All"       /** All facilities are selected */
                ];
            }

            /**
            * This object describes the configuration parameters for receiving syslog information on a network socket.
            *
            * @version V1.0
            */
            %persistent object Network {
                on action destroy call syslog_source_network_cleanup;
                /**
                * Enables or disables the functionality to receive syslog information on a network socket.
                *
                * @version V1.0
                */
                %persistent bool Enable;

                /**
                 * The IP or Logical Interface on which the syslog information will be received.
                 *
                 * When an empty string is specified the syslog service will listen to all available network interfaces.
                 * The value MUST be the Path Name of a table row.
                 *
                 * Example:
                 * - "Device.IP.Interface.1."
                 * - "Device.Logical.Interface.1."
                 *
                 * If the value is an empty string, the CPE MUST use its routing policy (IP Forwarding table entries), if necessary, to determine the appropriate interface.
                 *
                 * @version V1.0
                 */
                %persistent string Interface {
                    default "";
                    on action validate call check_maximum_length 256;
                    on action validate call matches_regexp "^((Device\.)?(IP|Logical)\.Interface\.([0-9a-zA-Z_-]+|\\[.*\\])\.?)?$";
                    on action write call set_object_ref_simple;
                }

                /**
                * Specifies the port number on which the syslog information will be received.
                *
                * @version V1.0
                */
                %persistent uint16 Port = 1099;

                /**
                * The protocol to be used for receiving syslog information.
                *
                * @version V1.0
                */
                %persistent string Protocol {
                    default "UDP";
                    on action validate call check_enum [
                        "TCP",
                        "UDP",
                        "TLS"
                    ];
                }

                /**
                * This feature represents the ability to receive log messages in structured-data format.
                *
                * Reference RFC5424(The Syslog Protocol).
                *
                * @version V1.0
                */
                %persistent bool '${prefix_}StructuredData' = false;

                /**
                * Certificate that must be used by the syslog service.
                * Only applicable when Protocol is set to TLS
                *
                * The value MUST be the Path Name of a row in the "Security.Certificate." table.
                *
                * @version V1.0
                */
                %persistent string '${prefix_}Certificate' {
                    default "";
                    on action validate call matches_regexp "^((Device\.)?Security\.Certificate\.[0-9]+\.?)?$";
                    on action write call set_object_ref_simple;
                }

                /**
                * CA certificate that must be used by the syslog service to validate the remote syslog server certificate.
                *
                * The value MUST be the Path Name of a row in the "Security.Certificate." table.
                *
                * @version V1.0
                */
                %persistent string '${prefix_}CACertificate' {
                    default "";
                    on action validate call matches_regexp "^((Device\.)?Security\.Certificate\.[0-9]+\.?)?$";
                    on action write call set_object_ref_simple;
                }

                /**
                * Ensures authentic and secure connections by validating entity credentials such as certificates or tokens.
                * Only applicable when Protocol is set to TLS
                *
                * In order to validate a certificate, the entire certificate chain,
                * including the CA certificate, must be valid.
                *
                * If any certificate in the chain is found to be invalid, the syslog service must reject the connection.
                *
                * @version V1.0
                */
                %persistent bool '${prefix_}PeerVerify' = false;
            }
        }
    }
}

%populate {
    on event "dm:object-changed" call syslog_update_config
        filter 'path matches "^Syslog\.Source\..*"';
}
