%define {
    /**
     * This object contains global parameters relating to the syslog implementations that is active in the Device.
     *
     * Based on "A YANG Data Model for Syslog Configuration/YANGSYSLOG"[https://datatracker.ietf.org/doc/draft-ietf-netmod-syslog-model/]
     *
     * @version V1.0
     */
    %persistent object Syslog {

        /**
         * Enables or disables the Syslog service.
         *
         * @version V1.0
         */
        %persistent bool Enable {
            default true;
        }

        /**
         * Indicates the syslog service operational state. Enumeration of:
         *
         * Disabled (Indicates that the Syslog service is disabled)
         * Enabled (Indicates that the Syslog service is enabled)
         * Error (Indicates that the Syslog service has encountered an error, OPTIONAL)
         *
         * @version V1.0
         */
        %read-only string Status {
            default "Disabled";
            on action validate call check_enum [
                "Disabled",
                "Enabled",
                "Error"
            ];
        }

        /**
         * Syslog controller
         *
         * Select which syslog module to use to apply the datamodel
         *
         * @version V1.0
         */
        %protected %read-only string Controller = "${controller-mod}";

        /**
         * Convert timestamps to the timezone specified by this option.
         *
         * If this option is not set, then the original timezone information in the message is used.
         *
         * @version V1.0
         */
        %protected %persistent string TimeZone = "";

        /**
         * The default timezone used for incoming logs
         *
         * If empty use local timezone
         *
         * @version V1.0
         */
        %protected %persistent string DefaultReceiveTimeZone = "";

        /**
         * The default timezone used for outgoing logs
         *
         * If empty use original timezone of the log
         *
         * @version V1.0
         */
        %protected %persistent string DefaultSendTimeZone = "";

        /**
         * The default group for new directories
         *
         * If empty use the default group (e.g. root)
         *
         * @version V1.0
         */
        %protected %persistent string DirectoryGroup = "";

        /**
         * The default directory permissions
         *
         * If empty use the default permissions (0600)
         *
         * @version V1.0
         */
        %protected %persistent string DirectoryPermissions = "";

        /**
         * The default group for new files
         *
         * If empty use the default group (e.g. root)
         *
         * @version V1.0
         */
        %protected %persistent string FileGroup = "";

        /**
         * The default file permissions
         *
         * If empty use the default permissions (0600)
         *
         * @version V1.0
         */
        %protected %persistent string FilePermissions = "";

        /**
         * Syslog default log directory
         *
         * @version V1.0
         */
        %protected %read-only string LogDir = "${log-dir}";

        /**
        * This command is issued to reload the syslog application.
        * @version V1.0
        */
        %protected void Reload();

        event 'Changed';
    }
}

include "tr181-syslog_definition_filter.odl";
include "tr181-syslog_definition_template.odl";

include "tr181-syslog_definition_source.odl";
include "tr181-syslog_definition_action.odl";

%populate {
    on event "app:start" call syslog_app_start;
    on event "app:stop" call syslog_app_stop;
    on event "dm:instance-added" call syslog_update_config
        filter 'path matches "^Syslog\."';
    on event "dm:instance-removed" call syslog_update_config
        filter 'path matches "^Syslog\."';
    on event "dm:object-changed" call syslog_update_config
        filter 'object == "Syslog." && (
            contains("parameters.Enable") ||
            contains("parameters.DirectoryGroup") ||
            contains("parameters.DirectoryPermissions") ||
            contains("parameters.FileGroup") ||
            contains("parameters.FilePermissions")
        )';
    on event "Changed" call syslog_changed;
}
