# TR181-syslog

[[_TOC_]]

## Description
The tr181-syslog service manages the system logging daemon and its configuration.
(currently only syslog-ng is supported)
