#!/bin/sh
set -e

LIBDEBUG=/usr/lib/debuginfo/debug_functions.sh

if [ ! -f "$LIBDEBUG" ]; then
    echo "No debug library found : $LIBDEBUG"
    exit 1
fi

. $LIBDEBUG

find_early_boot_logs() {
    find /tmp/ -type f -maxdepth 1 -name syslog_early_boot
}

find_log_messages_in_dir() {
    if [ -d "$1" ]; then
        find "$1" -type f -maxdepth 1 -name "messages*"
    fi
}

find_log_files() {
    for log_dir in /var/log /data/log /ext
    do
        find_log_messages_in_dir "${log_dir}"
    done
}

dump_dmesg()
{
    show_title "show dmesg"
    show_cmd $SUDO dmesg
}

print_syslog_file()
{
    show_title " ##|$1|##"
    # assumption: if we have gz log-files on the system, we assume zcat is also there
    case "$1" in
    *.gz )
        zcat $1 2>/dev/null
        ;;
    *)
        cat $1  2>/dev/null
        ;;
    esac
}

dump_syslog()
{
    show_title "Show all syslog messages"
    for log_file in $(find_early_boot_logs) $(find_log_files | sort -r)
    do
        print_syslog_file "${log_file}"
    done
}

dump_dmesg
dump_syslog
